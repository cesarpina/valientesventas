import React from 'react';
import {mostrarInfo} from '../../utils/base'

const Denegado = ({}) => (
    
    <div onmouseover={mostrarInfo(this)} className="d-flex flex-column justify-content-center align-items-center bxic">
        <i class="material-icons">info</i>
        <span className="d-flex align-items-center">No Califica</span>
        
        <div className="bxinfNoCal z-depth-2 InfoOculto">
            <div className="d-flex flex-column align-items-center">
                <i class="material-icons">warning</i>
                <span>Lo sentimos</span>
                <p>Este cliente no se encuentra en nuestra base de dato.</p>
            </div>

        </div>

    </div>

)
export default Denegado
