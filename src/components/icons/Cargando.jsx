import React from 'react';

const Cargando = ({}) => (
    
        <div className="d-flex flex-column justify-content-center align-items-center">
        <div class="preloader-wrapper small active">
            <div class="spinner-layer spinner-green-only">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>
        </div>
        <span>Cargando</span>
        </div>
)
export default Cargando
