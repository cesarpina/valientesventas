import React from 'react';

const Check = ({}) => (
    <div className="d-flex flex-column justify-content-center align-items-center">
        <i class="material-icons color-green">check</i>
        <span>Califica</span>
    </div>
)
export default Check
