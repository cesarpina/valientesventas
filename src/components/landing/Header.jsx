import React from 'react';
import logo from '../../resources/images/header/logo.png';
import { deslizarBtnMas, deslizarNavMobile } from '../../resources/js/events.js';
import Buttons from './commons/Buttons';
import condicionado from '../../resources/pdf/CondicionadoGeneral.pdf'
import resumenpoliza from '../../resources/pdf/ResumenPoliza.pdf'

const Header = ({ handlerCotizador, handlerScroll,handlerOptionesMenu }) => {
	let bottonMenu = {
		handleOnClick: handlerCotizador,
		value: 'Cómpralo Online',
		clases: ' btn-landing-comprar',
		labelgtm: 'Cómpralo Online - Menú'
	}
	
	function handlerMenuWeb(option){
		handlerOptionesMenu(option)
		deslizarBtnMas("menu-btn-mas", "dropdown1", 0)
	}

	function handlerMenuMobile(option){
		handlerOptionesMenu(option)
		deslizarNavMobile("mobile-demo")
	}

	return (
		<header className="header-landing" >
			<nav>
				<div className="nav-wrapper">
					<a className="brand-logo" onClick={() => handlerScroll("header")}><img src={logo} alt="" /></a>
					<a data-target="mobile-demo" className="sidenav-trigger" onClick={() => deslizarNavMobile("mobile-demo")}><i className="material-icons menu-icon">menu</i></a>
					<ul id="dropdown1" className="dropdown-content menu-web-mas">
						<li className="menu-item-web" onClick={()=>handlerMenuWeb('Condicionado General')}><a href={condicionado} target="_blank" >Condicionado General</a></li>
						{/* <li className="divider"></li> */}
						{/* <li className="menu-item-web" onClick={()=>handlerMenuWeb('Glosario de términos')}><a href={""} target="_blank" >Glosaio de Términos</a></li> */}
						<li className="divider"></li>
						<li className="menu-item-web" onClick={()=>handlerMenuWeb('Resumen de Póliza')}><a href={resumenpoliza} target="_blank" >Resumen de Póliza</a></li>
					</ul>
					<ul className="right hide-on-med-and-down menu-lista-web">
						<li className="menu-item-web" onClick={()=>handlerMenuWeb('¿Qué cubre?')}><a className="menu-item-web-link" onClick={() => handlerScroll("cobertura")} >¿Qué cubre?</a></li>
						<li className="menu-item-web" onClick={()=>handlerMenuWeb('¿Cómo comprar?')}><a className="menu-item-web-link" onClick={() => handlerScroll("comofunciona")} >¿Cómo comprar?</a></li>
						<li className="menu-item-web" onClick={()=>handlerMenuWeb('¿Cómo asegurarme?')}><a className="menu-item-web-link" onClick={() => handlerScroll("comocomprar")} >¿Cómo asegurarme?</a></li>
						{/* <li className="menu-item-web menu-item-web-btnmas">
							<a id="menu-btn-mas" className="dropdown-trigger menu-item-web-link" onClick={() => deslizarBtnMas("menu-btn-mas", "dropdown1", 1)} data-target="dropdown1">Más
                            <i className="material-icons right menu-icon">arrow_drop_down</i>
							</a>
						</li> */}
						<li className="menu-item-web"><Buttons {...bottonMenu} /></li>
					</ul>
				</div>
			</nav>

			{/* Version mobile */}
			<ul className="sidenav" id="mobile-demo">
				<li className="menu-item-mobile" onClick={()=>handlerMenuMobile('¿Qué cubre?')} ><a onClick={() => handlerScroll("cobertura")}>¿Qué cubre?</a></li>
				<li className="menu-item-mobile" onClick={()=>handlerMenuMobile('¿Cómo comprar?')} ><a onClick={() => handlerScroll("comofunciona")}>¿Cómo comprar?</a></li>
				<li className="menu-item-mobile" onClick={()=>handlerMenuMobile('¿Cómo asegurarme?')} ><a onClick={() => handlerScroll("comocomprar")}>¿Cómo asegurarme?</a></li>
				<li className="menu-item-mobile" onClick={()=>handlerMenuMobile('Condicionado General')} ><a href={condicionado}  target="_blank">Condicionado General</a></li>
				{/* <li className="menu-item-mobile" onClick={()=>handlerMenuMobile('Glosario de términos')} ><a href={condicionado}  target="_blank">Glosaio de Términos</a></li> */}
				<li className="menu-item-mobile" onClick={()=>handlerMenuMobile('Resumen de Póliza')} ><a href={resumenpoliza} target="_blank">Resumen de Póliza</a></li>
				<li className="menu-item-mobile" onClick={() => deslizarNavMobile("mobile-demo")} ><Buttons {...bottonMenu} /></li>
			</ul>

		</header>
	)
}

export default Header
