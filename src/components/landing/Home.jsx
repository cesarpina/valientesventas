import React from 'react'
import Banner from './Banner'
import SeemoreBtn from './commons/SeemoreBtn'
import { deslizarBtnMas } from '../../resources/js/events.js';

const Home = ({handlerCotizador, handlerScroll, handlerShowVideo}) => (
    <section className="HomeBx d-flex justify-content-center mb-full" onClick={()=> deslizarBtnMas("menu-btn-mas", "dropdown1", 0)}>
       
         <div className="SectionHomePastillaPrima" >
            <div>
                <div >
                    <p></p>
                    <p className="pricing">S/. 99</p>
                    <p>MENSUALES</p>
                </div>
            </div>
            <p className="ppventa">Precio de Venta</p>
            <p> con Tarjeta de Crédito/Débito</p>
        </div>
         <Banner      handlerCotizador={handlerCotizador} handlerShowVideo={handlerShowVideo} />
         <SeemoreBtn  handlerScroll={handlerScroll} />
    </section>
 )
 
export default Home