import React from 'react'
import Title from './commons/Title'
import SectionPasos from './commons/SectionPasos'
import Buttons from './commons/Buttons'
import { deslizarBtnMas } from '../../resources/js/events.js'
import ImgFecha from '../../resources/images/sectioncomocomprar/flecha-comocomprar@2x.png'

const SectionComoComprar = ({listacomocomprar,buttons}) => (
    <section id="comocomprar" className="comocomprar d-flex flex-column align-items-center center-align"  onClick={()=> deslizarBtnMas("menu-btn-mas", "dropdown1", 0)}>
        <Title {...listacomocomprar.header} />
        <SectionPasos {...listacomocomprar} />
        <img className="SectionPasosFlechaL" src={ImgFecha} alt="" />
        <img className="SectionPasosFlechaR" src={ImgFecha} alt="" />
        <Buttons {...buttons} />
    </section>
)

export default SectionComoComprar