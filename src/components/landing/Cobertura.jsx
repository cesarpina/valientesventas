import React from 'react'
import exclusionespdf from '../../resources/pdf/ExclusionesRespaldoOncologicoPonleCorazon.pdf'
import Imgsicubre from '../../resources/images/home/sicubre.png'
import Imgnocubre from '../../resources/images/home/nocubre.png'
import ImgLinkFlecha from '../../resources/images/iconos/flecha-link.png'
import { deslizarBtnMas } from '../../resources/js/events.js'

const Cobertura = ({ handlerVerExclusiones }) => (

    <section id="coberturas" className="CoberturaBx d-flex justify-content-center">
        <div className="CoberturaBxItem d-flex flex-wrap mb-flex-column">
            <h4 className="d-flex justify-content-center align-items-center">Nuestra cobertura es así:</h4>
            <p className="textCobert"> 
Si tu cliente no te paga en la fecha pactada, RIMAC te paga. Te abonamos hasta el 95% del total de tu comprobante de pago electrónico emitido (Facturas, recibo por honorarios y boleta). </p>

            <div className="boxCubrimos d-flex justify-content-between mb-flex-column">
                <div className="d-flex flex-column align-items-center">
                    <img src={Imgsicubre} alt="" />
                    <span>SÍ Cubre</span>
                    <p>Comprobantes de pago electrónicos desde S/1000 hasta S/5000 (Tú eliges).</p>
                    <p>Hasta 6 facturas con retraso al año.</p>
                    <p>Sólo comprobantes de pago emitidos en el territorio nacional.</p>
                </div>
                <div className="d-flex flex-column align-items-center">
                    <img src={Imgnocubre} alt="" />
                    <span>NO Cubre</span>
                    <p>Comprobantes de pago en formato fisico.</p>
                    <p>Comprobantes de pago con montos superiores a S/5000.</p>                
                    <p>Comprobantes de pago emitidos antes de la compra del seguro.</p>
                </div>
            </div>


        </div>
    </section>

)

export default Cobertura