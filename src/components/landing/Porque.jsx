import React from 'react'
import Icon1 from '../../resources/images/iconos/spv/landing/1.png'
import Icon2 from '../../resources/images/iconos/spv/landing/2.png'
import Icon3 from '../../resources/images/iconos/spv/landing/3.png'
import Icon4 from '../../resources/images/iconos/spv/landing/4.png'

const Porque = () => (
    <section id="porque" className="boxPorque d-flex flex-column align-items-center center-align">
        <span>¿Por qué RIMAC?</span>
        <div className="boxPqitem d-flex flex-wrap mb-full">
            <div className="boxPqitem-item d-flex align-items-center mb-full mb-justify-content-center">
                <p>Compra 100% digital.</p>
                <img className="" src={Icon1} alt=""/>
            </div>
            <div className="boxPqitem-item d-flex align-items-center mb-full mb-justify-content-center">
                <p>Producto personalizado.</p>
                <img className="" src={Icon2} alt=""/>
            </div>
            <div className="boxPqitem-item d-flex align-items-center mb-full mb-justify-content-center">
                <p>Inmediatez en la respuesta.</p>
                <img className="" src={Icon3} alt=""/>
            </div>
            <div className="boxPqitem-item d-flex align-items-center mb-full mb-justify-content-center">
                <p>Confianza en la marca.</p>
                <img className="" src={Icon4} alt=""/>
            </div>
        </div>
        
        

    </section>
)

export default Porque