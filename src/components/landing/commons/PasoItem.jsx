import React from 'react'
import Tooltip from '@material-ui/core/Tooltip'

const PasoItem = ({imagen, details, clases, tooltip}) => {
    let {container, img, detail} = clases
    let titleToolTip = 
                        <div className="tooltip-funciona-paso2">
                        “Es un examen necesario para validar la presencia de cáncer.<br/>
                        Los documentos deberás presentarlos en las plataformas presenciales”
                            <ul>
                                <li>- Comandante Espinar: Av. Comandante Espinar 689, Miraflores</li>
                                <li>- Torre del Parque : Calle Las Begonias 552, San Isidro</li>
                                <li>- Begonias: Av. Paseo de la República 3505, San Isidro</li>
                            </ul>
                        </div>

    return(
        <div className={container}>
            <img src={imagen} alt="img" className={img} />           
            {tooltip?
                  <Tooltip id="tooltip-details" title={titleToolTip}>
                    <p className={detail}  > {details} </p> 
                  </Tooltip>
                :   <p className={detail}> {details} </p> }
        </div>
    )
}

export default PasoItem