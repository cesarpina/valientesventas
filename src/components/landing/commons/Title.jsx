import React from 'react'

const Title = ({title, subtitle, clases}) => (
    <div className={clases.container}>
        <p className={clases.title}>{title}</p>
        <p className={clases.subtitle}>{subtitle}</p>
    </div>

)

export default Title