import React from 'react'

const Buttons = ({handleOnClick, value, clases, labelgtm}) => (
    <button className={"btnSucces z-depth-1 mb-full waves-effect"+clases} onClick={()=>{handleOnClick(labelgtm)}}>{value}</button>
)

export default Buttons