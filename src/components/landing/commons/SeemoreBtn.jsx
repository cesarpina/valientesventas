import React from 'react';
import ImgSm from '../../../resources/images/home/btn-deslizar.png';

const SeemoreBtn = ({ handlerScroll }) => (
	// btnSeemore d-flex justify-content-center align-items-center
	<a className="btnDeslizarLanding"><img src={ImgSm} alt="" onClick={() => handlerScroll("coberturas")} /></a>
)

export default SeemoreBtn