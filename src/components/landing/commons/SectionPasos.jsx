import React from 'react'
import PasoItem from './PasoItem'

const SectionPasos = ({pasos}) => (
    <div className="SectionPasosBx d-flex mb-flex-column align-items-center">        
        {pasos.map((paso, i)=>(
            <PasoItem key={i} {...paso} />
        ))}
    </div>  
)    

export default SectionPasos