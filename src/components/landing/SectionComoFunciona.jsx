import React from 'react'
import Title from './commons/Title'
import SectionPasos from './commons/SectionPasos'
import Buttons from './commons/Buttons'
import { deslizarBtnMas } from '../../resources/js/events.js'
// import ImgEmail from '../../resources/images/iconos/spv/email.png'

const SectionComoFunciona = ({listacomofunciona,buttons}) => (
    <section id="comofunciona" className="comofunciona d-flex flex-column align-items-center center-align"  onClick={()=> deslizarBtnMas("menu-btn-mas", "dropdown1", 0)} >
        <Title {...listacomofunciona.header} />
        <SectionPasos {...listacomofunciona} />
        <div className="SectionPasosBx SectionPasosLine"></div>
        {/* <div className="mb-full boxInfo z-depth-1 d-flex align-items-center justify-content-around">
            <img className="imgIcon" src={ImgEmail} alt="" />
            <p>Recibirás la confirmación de tu pago por correo, y estarás listo para usar tu Seguro para Valientes de RIMAC.</p>
        </div> */}
        <Buttons {...buttons} />
    </section>
)

export default SectionComoFunciona