import React from 'react'
import Button from './commons/Buttons'
import ImgBanner from '../../resources/images/iconos/spv/superhero.png'

const Banner = ({handlerCotizador, handlerShowVideo}) => {
    let bottonMenu = {
        handleOnClick : handlerCotizador ,
        value  : 'Cómpralo Online',
        clases : ' btn-landing-comprar btn-landing-comprar--home',
        labelgtm: 'Cómpralo Online - Banner Principal'
    }

    let bottonVideo = {
        handleOnClick : handlerShowVideo ,
        value  : 'Mirar el Video',
        clases : ' btn-landing-comprar btn-video',
        labelgtm : 'Mirar el video'
    }

    return(
        <section className="bannerBox d-flex mb-flex-column mb-full">

            <div className="bannerBox-text mb-full mb-flex-column">
                <p className="BannerRutaHome mb-full">Rimac Home > Seguros de Riesgo > Seguro Para Valientes</p>
                <p className="BannerItemProduct">SEGURO PARA VALIENTES </p><br/>  
                {/* <p>Pre Venta exclusiva con invitación</p> */}
                 
                <div className="BannerItemBx d-flex flex-column">
                    <h2>Que el retraso de pago de tus clientes no afecte tu crecimiento</h2>
                    <p className="BannerItemPricing">¡Te pagamos en caso tus clientes se retrasen en el pago! </p>
                    <div className="d-flex">
                        <Button {...bottonMenu} />
                        <Button {...bottonVideo} />
                    </div>
                </div>
                {/* <div className="BannerImgBx d-flex"><img alt="" src={Imagenbnr} /></div> */}
                {/* <div className="BannerBarBx d-flex justify-content-center flex-column align-items-center">
                    <h3>¡Asegúrate online desde S/14.90 al mes!</h3>
                    <span>Desde los 18 a 65 años</span>
                </div> */}
            </div>
            <div className="bannerBox-img d-flex justify-content-center mb-full">
                <img src={ImgBanner} alt=""/>
            </div>

            
            
        </section>
    )
}

export default Banner