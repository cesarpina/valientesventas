import React from 'react';
import Title from '../../components/cotizador/commons/Title';
// import imgCns from '../../resources/images/iconos/cns.png';
import ponlecorazon from '../../resources/images/cotizador/ponlecorazon-info.png';
import Pasos from '../commons/Pasos'
import GTM from '../../utils/GTM'

const PageInfo = () => {
	let title = <p className="bxTitle mb-full w60 mar-0-auto">¡Gracias por tus comentarios!<br />Los tomaremos en cuenta para crear el mejor producto para ti</p>
   
	function handlerDonar(){
		GTM.pasosCotizador('Comentarios - Clic Botón', 'Donar Online')
	}

	return (
		<section className="container">
			<div className="boxWhite">
				<Pasos paso={2} />
				<div className="bxInformativo d-flex justify-content-center flex-column align-items-center">
					<Title value={title} />
					<span className="frmSubt d-flex justify-content-center">Esto también te puede interesar</span>
					<div className="bxInformativo-item mb-flex-column mb-full d-flex justify-content-between">
						<div className="bxinfox mb-full d-flex flex-column justify-content-between">
							{/* <img className="mb-full flex-shrink-0" src={ponlecorazon} alt="" /> */}
							<img className="" src={ponlecorazon} alt="" />

							<p>La Fundación Peruana de Cáncer trabaja para ayudar a personas de bajos recursos afectados por el cáncer, acogiéndolos y dándoles asistencia integral* en el Albergue Frieda Heller, durante todo el tiempo que dure su tratamiento en el Instituto Nacional de Enfermedades Neoplásicas (INEI). Tu donación puede cambiar sus vidas. <b>Dona esperanza!</b></p>							
							<a href="https://fpc.pe/nosotros/ponle-corazon/" target="_black" rel="nofollow" onClick={handlerDonar} >DONAR ONLINE <i className="material-icons">trending_flat</i></a>
						</div>

					</div>
				</div>

			</div>

		</section>
	)
}

export default PageInfo