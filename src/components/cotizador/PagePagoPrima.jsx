import React from 'react';
import Input from '../../components/cotizador/commons/Input';
import Button from '../../components/cotizador/commons/Button';
import { fetchCONSULTAEMPRESA } from '../../actions/consultarDatosEmpresa';
import Pasos from '../commons/Pasos';
import GTM from '../../utils/GTM'; 
import { connect } from 'react-redux';

const PagePagoPrima = ({ handlerPaso, nombre, prima, handlerCot, handlerOnChange}) => {	

	this.state = {
		validruc: false,
	}   

	GTM.navegarCotizador('Precio')

	


	return (
		// <div className="d-flex flex-column">
		// 	<Pasos paso={1} />
		// 		<section className="container d-flex flex-column">			
				

		// 		<div className="d-flex justify-content-between">
		// 			<div className="boxWhite">
						
		// 				<div className="bxPagoPrima d-flex justify-content-center flex-column align-items-center">
							
		// 					<h2 className="title">Completa los siguientes datos sobre tu negocio</h2>
		// 					<div className="miNegocio d-flex flex-wrap justify-content-between">
		// 						<b className="labelForm cienp d-flex justify-content-start">Mi negocio</b>
		// 						<Input
		// 							clase="treintap"
		// 							name="rucCli"
		// 							holder="RUC"
		// 							handlerOnChange={this.handlerOnChange}
		// 							longitud="11"
		// 							check={this.state.validruc} />
		// 						<Input
		// 							clase="treintap"
		// 							name="actividad-economica"
		// 							value={this.state.Actividad}
		// 							holder="Actividad económica"
		// 							/>
		// 						<Input
		// 							clase="treintap"
		// 							name="facturacion-promedio"
		// 							holder="Facturación promedio al mes"
		// 							/>
		// 						<Input
		// 							clase="cienp"
		// 							name="direccion-fiscal"
		// 							holder="Dirección fiscal"
		// 							/>
		// 						<Input
		// 							clase="treintap"
		// 							name="distrito"
		// 							holder="Distrito"
		// 							/>
		// 						<Input
		// 							clase="treintap"
		// 							name="provincia"
		// 							holder="Provincia"
		// 							/>
		// 						<Input
		// 							clase="treintap"
		// 							name="departamento"
		// 							holder="Departamento"
		// 							/>
		// 					</div>
		// 					<div className="misClientes d-flex flex-wrap justify-content-start">
		// 						<b className="labelForm cienp d-flex justify-content-start">Mis Clientes</b>
		// 						<Input
		// 							clase="treintap btnInput"
		// 							name="ruc-comprador"
		// 							holder="Ingresa su RUC"
		// 							/>
		// 						<Button
		// 							clase="btn btnAction btnBrowser"
		// 							value="+" />

		// 						<div className="boxResultRUC cienp d-flex flex-wrap justify-content-between">

		// 							<div className="boxResultRUC-item mb-full d-flex justify-content-between align-items-center">
		// 								<div className="boxResultRUC-item-cal d-flex flex-column align-items-center">
		// 									<i class="material-icons color-green">check</i>
		// 									<span>Califica</span>
		// 								</div>
		// 								<div className="boxResultRUC-item-detail cienp d-flex mb-full flex-column">
		// 									<p><b>RUC: </b>10476549486</p>
		// 									<p><b>RS: </b>Panadería Cesarin SAC</p>
		// 								</div>
		// 								<Button
		// 								clase="btnDrop"
		// 								value="-" />
		// 							</div>

		// 							<div className="boxResultRUC-item mb-full d-flex justify-content-between align-items-center">
		// 								<div className="boxResultRUC-item-cal d-flex flex-column align-items-center">
		// 									<i class="material-icons color-red">close</i>
		// 									<span>No Califica</span>
		// 								</div>
		// 								<div className="boxResultRUC-item-detail cienp d-flex mb-full flex-column">
		// 									<p><b>RUC: </b>10476549486</p>
		// 									<p><b>RS: </b>Panadería Cesarin SAC</p>
		// 								</div>
		// 								<Button
		// 								clase="btnDrop"
		// 								value="-" />
		// 							</div>


		// 							<div className="boxResultRUC-item mb-full d-flex justify-content-between align-items-center">
		// 								<div className="boxResultRUC-item-cal d-flex flex-column align-items-center">
		// 									{/* <img className="imgIcoLite" src={ImgCheck} alt="" /> */}
											


		// 								<div class="preloader-wrapper small active">
		// 									<div class="spinner-layer spinner-green-only">
		// 										<div class="circle-clipper left">
		// 											<div class="circle"></div>
		// 										</div>
		// 										<div class="gap-patch">
		// 											<div class="circle"></div>
		// 										</div>
		// 										<div class="circle-clipper right">
		// 											<div class="circle"></div>
		// 										</div>
		// 									</div>
		// 								</div>
											
		// 									<span>Califica</span>
		// 								</div>
		// 								<div className="boxResultRUC-item-detail cienp d-flex mb-full flex-column">
		// 									<p><b>RUC: </b>20476549486</p>
		// 									<p><b>RS: </b>Yobel Logistic SAC</p>
		// 								</div>
		// 								<Button
		// 								clase="btnDrop"
		// 								value="-" />
		// 							</div>

															
		// 						</div>
		// 					</div>



		// 					<Button
		// 						clase="btn btnAction btnAction"
		// 						value="CONTINUAR"
		// 						onClick={()=>this.handlerCot}
		// 					/>




		// 					{/* {<a className="link d-flex align-items-center" onClick={()=>handlerPaso(5)}>Checkout</a>} */}

		// 				</div>

		// 			</div>

		// 			<div className="cuarentaycincop ml20">
		// 				<div className="boxInfor boxWhite">
		// 					<span className="titleHelp cienp d-flex justify-content-center">¿NECESITAS AYUDA?</span>
		// 					<p><b>Actividad económica: </b>Deberás considerar la misma que registraste en SUNAT como actividad principal.</p>
		// 					{/* <p><b>Facturación más alta:</b> El monto de tu factura más alta del mes.</p> */}
		// 					<p><b>Facturación promedio:</b> El monto promedio de tus facturas al mes.</p>
		// 					<p><b>Dirección fiscal: </b>Dirección de negocio registrada en la SUNAT.</p>
		// 					<p><b>Tope de facturación: </b>Rango de montos que RIMAC te puede cubrir.</p>
		// 					<p><b>Precio: </b>Monto único mensual a pagar.</p>
		// 				</div>
		// 				<div className="boxAlert boxWhite mt20">
		// 					<span className="titleImportant cienp d-flex justify-content-start">IMPORTANTE</span>
		// 					<p>Cubrimos máximo 06 comprobantes de pago electrónicos retrasados al año. (1 por mes)</p>
		// 					<p>Los comprobantes de pago electrónicos deben ser emitidos durante la vigencia del seguro.</p>
		// 				</div>
		// 			</div>

		// 		</div>

				

			
		// 	</section>
			
		// </div>
		<h1>...2</h1>


	)
}
const mapStateToProps = store => {
	return {
		
	}
}

const mapDispatchToProps = dispatch => {
	return {
		consultarDatosEmpresa: (dni) => dispatch(fetchCONSULTAEMPRESA(dni)),
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(PagePagoPrima)