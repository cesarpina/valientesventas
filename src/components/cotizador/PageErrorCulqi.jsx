import React from 'react';
import Title from '../cotizador/commons/Title';
// import iconRefresh from '../../resources/images/iconos/refresh-button.svg'


const PageErrorCulqi = ({ value, handlerPaso, showVolver }) => {

	let title = <p className="bxTitle mb-full text-center w100 mar-0-auto">¡Ups!<br />Hemos tenido un problema procesando tu compra.</p>

	return (
		<section className="container">
			<div className="boxWhite">
				<div className="bxPageError d-flex flex-column align-items-center">
					<Title value={title} />
					<div className="d-flex flex-column align-items-center w60 mb-full">
						<p className="rectangle mar-b-20 text-center">{value}</p>
						{showVolver ? <a className="link d-flex align-items-center" onClick={()=>handlerPaso(7)}>Intentar con otra tarjeta <i className="material-icons dp48">refresh</i></a> : null}
					</div>
				</div>
			</div>
		</section>

	)

}


export default PageErrorCulqi