import React from 'react';

const PreguntaItem = ({ pregunta, respuesta, handlerPregunta }) => {
	return (
		<div className="bxQuestion d-flex mb-flex-column">
			<p>{pregunta}</p>
			<div className="bxQuestion-item d-flex">
				<label><input className="with-gap" type="radio" name={respuesta.name} value="S" checked={respuesta.positivo} onClick={(e) => handlerPregunta(e, respuesta.orden)} /> <span>Si</span></label>
				<label><input className="with-gap" type="radio" name={respuesta.name} value="N" checked={respuesta.negativo} onClick={(e) => handlerPregunta(e, respuesta.orden)} /> <span>No</span></label>
			</div>
		</div>
	)
}

export default PreguntaItem
