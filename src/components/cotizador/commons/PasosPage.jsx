import React  from 'react'

const PasosPage = ({nropaso}) => {
    let paso1, paso2, paso3, paso4 = "box-pasos-plomo"
    ["paso"+nropaso] = "box-paso1-verde"

    return(
        <section>
            <div className={paso1} ></div>
            <div className={paso2} ></div>
            <div className={paso3} ></div>
            <div className={paso4} ></div>
        </section>
    )
}

export default PasosPage