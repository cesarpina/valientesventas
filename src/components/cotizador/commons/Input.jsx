import React from 'react';
import imgcheck from '../../../resources/images/iconos/check.png'
import calendar from '../../../resources/images/iconos/calendar.png' 
import error from '../../../resources/images/iconos/error.png'
const Input = ({ clase, name, value, handlerOnChange, holder, check, disableCampo, longitud ,minlog, classnrodocctz }) => (
	<div className={"input-field input-contratante "+clase}>
		<input
			type='text'
			className={"validate input-contratante mb-full cienp"}
			id={name}
			name={name}
			value={value}
			maxLength={longitud}
			minLength={minlog}
			disabled={disableCampo}
			onChange={(e) => handlerOnChange(e)} /> 
			{name==="fechanacimiento" ? <img className="input-check input-img-calendar" src={calendar} alt="" /> : null  }
			{check===true? <img className={"input-check" + (classnrodocctz?classnrodocctz:"")} src={imgcheck} alt="" /> : (check==="error" ? <img className="input-check input-check-error" src={error} alt="" />  : null) }
			<label id={"label-"+name} htmlFor={name} className={"label-input"+( value?" active":"" )} >{holder}</label>
	</div>
)


export default Input