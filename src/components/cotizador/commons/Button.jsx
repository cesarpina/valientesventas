import React from 'react'

const Button = ({handlerOnClick, value, clase}) => (
    <button className={clase} onClick={()=>handlerOnClick()} >{value}</button>
)

export default Button