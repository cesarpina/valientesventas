import React from 'react';

const Textarea = ({ name, value, handlerOnChange, clase, holder }) => (
	<input
		type="textarea"
		className={clase}
		name={name}
		value={value}
		placeholder={holder}
		onChange={(e) => handlerOnChange(e)} />
)

export default Textarea