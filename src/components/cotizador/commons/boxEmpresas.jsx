import React from 'react';

const boxEmpresa = ({ rucCompradores, razonSocialCompradores }) => (
	<div className="boxResultRUC-item mb-full d-flex justify-content-between align-items-center">
        <div className="boxResultRUC-item-cal d-flex flex-column align-items-center">
            <i class="material-icons color-green">check</i>
            <span>Califica</span>
        </div>
        <div className="boxResultRUC-item-detail cienp d-flex mb-full flex-column">

            <p><b>RUC: </b>${rucCompradores}</p>
            <p><b>RS: </b>${razonSocialCompradores}</p>
        </div>
    </div>
)


export default boxEmpresa

