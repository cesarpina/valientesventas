import React from 'react';
import Input from '../cotizador/commons/Input'
import Button from '../cotizador/commons/Button'
import Pasos from '../commons/Pasos'
import UtilsJs from '../../utils/Funciones'

const PageCalculoPrima = ({ handlerPaso, nombre }) => {

	let title = <p className="bxTitle mb-full w100 mar-b-20"><b>{nombre},</b> no podemos darte este seguro</p>
	
		

	return (
		
		// 	<div>
		// 		<Pasos paso={2} />
		// 		<section className="container d-flex flex-column">			
		// 		<div className="d-flex justify-content-between">
		// 			<div className="boxWhite">
						
		// 				<div className="bxPagoPrima d-flex justify-content-center flex-column align-items-center">
							
		// 					<h2 className="title">Completa los siguientes datos sobre tu negocio</h2>
		// 					<div className="miNegocio d-flex flex-wrap justify-content-between">
		// 						<b className="labelForm cienp d-flex justify-content-start">Mi negocio</b>
		// 						<Input
		// 							clase="treintap"
		// 							name="rucCli"
		// 							holder="RUC"
		// 							handlerOnChange={this.handlerOnChange}
		// 							longitud="11"
		// 							check={this.validruc} />
		// 						<Input
		// 							clase="treintap"
		// 							name="actividad-economica"
		// 							value={this.Actividad}
		// 							handlerOnChange={this.handlerOnChange}
		// 							holder="Actividad económica"
		// 							/>
		// 						<Input
		// 							clase="treintap"
		// 							name="facturacion-promedio"
		// 							longitud="10"
		// 							handlerOnChange={this.handlerOnChange}
		// 							holder="Facturación promedio al mes"
		// 							/>
		// 						<Input
		// 							clase="cienp"
		// 							name="direccion-fiscal"
		// 							value={this.Direccion}
		// 							handlerOnChange={this.handlerOnChange}
		// 							holder="Dirección fiscal"
		// 							/>
		// 						<Input
		// 							clase="treintap"
		// 							name="distrito"
		// 							value={this.Distrito}
		// 							handlerOnChange={this.handlerOnChange}
		// 							holder="Distrito"
		// 							/>
		// 						<Input
		// 							clase="treintap"
		// 							name="provincia"
		// 							value={this.Provincia}
		// 							handlerOnChange={this.handlerOnChange}
		// 							holder="Provincia"
		// 							/>
		// 						<Input
		// 							clase="treintap"
		// 							name="departamento"
		// 							value={this.Departamento}
		// 							handlerOnChange={this.handlerOnChange}
		// 							holder="Departamento"
		// 							/>
		// 					</div>
		// 					<div className="misClientes d-flex flex-wrap justify-content-start">
		// 						<b className="labelForm cienp d-flex justify-content-start">Mis Clientes</b>
		// 						<Input
		// 							clase="treintap btnInput"
		// 							name="ruc-comprador"
		// 							holder="Ingresa su RUC"
		// 							value={this.rucCompradores}
		// 							handlerOnChange={this.handlerOnChange}
		// 							longitud="11"
		// 							check={this.validrucComprador} 
		// 							/>
		// 						<Button
		// 							clase="btn btnAction btnBrowser"
		// 							handlerOnClick={() => this.handlerCS(this.rucCompradores)}
		// 							value="+" />

		// 						<div  className="boxResultRUC cienp d-flex flex-wrap justify-content-between">

		// 							{
		// 								(this.array || [] || null).map((obj, index) => {
		// 									return (

		// 										<div id="i1" className=" boxResultRUC-item mb-full d-flex justify-content-between align-items-center">
		// 											<div className="boxResultRUC-item-cal d-flex flex-column align-items-center">
		// 												<i class="material-icons color-green">check</i>
		// 												<span>Califica</span>
		// 											</div>
		// 											<div className="boxResultRUC-item-detail cienp d-flex mb-full flex-column">
		// 												<p><b>RUC: </b>{obj.rucCompradores}</p>
		// 												<p><b>RS: </b>{obj.razonSocialCompradores}</p>
		// 											</div>
		// 											<Button
		// 											clase="btnDrop"
		// 											value="-" />
		// 										</div>

		// 									)
		// 								})
		// 							}

		// 							{/* <div id="i1" className=" boxResultRUC-item mb-full d-flex justify-content-between align-items-center">
		// 								<div className="boxResultRUC-item-cal d-flex flex-column align-items-center">
		// 									<i class="material-icons color-green">check</i>
		// 									<span>Califica</span>
		// 								</div>
		// 								<div className="boxResultRUC-item-detail cienp d-flex mb-full flex-column">
		// 									<p><b>RUC: </b>10476549486</p>
		// 									<p><b>RS: </b>Panadería Cesarin SAC</p>
		// 								</div>
		// 								<Button
		// 								clase="btnDrop"
		// 								value="-" />
		// 							</div>

		// 							<div id="i2" className=" boxResultRUC-item mb-full d-flex justify-content-between align-items-center">
		// 								<div className="boxResultRUC-item-cal d-flex flex-column align-items-center">
		// 									<i class="material-icons color-red">close</i>
		// 									<span>No Califica</span>
		// 								</div>
		// 								<div className="boxResultRUC-item-detail cienp d-flex mb-full flex-column">
		// 									<p><b>RUC: </b>10476549486</p>
		// 									<p><b>RS: </b>Panadería Cesarin SAC</p>
		// 								</div>
		// 								<Button
		// 								clase="btnDrop"
		// 								value="-" />
		// 							</div>


		// 							<div id="i3" className=" boxResultRUC-item mb-full d-flex justify-content-between align-items-center">
		// 								<div className="boxResultRUC-item-cal d-flex flex-column align-items-center">

		// 								<div class="preloader-wrapper small active">
		// 									<div class="spinner-layer spinner-green-only">
		// 										<div class="circle-clipper left">
		// 											<div class="circle"></div>
		// 										</div>
		// 										<div class="gap-patch">
		// 											<div class="circle"></div>
		// 										</div>
		// 										<div class="circle-clipper right">
		// 											<div class="circle"></div>
		// 										</div>
		// 									</div>
		// 								</div>
											
		// 									<span>Califica</span>
		// 								</div>
		// 								<div className="boxResultRUC-item-detail cienp d-flex mb-full flex-column">
		// 									<p><b>RUC: </b>20476549486</p>
		// 									<p><b>RS: </b>Yobel Logistic SAC</p>
		// 								</div>
		// 								<Button
		// 								clase="btnDrop"
		// 								value="-" />
		// 							</div> */}

															
		// 						</div>
		// 					</div>



		// 					<Button
		// 						clase="btn btnAction btnAction"
		// 						value="CONTINUAR"
		// 						handlerOnClick={() => this.handlerNext(this.state.score)} />




		// 					{/* {<a className="link d-flex align-items-center" onClick={()=>handlerPaso(5)}>Checkout</a>} */}

		// 				</div>

		// 			</div>

		// 			<div className="cuarentaycincop ml20">
		// 				<div className="boxInfor boxWhite">
		// 					<span className="titleHelp cienp d-flex justify-content-center">¿NECESITAS AYUDA?</span>
		// 					<p><b>Actividad económica: </b>Deberás considerar la misma que registraste en SUNAT como actividad principal.</p>
		// 					{/* <p><b>Facturación más alta:</b> El monto de tu factura más alta del mes.</p> */}
		// 					<p><b>Facturación promedio:</b> El monto promedio de tus facturas al mes.</p>
		// 					<p><b>Dirección fiscal: </b>Dirección de negocio registrada en la SUNAT.</p>
		// 					<p><b>Tope de facturación: </b>Rango de montos que RIMAC te puede cubrir.</p>
		// 					<p><b>Precio: </b>Monto único mensual a pagar.</p>
		// 				</div>
		// 				<div className="boxAlert boxWhite mt20">
		// 					<span className="titleImportant cienp d-flex justify-content-start">IMPORTANTE</span>
		// 					<p>Cubrimos máximo 06 comprobantes de pago electrónicos retrasados al año. (1 por mes)</p>
		// 					<p>Los comprobantes de pago electrónicos deben ser emitidos durante la vigencia del seguro.</p>
		// 				</div>
		// 			</div>

		// 		</div>
		// 	</section>
		// </div>

		<h1>...</h1>
	)
}

export default PageCalculoPrima
