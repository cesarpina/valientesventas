import React from 'react';
import Button from '../../components/cotizador/commons/Button';
import Pasos from '../commons/Pasos';
import GTM from '../../utils/GTM';
// import {ROUTE} from '../../utils/Constantes'

const PageComprarCtz = ({ handlerPaso, nombre, prima}) => {	

	GTM.navegarCotizador('Precio')

	return (
		<div className="d-flex flex-column">
			<Pasos paso={2} />
				<section className="container d-flex flex-column">			
				
				<div className="d-flex justify-content-between">
					<div className="boxWhite">
						
						<div className="bxPagoPrima d-flex justify-content-center flex-column align-items-center">
							
							<h2 className="title">Escoge el rango de tu factura, boleta o recibo por honorarios</h2>
							
							<div className="bxInput d-flex flex-column justify-content-start align-items-center cienp">
									<b className="labelForm d-flex justify-content-start cienp">Mi facturación</b>
									
									<div className="d-flex align-items-center justify-content-start cienp">
										<select className="mb-full cienp mr20" name="idpgenero" value="">
											<option value="" >Monto máximo por factura</option>
											<option value="M">1000</option>
											<option value="F">2000</option>
											<option value="F">3000</option>
											<option value="F">4000</option>
											<option value="F">5000</option>
										</select>
									</div>

									
							</div>

							<div class="cienp">
								<div className="cienp d-flex justify-content-between">
									<div className="boxPrice cuarentap">
										<span className="mb-full cienp d-flex justify-content-center">CUBRIMOS</span>
										<div className="boxPriceCost boxPorcentaje">
											<b className="mb-full cienp d-flex justify-content-center">50%</b>
											<p className="mb-full cienp d-flex justify-content-center">de tu factura</p>
										</div>
									</div>
									<div className="boxPrice cuarentap">
										<span className="mb-full cienp d-flex justify-content-center">PRECIO</span>
										<div className="boxPriceCost">
											<b className="mb-full cienp d-flex justify-content-center">S/ 00.00</b>
											<p className="mb-full cienp d-flex justify-content-center">mensuales</p>
										</div>
									</div>
								</div>
							</div>



							<Button
								clase="btn btnAction btnAction mt20"
								value="CONTINUAR CON LA COMPRA"
								handlerOnClick={() => this.props.handlerCot(this.state.nrodni)}
							/>
							{/* {<a className="link d-flex align-items-center" onClick={()=>handlerPaso(7)}>Checkout</a>} */}

						</div>

					</div>

					<div className="cuarentaycincop ml20">
						<div className="boxInfor boxWhite">
							<span className="titleHelp cienp d-flex justify-content-center">¿NECESITAS AYUDA?</span>
							<p><b>Actividad económica: </b>Deberás considerar la misma que registraste en SUNAT como actividad principal.</p>
							{/* <p><b>Facturación más alta:</b> El monto de tu factura más alta del mes.</p> */}
							<p><b>Facturación promedio:</b> El monto promedio de tus facturas al mes.</p>
							<p><b>Dirección fiscal: </b>Dirección de negocio registrada en la SUNAT.</p>
							<p><b>Tope de facturación: </b>Rango de montos que RIMAC te puede cubrir.</p>
							<p><b>Precio: </b>Monto único mensual a pagar.</p>
						</div>
						<div className="boxAlert boxWhite mt20">
							<span className="titleImportant cienp d-flex justify-content-start">IMPORTANTE</span>
							<p>Cubrimos máximo 06 comprobantes de pago electrónicos retrasados al año. (1 por mes)</p>
							<p>Los comprobantes de pago electrónicos deben ser emitidos durante la vigencia del seguro.</p>
						</div>
					</div>

				</div>

				

			
			</section>
			
		</div>



	)
}

export default PageComprarCtz