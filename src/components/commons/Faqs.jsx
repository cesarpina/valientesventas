import React, { Component } from 'react';
import {Collapsible, CollapsibleItem} from 'react-materialize';


class Faqs extends Component {
	constructor(props){
		super(props)
		this.state = {
            bxt1: true, // variable 1
            bxt2: false, // variable 1

		}
	}

	componentDidMount() {
		console.log("Inicializador");
	}

    handlerBx = (n) => {
        
        let nf = 5; // cantidad de opciones
        for(var i=0;i<nf;i++){
			this.setState({ ["bxt" + i]: false })
        }
        this.setState({ ["bxt" + n]: true })

	}

	render() {
		
		return (
			
			<section className="full bwhite">
                <div className="d-flex flex-wrap bxFaqs container">
                    <div className="d-flex flex-column bxTitles">
                        <span onClick={() => {this.handlerBx(1)}} className={"bxTitlesItem" + ( this.state.bxt1===true?" active": "") }>Coberturas</span>
                        {/* <span onClick={() => {this.handlerBx(2)}} className={"bxTitlesItem" + ( this.state.bxt2===true?" active": "") }>Documentos</span> */}
                        <i></i>
                    </div>
                    <div className="d-flex flex-column bxTogles">

                        <div className={"none" + (this.state.bxt1===true?" active": "")}>
                            <div className="bxCoberturas d-flex flex-column"> 
                                <b>Preguntas Frecuentes</b>
                                <Collapsible accordion defaultActiveKey={0}>
                                    <CollapsibleItem className="bxCollapItem" header='¿En qué consiste mi cobertura de Seguro para Valientes?' icon='chevron_right'>
                                    Si uno de tus clientes ubicados en Perú, se retrasan en pagarte un comprobante de pago (factura, boleta o recibo por honorarios) que le hayas emitido de manera electrónica, nosotros nos encargaremos de pagarte un porcentaje del total del importe de dicho comprobante de pago, lo cual haremos conforme al plazo y términos que hayas contratado.
                                    </CollapsibleItem>
                                    <CollapsibleItem className="bxCollapItem" header=' ¿Qué es el retraso en el pago del comprobante?' icon='chevron_right'>
                                    Retraso en el pago del comprobante se considera desde el día siguiente a la fecha límite fijada para el pago de dicho comprobante. Los comprobantes válidos para poder contar con la cobertura, son los que se hayan generado a través del Sistema de Emisión Electrónica SOL (SEE-SOL) de la SUNAT o en el Sistema de Emisión Electrónica del Contribuyente (SEEC).
                                    </CollapsibleItem>
                                    <CollapsibleItem className="bxCollapItem" header=' ¿Qué sucede si no realizo el pago mensual de mi seguro?' icon='chevron_right'>
                                    En caso no te encuentres al día en el pago de tu seguro nosotros te mantendremos al tanto y te enviaremos alertas por correo electrónico. Sin embargo, ten en cuenta que este seguro se extinguirá en caso transcurran 90 días calendarios contados desde la fecha en que incumpliste con el pago de tu seguro.
                                    </CollapsibleItem>
                                    <CollapsibleItem className="bxCollapItem" header='¿En qué casos podrían pedirme la devolución del pago realizado por el comprobante de pago retrasado?' icon='chevron_right'>
                                        <p> Si luego de hacer el depósito en tu cuenta, determinamos que no correspondía pagarte debido a:</p>
                                        <p>a) La existencia de fraude. </p>
                                        <p>b) Tu cliente no reconoce a RIMAC como su nuevo acreedor.</p>
                                        <p>c) Tu cliente no desea pagar a RIMAC porque no recibió el producto o no estuvo de acuerdo con el servicio recibido o producto entregado.</p>
                                        <p>En cualquiera de los casos antes descritos, deberás devolvernos inmediatamente el pago realizado, sin perjuicio de las acciones legales que correspondan.</p>
                                    </CollapsibleItem>
                                </Collapsible>
                            </div>
                        </div>

                        <div className={"none" + (this.state.bxt2===true?" active": "")}>
                            <div className="bxCoberturas d-flex flex-column"> 
                                <b>Preguntas Frecuentes</b>
                                <Collapsible accordion defaultActiveKey={0}>
                                    <span>Condicionado General</span>
                                </Collapsible>
                            </div>
                        </div>
                        
                    </div>
                </div>
			</section>


			
		)
	}

}


export default Faqs
