import React from 'react'
import logo from '../../resources/images/header/logo.png'

const Loading = () => (
    <div id="preloader" >
        <div className="logo-loader">
            <img src={logo} alt="logo" />
        </div>
        <div id="status"></div>
    </div>
)
export default Loading