import React from 'react'

const LoadingProcesando = ({imagen}) => (
    <div className="modal-procesa-compra">
        <img className="modal-imagen" src={imagen} alt="" />
    </div>
)

export default LoadingProcesando