import React, { Component } from 'react';
import footerface from '../../resources/images/footer/footer-face.png';
import footerin from '../../resources/images/footer/footer-in.png';
import footerlogo from '../../resources/images/footer/footer-logo.jpg';
import footertwiter from '../../resources/images/footer/footer-twitter.png';
import logo from '../../resources/images/footer/logo.png';
import rimacgoogleplay from '../../resources/images/footer/rimac-googleplay.png';
import rimacstore from '../../resources/images/footer/rimac-store.png';
import ModalInformativo from './ModalInformativo';
import TerminosCondiciones from './TerminosCondiciones';

class Footer extends Component {
	state = {
		showModal: false,
		contenido: <TerminosCondiciones />
	}


	handlerCloseModal = () => {
		this.setState({ showModal: false })
	}

	handlerOpenModal = () => {
		this.setState({ showModal: true })
	}
	handlerFaqs = () => {
		this.props.handlerPaso(8);
		// this.props.history.push({pathname : ROUTE.URL_COMPRAR});
	}

	render() {
		return (
			<footer>
				<div className="footer-logo">
					<img alt="Rimac" src={footerlogo} />
				</div>
				<div className="footer-terminos_condiciones">
					<p><a className="fancybox-footer" onClick={this.handlerOpenModal} >Términos y condiciones</a> - RIMAC Seguros y Reaseguros . Todos los derechos reservados.</p>
				</div>
				<div className="footer-article">
					<div className="article-uno">
						<div className="logo-rimac">
							<img src={logo} alt="" />
						</div>
						<div className="direccion">
							<p>Calle Amador Merino Reina 339</p>
							<p>01 411-300</p>
						</div>
						<div className="compartir">
							<img src={footerface} alt="face" />
							<img src={footerin} alt="in" />
							<img src={footertwiter} alt="tittwer" />
						</div>
					</div>
					<div className="article-dos">
						<div className="footer-title">RIMAC Personas</div>
						<div className="footer-detail">
							<p>Seguro de Salud</p>
							<p>Seguro de Vehicular</p>
							<p>Seguro de Vida</p>
							<p>Seguro de Hogar</p>
							<p>Seguro de Jubilados</p>
							<p>SOAT</p>
							<p>Viajes</p>
						</div>
					</div>
					<div className="article-tres">
						<div className="footer-title">RIMAC Empresas</div>
						<div className="footer-detail">
							<p>Para mi patrimonio</p>
							<p>SCTR</p>
							<p className="mascara-tres">a</p>
						</div>
					</div>
					<div className="article-cuatro">
						<div className="footer-title">RIMAC App</div>
						<div className="footer-detail-app">
							<img src={rimacstore} alt="rimac" />
							<img className="img-play" src={rimacgoogleplay} alt="rimac" />
						</div>
					</div>
					<div className="article-cinco">
						<div className="footer-title">¿Tienes dudas?</div>
						<div className="contactos">
							<div className="lima footer-detail">
								<p>LIMA</p>
								<p className="footer-title">411-1111</p>
							</div>
							<div className="provincia footer-detail">
								<p>PROVINCIA</p>
								<p className="footer-title">0800-41111</p>
							</div>
						</div>
						<div className="atencion footer-detail">
							<p>De Lunes a Viernes de: 9am a 6pm</p>
							<p>Sábados y Feriados: 9am a 1pm</p>
							<p>pamela.nogales@rimac.com.pe</p>
						</div>
					</div>
				</div>
				<ModalInformativo contenido={this.state.contenido}
					showModal={this.state.showModal}
					handlerCloseModal={this.handlerCloseModal} />
			</footer>

		)
	}
}

export default Footer;
