import React from 'react'
import Logoi from '../../resources/images/header/logo.png'

const Header = () => (
    <header className="d-flex justify-content-center align-items-center">
        <div className="container-fluid d-flex justify-content-start align-items-center">
            <img src={Logoi} alt="Logo Rimac" />
        </div>
    </header>
)

export default Header