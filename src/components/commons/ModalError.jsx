import React from 'react'
import ReactModal from 'react-modal';

const ModalError = ({contenido, title, handlerCloseModal,showModal}) => (
     <div>
        <ReactModal
            isOpen={showModal}
            contentLabel="Modal Error"  
            onRequestClose={handlerCloseModal}
            ariaHideApp={false} 
            className="modalError">   
            {title}
            {contenido}<br/><br/>
            <button className="btn btnCancel" onClick={handlerCloseModal}>Aceptar</button>
        </ReactModal>
    </div>
)

ReactModal.defaultStyles.overlay.backgroundColor = 'rgba(0, 0, 0, 0.8)';
export default ModalError;