import React from 'react'
import shape from '../../resources/images/iconos/shape.png';

const BtnVolver = ({handlerAnterior}) => (
    <a onClick={handlerAnterior} className="btn-cotizador-volver">
        <img src={shape} alt="" />
    <p>Volver</p></a>
)

export default BtnVolver;
 