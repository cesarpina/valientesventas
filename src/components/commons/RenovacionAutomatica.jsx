import React from 'react'

const RenovacionAutomatica = () => (
    <div className="terminos-condiciones">
        <h2>Condiciones de renovación automática</h2>
        <h3></h3>
        <p>RIMAC SEGUROS Y REASEGUROS realiza el cargo mensual en el medio de pago que he utilizado para adquirir la Póliza  de Seguro para Valientes y para la renovación de la vigencia  de la Póliza de Seguro antes citada.</p>
        <p>Asimismo, el contratante es responsable de comunicar a RIMAC SEGUROS Y REASEGUROS en caso se modifique o anule el medio de pago empleado (por ejemplo: Cambio en el número de la tarjeta de crédito o débito), a fin de actualizar el nuevo medio de pago y vincularlo a la Póliza de Seguro. En caso de no realizarlo, RIMAC SEGUROS no podrá realizar el cargo de la prima, y la Póliza de Seguro podría extinguirse por incumplimiento de pago de la prima conforme lo establecido en la normativa vigente. En este sentido, RIMAC SEGUROS Y REASEGUROS no será responsable por cualquier siniestro ocurrido con posterioridad a la fecha de extinción de la Póliza de Seguro, o en su defecto, en caso sea imposible optar por su renovación.
        Usted podrá revocar la autorización del cargo recurrente de su Tarjeta de Crédito y/o Débito llamando al 411-1111 o escribiéndonos al correo atenciónalcliente@rimac.com.pe.</p>
    </div>  
)
             
export default RenovacionAutomatica