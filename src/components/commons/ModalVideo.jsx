import React from 'react'
import ReactModal from 'react-modal';
import videoSpv from '../../resources/images/videos/seguro-para-valientes.mp4';

const ModalVideo = ({ handlerCloseModal,showModal}) => {
    
    let url = "//www.youtube.com/embed/XJ0U32WKJSM?autoplay=0&showinfo=0"

    return(
        <div>
            <ReactModal
                isOpen={showModal}
                contentLabel="Modal Video"  
                onRequestClose={handlerCloseModal}
                ariaHideApp={false} 
                className="modalVideo">
                
                {/* <iframe id="iframe-rimac" 
                        className="iframe-video-ponlecorazon"
                        src={url} 
                        title="This is a unique title" 
                        frameBorder="0" allowFullScreen></iframe> */}
                
                <div className="boxVideo">
                    <video width="255" height="450" autoplay="autoplay" controls>
                                <source src={videoSpv} type="video/ogg" />
                                Tu navegador no soporta este video.
                    </video>
                </div>
                
                
                {/* <button className="btn btnCancel" onClick={handlerCloseModal}>Aceptar</button> */}
            </ReactModal>
        </div>
    )
}

// ReactModal.defaultStyles.overlay.marginTop = "80px"
export default ModalVideo;