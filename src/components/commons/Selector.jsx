import React from 'react';

 
const Selector = ({ monto, onchange, valor, holder }) => {
    
    var _ls = monto; 
   
    return(
    <div className="d-flex align-items-center justify-content-start cienp">
        <select className="mb-full cienp mr20" name="idpgenero" value={valor} onChange={onchange}> 
            <option value="0">{holder}</option>
            {
                _ls.map(o=>(<option value={o}>{o}</option>))
            }
        </select>
    </div>
    )
}
export default Selector
