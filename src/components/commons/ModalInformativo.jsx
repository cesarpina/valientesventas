import React from 'react';
import ReactModalError from 'react-modal';

const ModalInformativo = ({ contenido, showModal, handlerCloseModal }) => {

	return (
		<div>
			<ReactModalError
				isOpen={showModal}
				contentLabel="Modal Informativo"
				onRequestClose={handlerCloseModal}
				ariaHideApp={false} >
				
				<button className="btn btnCancel btnCerrarModal" onClick={handlerCloseModal}>OK</button>
				{contenido}
			</ReactModalError>
		</div>
	)
}

ReactModalError.defaultStyles.overlay.backgroundColor = 'rgba(0, 0, 0, 0.8)';
// ReactModalError.defaultStyles.overlay.height = '400px';
export default ModalInformativo;