import React from 'react'

const TerminosPolizaElectronica = () => (
    <div className="terminos-condiciones">
        <h2>Autorización para envío de la póliza electrónica.</h2>
        <h3></h3>
        <p>Autorizo para que la Póliza de Seguro de Respaldo Oncológico Ponle Corazón que solicito sea remitida a la dirección de correo electrónico aquí consignada. Asimismo, declaro conocer y aceptar que: </p>
        <div>
            <ol className="lista_poliza" >
            <li>La “Póliza de Seguro” comprende los documentos que forman parte de ella, así como cualquier comunicación relacionada a ella de cualquier índole.</li>
            <li>La Póliza de Seguro será enviada por correo electrónico: (i) Incluyendo un link donde podré visualizar la Póliza en PDF o un documento adjunto en PDF o (ii) Direccionándome a la parte privada de la web www.rimac.com, donde crearé mi usuario y contraseña para el ingreso. </li>
            <li>Es obligatorio contar con un navegador de internet (Internet Explorer 6 o superior, Chrome, Opera, Firefox) y/o cualquier software que permita abrir archivos PDF.</li>
            <li>La integridad y autenticidad de la Póliza de Seguro se acreditará a través de un certificado digital (firma digital).</li>
            <li>La entrega de la Póliza de Seguro se acreditará mediante una bitácora electrónica que registrará y confirmará el envío y recepción del correo electrónico. </li>
            <li>La forma de envío inmediato, seguro, eficiente son las ventajas de este procedimiento.</li>
            <li>El uso indebido de mi clave de correo electrónico puede originar la pérdida de confidencialidad, lo cual representa un riesgo.</li>
            </ol>
        </div>
        <h2>Consideraciones básicas: </h2>
        <p>En caso no pueda abrir o leer los archivos adjuntos, o modifique / anule el correo electrónico, deberá informarlo a: pamela.nogales@rimac.com.pe, o a la Central Aló RIMAC 411-1111. Recuerde que debe abrir y leer las comunicaciones electrónicas, archivos y links adjuntos, revisar su correo electrónico (bandejas de entrada y de “no deseados”), verificar la política de filtro o bloqueo de su proveedor de correo electrónico, mantener activa su cuenta y no bloquear nunca a pamela.nogales@rimac.com.pe. RIMAC garantiza la conservación de la información y evita su transferencia o divulgación a personas no autorizadas. La información contenida en los soportes tecnológicos se encuentra a su disposición y de la Superintendencia. RIMAC remitirá comunicaciones escritas al domicilio del ASEGURADO, en caso la normatividad vigente lo exija o, no se consigne una dirección electrónica o, a decisión expresa de RIMAC en forma adicional a la comunicación electrónica.</p>
    </div>
)
export default TerminosPolizaElectronica