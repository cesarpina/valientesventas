import React from 'react'
import ImgCash from '../../resources/images/iconos/spv/cash.svg'
import ImgCheck from '../../resources/images/iconos/spv/check.svg'
import ImgCard from '../../resources/images/iconos/spv/card.svg'

const Pasos = ({paso}) => (
    <div className="d-flex cienp boxPasos">
        <div className="container-pasos container d-flex align-items-end justify-content-between">
            <p className={"item-paso treintaytresp d-flex align-items-center justify-content-center boxPasos-item mb-flex-column" + ( paso===1?" item-paso-verde": "") }><img src={ImgCheck} alt="" className="Imgico" />Datos del negocio</p>
            <p className={"item-paso treintaytresp d-flex align-items-center justify-content-center boxPasos-item mb-flex-column" + ( paso===2?" item-paso-verde": "") }><img src={ImgCash} alt="" className="Imgico" /> Cotización</p>
            <p className={"item-paso treintaytresp d-flex align-items-center justify-content-center boxPasos-item mb-flex-column" + ( paso===3?" item-paso-verde": "") }><img src={ImgCard} alt="" className="Imgico" />Pago</p>
            {/* <p className={"item-paso" + ( paso===4?" item-paso-verde": "") }></p>
            <p className={"item-paso" + ( paso===5?" item-paso-verde": "") }></p> */}
        </div>
    </div>
    
)

export default Pasos