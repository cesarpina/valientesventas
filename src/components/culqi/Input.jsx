import React from 'react';

const Input = ({idculqi, clase, claseicon, name, dataculqi,holder,maxleng,referencia,value,onchange,creditCardClass}) => (
    <div className="d-flex align-items-center">
        <i className="material-icons dp48">{claseicon}</i>
        <input 
            id={idculqi}
            className={clase}
            name={name}
            data-culqi={dataculqi}
            placeholder={holder}
            maxLength={maxleng}
            ref={referencia}
            onChange={onchange}
            value={value}
        />
        <div className={creditCardClass?creditCardClass:"not-type-card"}></div>
        {/* <label htmlFor={idculqi}>{holder}</label> */}
    </div>
)
export default Input
