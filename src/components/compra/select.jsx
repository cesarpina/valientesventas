import React from 'react';

 
const Select = ({ monto, onchange, valor }) => {
    
    var _ls = monto; 
   
    return(
    <div className="d-flex align-items-center justify-content-start cienp">
        <select className="mb-full cienp mr20" name="idpgenero" value={valor} onChange={onchange}> 
            <option value="0">Monto máximo por factura</option>
            {
                _ls.map(o=>(<option value={o}>{o}</option>))
            }
        </select>
    </div>
    )
}
export default Select
