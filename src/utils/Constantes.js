export const ROUTE = {
	URL_PATH: '/solicitar/seguro-valientes',
	URL_LANDING: '/inicio',
	URL_REGISTRAR: '/registrar',
	URL_COTIZADOR: '/cotizar',
	URL_COMPRAR: '/comprar',
	URL_WELCOME: '/gracias'
}

/* PRODUCCIÓN */
const DATAPOWER = "https://api.rimac.com.pe/"
const DATAPOWERTEST = "https://api-desa.rimac.com.pe/"
// const SERVIDOR  = "http://rsdcppapache01.rimac.com.pe/"

/** DESARROLLO */
const APICLOUD = "https://4x02my1lk0.execute-api.us-east-2.amazonaws.com/dev-millennial/"
// const SERVIDOR  = "http://rsdcaapache01.rimac.com.pe/"

const APICULQI = "https://pago.culqi.com/api/v1/tokens"
//const APICULQI = "https://integ-pago.culqi.com/api/v1/tokens"
export const CULQI = {
	/** PRODUCCION*/
	KEY_BEARER : "live_hATlWOuqwLlI",
	/** DESARROLLO Y TEST*/
	//KEY_BEARER : "pk_test_t8SXXomzEbK12t0q"
}

export const API = {
	URL_CONSULTA_CLIENTES	: APICLOUD+"consultclient",
	URL_CONSULTA_EMPRESA	: APICLOUD+"consultdatasunat",
	URL_EVALUACION_EMPRESA	: APICLOUD+"evaluationclient",
	URL_LOG_EVENT			: APICLOUD+"logevent",
	URL_CALCULATE_PRICE		: APICLOUD+"calculateprice",
	URL_REGISTRAR_LEAD		: APICLOUD+"savelead",
	URL_UBIGEO_DEPARTAMENTO	: APICLOUD+"consultubigeo",
	URL_UBIGEO_PROVINCIA	: APICLOUD+"consultubigeo",
	URL_UBIGEO_DISTRITO		: APICLOUD+"consultubigeo",	
	URL_ENVIAR_CORREO		: APICLOUD+"sendmail",
	URL_GRABAR_TERCERO		: DATAPOWER+"GestionarTerceroSaludRest/api/grabarTerceroGenerico",
	URL_CULQI_TOKEN			: APICULQI,
	URL_CONSULTA_PRIMA		: DATAPOWER+"PagoSaludDigitalRest/api/pago/consultarPrima",
	URL_CULQI_CARGODIRECTO	: DATAPOWER+"PagoSaludDigitalRest/api/pago/cargoDirecto",
	URL_GESTIONAR_COMPRA    : APICLOUD+"GestionarCompraIEGRest/api/ieg/gestionarCompra",
	URL_OBTENER_CONSTANTES  : APICLOUD+"UtilSaludDigitalRest/api/obtenerConstante",
	URL_GRABAR_VENTA		: APICLOUD+"savesale",
	URL_PERSONA_EQUIFAX		: DATAPOWER+"UtilSaludDigitalRest/api/obtenerPersonaEquifax", 
}

export const CTTES = {
	KEY_PDIG_USUARIO_COT_IEG : "PDIG_USUARIO_COT_IEG"
}