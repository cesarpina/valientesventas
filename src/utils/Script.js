import { CULQI } from './Constantes'

export function getTramaToken (params) {    
    let fecha = params.inputfechavencimiento.split("/")
    let tramaToken = {
            correo_electronico  : params.inputemailcard,
            numero             : params.inputCreditCard.toString().replace(/\s/g,""),
            cvv                : params.inputcvvcard,
            m_exp              : fecha[0]               ,
            a_exp              : "20" + fecha[1]        ,
            guardar            : true
    }
    return tramaToken
}

function obtenerIdTipoDoc(dni,ce){
    if(dni){
        return "2"
    }

    if(ce){
        return "4"
    }
}

export function getTramaCargoDirecto(gettercero, getcard, gettoken, primactz, cotz) {
    let tramaCargoDirecto = {
        ideTercero: gettercero.tercero.registro.respuestaTercero.idetercero,
        token: gettoken.id,
        correo: gettoken.correo_electronico,
        monto: primactz,
        moneda : "SOL",
        usuario: cotz.usuario,
        numTarjeta: gettoken.tarjeta.numero,
        titularTarjeta: getcard.inputnombre + ' ' +getcard.inputapellido,
        operador: gettoken.tarjeta.marca
    }  
    
    return tramaCargoDirecto
}

export function getTramaGestionarTercero (params) {
    let tramaGestionarTercero = {
        tercero:{
            consulta:{
                idptipodocumento: obtenerIdTipoDoc(params.tipodocdni, params.tipodocce),
                numerodoc: params.tipodocdni ? params.nrodni : params.nroce
            },
            validacionhomonimia:{
                apepaterno:params.apepaterno,
                apematerno:params.apematerno,
                nombre:params.nombre,
                idpgenero:params.idpgenero,
                fecnacimiento: params.fecnacimiento,
                idpprioridad:"P01"
            },
            registro:{
                codorigen:"SAS8",
                direccion:{
                    inddirdomicilio:"1",
                    inddiroficina:"1",
                    indprincipal:"1",
                    idedistrito:params.idedistrito,
                    idptipovia:"OT",
                    nomvia:"S/N",
                    numcasa:"S/N",
                    direccioncompleta:params.direccion
                },
                documento:{
                    idptipodocumento:  obtenerIdTipoDoc(params.tipodocdni, params.tipodocce),
                    numerodoc: params.tipodocdni ? params.nrodni : params.nroce
                },
            idetercero:null,
            indValidadoReniec:false,
            indtratamiento:"2",
            mailpersonal:{
                email: params.correo,
                indprincipalmail:"1"
            },
            nombretercero:{
                nombre: params.nombre,
                apepaterno:params.apepaterno,
                apematerno: params.apematerno,
                nomcompleto: params.nomcompleto,
                nomcompletocomercial: params.nomcompleto
            },
            paisorigen:80,
            stster:"ACT",
            telefcasa:{
                numtelef: params.celular,
                indprincipal:"1"
            },
            tercerojuridico:{
                fecfundacion: params.fecnacimiento,
                ideacteconomica:"3109"
            },
            terceronatural:{
                fecnacimiento: params.fecnacimiento,
                idpgenero: params.idpgenero,
                idpestadocivil:"Z",
                idpprofesion:"36",
                idpsituacionlaboral:"0"
            },
          }
       },
       usuario:params.usuarioAuditoria,
       origen: params.origen,
       ideValor: params.idelead
    }
    
    return tramaGestionarTercero;
}


export function getTramaCorreoBienvenida(params){
    let tramaCorreoBienvenida = {
        xplantilla      : "5",
        numerodni       : params.tipodocdni?"DNI":"CE",
        numerodocumento : params.tipodocdni?params.nrodni:params.nroce,
        nombre          : params.nombre,
        apepaterno      : params.apepaterno,
        apematerno      : params.apematerno,
        telefono        : params.celular,
        correo          : params.correo
    }

    return tramaCorreoBienvenida;
}

export function getTramaGestionarCompra(tercero, params, primactz, tarjeta){
    let fecha = new Date();
    let dia = fecha.getDate() < 10 ? "0"+fecha.getDate() : fecha.getDate()
    let mes =  (fecha.getMonth() +1) < 10 ? "0"+ (fecha.getMonth() +1) :  (fecha.getMonth() +1)
    
    let fechaActual = dia + "/" + mes + "/" + fecha.getFullYear();

    var tramaGestionarCompra = {
        usuario: params.usuarioCot,
        solicitud:{
           nroEmpleado    :   params.tipodocdni?params.nrodni:params.nroce,
           fecFirma       :   fechaActual,
           fecIniVig      :   fechaActual
        },
        cotizacion:{
           ideTerceroContratante : tercero.tercero.registro.respuestaTercero.idetercero,
           ideTerceroTitular     : tercero.tercero.registro.respuestaTercero.idetercero,
           indEnvioMail          : "S"
        },
        lead:{
           idelead            : params.idelead,
           idptipodocumento   : params.tipodocdni?"DNI":"CE",
           numerodocumento    : params.tipodocdni?params.nrodni:params.nroce,
           nomcompleto        : params.nomcompleto,
           fecnacimiento      : params.fecnacimiento,
           idpgenero          : params.idpgenero,
           precio             : primactz,
           numerotelf         : params.celular,
           email              : params.correo,
           useragent          : "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36",
           nombres            : params.nombre,
           apepaterno         : params.apepaterno,
           apematerno         : params.apematerno,
           fgVentaFinalizada  : "S"
        },
        suscripcion:[
           {
              pregunta  :   "He sido diagnosticado de cáncer,tumor maligno u otro tipo de enfermedad oncológica.",
              respuesta :   "NO"
           }
        ],
        correo:{
           tipoDocumento :   params.tipodocdni?"DNI":"CE"                   ,
           nroDocumento  :   params.tipodocdni?params.nrodni:params.nroce   ,
           nombre        :   params.nombre          ,
           apePaterno    :   params.apepaterno      ,
           apeMaterno    :   params.apematerno      ,
           telefono      :   params.celular         ,
           correo        :   params.correo          ,
           fechaFirma    :   fechaActual            ,
           departamento  :   params.dscdepartamento ,
           provincia     :   params.dscprovincia    ,
           distrito      :   params.dscdistrito     ,
           direccion     :   params.direccion       ,
           nroTarjeta    :   tarjeta
        }
      }
      console.log("jeampiere"+tramaGestionarCompra);
    return tramaGestionarCompra
}