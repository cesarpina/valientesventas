export default class GTM {
    static enviarCotizador = (label) => {
        window.dataLayer.push({
            event    : 'virtualEvent',
            category : 'Seguro para Valientes',
            action   : 'Home - Clic Botón',
            label    
        });
    }

    static verExclusiones = () => {
        window.dataLayer.push({
            event    : 'virtualEvent',
            category : 'Seguro para Valientes',
            action   : 'Coberturas - Clic Enlace',
            label    : 'Ver todas las exclusiones'
        });
    }

    static optionesMenu = (label) => {
        window.dataLayer.push({
            event    : 'virtualEvent',
            category : 'Seguro para Valientes',
            action   : 'Menú',
            label
        });
    }

    static pasosCotizador = (action, label) =>{
        window.dataLayer.push({
            event    : 'virtualEvent',
            category : 'Seguro para Valientes',
            action, label
        });
    }
   
    static navegarCotizador = (page) => {
        window.dataLayer.push({
            event     : 'virtualPage',
            pageUrl   : '/respaldo-oncologico-ponlecorazon/'+page.toLowerCase() ,
            pageTitle : 'Seguro para Valientes - '+page
        });
    }

    static transaccionCompleta = (tx) => {
        window.dataLayer.push({
            event: 'productTransaction',
            ecommerce: {
                purchase: {
                    actionField: {
                        id          : tx.id,
                        affiliation : 'Rimac Seguros Online',
                        revenue     : tx.price,
                        tax         : tx.tax,
                        shipping    : 0
                    },
                    products: [{
                        name     : 'Seguro para Valientes',
                        id       : tx.ideProducto,
                        price    : tx.price,
                        brand    : 'Rimac Seguros',
                        category : 'Seguro Riesgos',
                        variant  : 0,
                        quantity : 1
                    }]
                }   
            }
        });
    }
}
