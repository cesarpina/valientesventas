
export default class UtilsJS {
	static campovacio = (campo, caso) => {
		if (campo.length === 0) {
			switch (caso) {
				case 'ce':
				case 'dni':
				case 'ruc':
				case 'nombre':
				case 'apepaterno':
				case 'apematerno':
				case 'fecnacimiento':
				case 'genero':
				case 'correo':
				case 'telefono':
				case 'direccion':
				case 'departamento':
				case 'provincia':
				case 'distrito':
				case 'nrohijos':
					return false
				default:
					return false
			}
		}
		return true
	}

	static validadni = (campo, caso) => {
		if (UtilsJS.campovacio(campo, caso)) {
			if (UtilsJS.validasolonumero(campo)) {
				if (campo.length === 8) {
					return true
				} else if (campo.length !== 8) {
					return false
				}
			} else {
				return false
			}
		} else {
			return false
		}
	}

	static validaruc = (campo, caso) => {
		if (UtilsJS.campovacio(campo, caso)) {
			if (UtilsJS.validasolonumero(campo)) {
				if (campo.length === 11) {
					return true
				} else if (campo.length !== 11) {
					return false
				}
			} else {
				return false
			}
		} else {
			return false
		}
	}

	static validace = (campo, caso) => {
		if (UtilsJS.campovacio(campo, caso)) {
			if (campo.length < 8 || campo.length > 12) {
				return false
			} else {
				return true
			}
		} else {
			return false
		}
	}

	static validasololetras = (campo, caso) => {
		if (UtilsJS.campovacio(campo, caso)) {
			var exp = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]*$/i
			if (!exp.test(campo)) {
				return false
			} else {
				return true
			}
		} else {
			return false
		}
	}

	static validasolonumero = (campo) => {
		//let camponumerico = parseInt(campo); //, 10)
		//console.log(campo);
		if (isNaN(campo)) {
			//console.log(false);
			return false
		} else {
			//console.log(false);
			return true
		}
		/*if (!(!isNaN(parseFloat(camponumerico)) && isFinite(camponumerico))) {
			console.log(false);
			return false
		} else {
			console.log(false);
			return true
		}*/
	}

	static validacorreo = (campo, caso) => {
		var exp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

		if (UtilsJS.campovacio(campo, caso)) {
			if (!exp.test(campo.toLowerCase())) {
				return false
			} else {
				return true
			}
		} else {
			return false
		}
	}

	static validatelefono = (campo, caso) => {
		
		if (UtilsJS.campovacio(campo, caso) ) {
			if ( campo.length < 5  || campo.length > 9)  {
				return false  
			}
			if (!UtilsJS.validasolonumero(campo)){
				return false  
			} 
		} 
		return true 
	}

	static validafecnacimiento = (campo, caso) => {
		if (UtilsJS.campovacio(campo, caso)) {
			if (UtilsJS.validarEdadContratante(campo)) {
				return true
			} else {
				return false
			}
		} else {
			return false
		}
	}

	static validadireccion = (campo, caso) => {
		if (UtilsJS.campovacio(campo, caso)) {
			var exp = /^[0-9a-zA-ZáéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ-\s]+$/
			if (!exp.test(campo)) {
				return "errorDirFormat"
			}else if( campo.length < 6){
				return "errorDirLargo" 
			} else {
				return true
			}
		} else {
			return false
		}
	}

	static validarEdadContratante = (value) => {
		var campo = value;
		var RegExPattern = /^\d{1,2}\/\d{1,2}\/\d{4,4}$/;
		var isOk = true;
		if ((campo.match(RegExPattern))) {
			var fechaf = campo.split("/");
			var day = fechaf[0];
			var month = fechaf[1];
			var year = fechaf[2];
			var date = new Date(month + '/' + day + '/' + year);

			if (date === "Invalid Date") {
				isOk = false;
			} else {
				var hoy = new Date();
				if (hoy < date) {
					isOk = false;
				} else {
					var edad_apoderado = UtilsJS.calcularEdad(month + '/' + day + '/' + year);
					if (edad_apoderado < 18) {
						isOk = false;
					}
					if (edad_apoderado > 90) {
						isOk = false;
					}
				}
			}
		} else {
			isOk = false;
		}
		return isOk;
	}

	static calcularEdad = (fecha) => {
		var hoy = new Date();
		var cumpleanos = new Date(fecha);
		var edad = hoy.getFullYear() - cumpleanos.getFullYear();
		var m = hoy.getMonth() - cumpleanos.getMonth();
		if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
			edad--;
		}
		return edad;
	}

	static validgenero = (campo, caso) => {
		return UtilsJS.campovacio(campo, caso) ? true : false
	}

	static validarCuestionario = (cuestionario) => {
		let { respuesta1, respuesta2, respuesta3, respuesta4 } = cuestionario
		if (respuesta1.positivo || respuesta1.negativo) {
			if (respuesta2.positivo || respuesta2.negativo) {
				if (respuesta3.positivo || respuesta3.negativo) {
					if (respuesta4.positivo || respuesta4.negativo) {
						if (respuesta1.positivo) {
							return 1
						} else if (respuesta2.positivo) {
							return 2
						} else if (respuesta3.positivo) {
							return 3
						} else if (respuesta4.positivo) {
							return 4
						} else {
							return true
						}
					} else {
						return false
					}
				} else {
					return false
				}
			} else {
				return false
			}
		} else {
			return false
		}
	}



}