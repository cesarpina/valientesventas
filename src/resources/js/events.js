export function deslizarNavMobile(element){    
    if(document.getElementById(element).style.transform === "translateX(0%)"){
        document.getElementById(element).style.transform = "translateX(-110%)";
    }else{
        document.getElementById(element).style.transform = "translateX(0%)"

    }
    document.getElementById(element).style.transition = "all 0.3s ease";
}

export function deslizarBtnMas(element1, element2, caso){
    if(caso === 0){ //landing
        let container = document.getElementById(element2);
        if(container.style.display === "block"){
            container.style.display = "none"
        }
    }
    if(caso === 1){ // header
        let container = document.getElementById(element2);
        if(container.style.display === "block"){
            container.style.display = "none"
        }else{
            let left = document.getElementById(element1).getBoundingClientRect().left;
            container.style.left = left+"px";
            container.style.display = "block"
        }
    }
}