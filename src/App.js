import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Redirect, Route } from 'react-router-dom';
import Registrar from './containers/Registrar';
import Landing from './containers/Landing';
import Cotizador from './containers/Cotizador';
import Comprar from './containers/Comprar';
import Welcome from './containers/Welcome';
import 'jquery';
import './resources/css/materializeCalendar.css';
import './resources/css/materialize.min.css';
import './resources/css/transitions.css';
import './resources/css/fonts.css';
import './resources/css/index.css';
import './resources/css/main.css';
import './resources/css/base.css';
import './resources/js/events.js';
import configureStore from './store/configureStore';
import { ROUTE } from './utils/Constantes'
require('materialize-css');
require('materialize-js');

class App extends Component {
	render() {
		return (
			<Provider store={configureStore()}>
				<Router basename={ROUTE.URL_PATH}>
					<div>
						<Route exact path="/" render={() => (<Redirect to={ROUTE.URL_LANDING} />)} ></Route>
						<Route exact path={ROUTE.URL_LANDING} component={Landing} ></Route>
						<Route exact path={ROUTE.URL_REGISTRAR} component={Registrar} ></Route>
						<Route exact path={ROUTE.URL_COTIZADOR} component={Cotizador} ></Route>
						<Route exact path={ROUTE.URL_COMPRAR} component={Comprar} ></Route>
						<Route exact path={ROUTE.URL_WELCOME} component={Welcome} ></Route>
					</div>
				</Router>
			</Provider>
		);
	}
}
 

export default App; 
 