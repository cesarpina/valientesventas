import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Element, scroller } from 'react-scroll';
import Footer from '../components/commons/Footer';
import Header from '../components/commons/Header';
import PageComprarCtz from '../components/cotizador/PageComprarCtz';
import PageDescalificado from '../components/cotizador/PageCalculoPrima';
import PageErrorCulqi from '../components/cotizador/PageErrorCulqi';
import { fetchEvaluacionClientes } from '../actions/evaluacionClientes';
import PageInfo from '../components/cotizador/PageInfo';
import PagePagoPrima from '../components/cotizador/PagePagoPrima';
import PageContratante from './cotizador/PageContratante';
import PageThanku from './cotizador/PageThanku';
import PageResumenCtz from './cotizador/PageResumenCtz';
import PageCulqi from './culqi/PageCulqi';
import Loading from '../components/commons/Loading'
import { fetchEnviarCorreo } from '../actions/envioCorreo';
import {ROUTE} from '../utils/Constantes'
import ModalError from '../components/commons/ModalError'
import UtilsJs from '../utils/Funciones';
import { fetchRegistrarLog } from '../actions/registrarLog'
import { fetchRegistrarLead } from '../actions/registrarLead'

class Registrar extends Component {
	constructor(props){
		super(props)
		this.state = {
			isPaso1: true, // Registrar -> Formulario de contacto
			isPaso2: false, // Comprar1 -> Datos del negocio y Clientes
			isPaso3: false,  // Comprar2 -> Datos del negocio y Clientes
			isPaso4: false, // DONA
			isPaso5: false, // VAIDACION DE USUARIO
			isPaso6: false,
			isPaso7: false, //checkout
			isPaso8: false, // No podemos darte este seguro
			isPaso9: false,
			isLoading: false,
			nombre  : "",
			mensaje : "",
			telefono : "",
			correo   : "",
			nrodoc  : "",
			tipodoc : "",
			showVolver: false ,
			mensajeculqi : "La tarjeta que has ingresado no está activa para compras por internet. Te recomendamos contactar con el banco de tu tarjeta",
			showModalError : false,
			persona:[],
			leadResponse:[]
		}
	}

	componentDidMount(){
		this.handlerScroll("cotizadorTop")
	}
	handlerCloseModalError = () => {
		this.setState({
			showModalError :  false
		})
	}
	evaluarEMail=(email)=>{
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var evalue = re.test(email.toLowerCase());
        if (evalue) {
          this.isValidEmail = true;
        } else {
          this.isValidEmail = false;
        }
        return evalue;
	  }
	  
	  registroLog=(dniCliente,modulo,accion)=>{
		this.props.registrarLog(dniCliente,modulo,accion).then(respLog=> {           
		})
	  }
	  
	  terceroLead = (xaccion,xtipoDocumento,xnumeroDocumento,xnombreCliente,
		xfechaNacimiento, xidGenero, xnumeroTelefono,xcorreoElectronico)=>{

			var data = {
			accion: xaccion,
			canalRegistrador : 'Web Venta Millennial',
			tipoDocumento : xtipoDocumento,
			numeroDocumento : xnumeroDocumento,
			nombreCliente : xnombreCliente,
			fechaNacimiento : xfechaNacimiento,
			idGenero : xidGenero,
			descripcionModulo : 'LEAD',
			detalleAccion : 'GENERACION DE LEAD',
			numeroTelefono : xnumeroTelefono,
			correoElectronico : xcorreoElectronico,
			direccionIP : '192.168.1.1',
			usuarioRegistrador : 'USUARIOMILLENNIALS'
		}
		var dataConsulta = {
			accion: "CONSULTAR",
			numeroDocumento : xnumeroDocumento 
		}
		this.props.registroLead(data).then(response=> {   
			this.props.registroLead(dataConsulta).then(responseConsult=> {   
				
				this.setState({
					leadResponse: responseConsult })
			})
		})
	  }

	handlerCot = (seccion) => {
		this.handlerLoading(true)
		var modulo = "REGISTRO" 
		if(seccion.validdni=="error" || seccion.nrodni=='' || seccion.nrodni==null){
			this.setState({
							showModalError: true,
							contenido: <p>Por favor, ingresa tu número de documento de identidad.</p>,
			})
			this.handlerLoading(false)
			return;
		}
		if(seccion.celular=='' || seccion.celular==null || seccion.validcelular == "error"){
			this.setState({
							showModalError: true,
							contenido: <p>Por favor, ingresa tu número celular.</p>,
			})
			this.handlerLoading(false)
			return;
		}
		if(seccion.correo=='' || seccion.correo==null || seccion.validcorreo == "error"){
			this.handlerLoading(false)
			this.setState({
				showModalError: true,
				contenido: <p>Por favor, ingresar tu correo electrónico.</p>,
			})
			return;
		} 
		if(!this.evaluarEMail(seccion.correo)){
			this.handlerLoading(false)
			this.setState({
				showModalError: true,
				contenido: <p>Por favor, ingresar un correo electrónico válido.</p>,
			})
			return;
		}

		if(!document.getElementById("checkTerminos").checked){
			this.setState({
							showModalError: true,
							contenido: <p>Por favor, aceptar la política de tratamiento y protección de datos personales.</p>,
			})
			this.handlerLoading(false)
			return;
		}
 


		var obj = {	nrodni		: seccion.nrodni ,
					Nombres		: seccion.Nombres,
					idTecero 	: seccion.id,
					celular 	: seccion.celular,
					correo 	: seccion.correo,					
		};
		
		var xpersona=this.state.persona;
		xpersona=obj;
		this.setState({persona:xpersona});

		if(seccion.length != 0){


			//validacion
			this.props.evaluacionCliente(seccion.nrodni, "1", seccion.nrodni, "1").then(resp => {		
				if(resp.type === "RECEIVE_EVALUACIONCLIENTES"){
					this.registroLog(seccion.nrodni,modulo,"[EVALUACIÓN DE CLIENTE] - Cliente: " + seccion.nrodni + " - Puntaje: " + resp.cliente.puntaje)
					if(resp.cliente.detalle == "V"){
						this.handlerLoading(false)
						this.terceroLead(
									"REGISTRAR","DNI",this.props.cliente.numerodoc,
									seccion.Nombres,this.props.cliente.fecnacimiento, 
									this.props.cliente.idpgenero, seccion.celular,
									seccion.correo); 
									
						this.props.history.push({pathname : ROUTE.URL_COTIZADOR});	
						window.dataLayer.push({
							event    : 'virtualEvent',
							category : 'Seguro para Valientes',
							action   : 'Paso 1: Datos del Contacto',
//							label    :  this.props.principalcliente.IdCliente
							label    :  this.props.cliente.numerodoc
				
						});
					}else{
						this.handlerLoading(false)
							this.setState({
								showModalError: true,
								contenido: <p>Hemos tenido un problema con nuestro sistema, inténtelo más tarde.</p>,
								
							});
							window.dataLayer.push({
								event: 'virtualEvent',
								category: 'Seguro para Valientes',
								action: 'Paso 1: Resultado de Calificación',
								//label: `Resultado Denegado - ${this.props.principalcliente.IdCliente}`
								label: `Resultado Denegado - ${this.props.cliente.numerodoc}`
							});

					}

  
				}else if(resp.type === "INVALIDATE_CONSULTACLIENTES"){
					//mensaje de error no puede pasar
					this.handlerLoading(false)
					this.setState({
						showModalError: true,
						contenido: <p>Hemos tenido un problema con nuestro sistema, inténtelo más tarde.</p>,
						
					})
				}			
				
			})


		}else{
			this.handlerLoading(false)
			this.setState({
				showModalError: true,
				contenido: <p>Por favor, ingresar los datos solicitados. (ERROR-ND)</p>,
			})
	
		}		

	}



	handlerLoading = (value) => {
		this.setState({isLoading : value})
	}

	handlerUpdateNombre = (nombre) => {
		let nombres = nombre.split(" ");
		let nombreMasc = "";
		for (let i = 0; i < nombres.length; i++) {
			nombreMasc = nombreMasc + nombres[i].charAt(0).toUpperCase() + nombres[i].slice(1).toLowerCase() + " "
		}

		nombreMasc = nombreMasc.substring(0, nombreMasc.length-1)
		this.setState({ nombre : nombreMasc })
	}

	handlerUpdateDatoCorreo = (nrodoc, tipodoc, telefono, correo) => {
		this.setState({ nrodoc  , tipodoc , telefono , correo })
	}

	handlerOnChangeMensaje = (e) => {
		this.setState({ mensaje: e.target.value })
	}

	handlerEnvioCorreoMensaje = () => {
		this.handlerLoading(true)
		let params = {
			tipocorreo		: "COMONCO"			 ,
			numerodni		: this.state.tipodoc ,
			numerodocumento : this.state.nrodoc  ,
			nombre			: this.state.nombre  ,
			mensaje			: this.state.mensaje ,
			telefono		: this.state.telefono,
			correo 			: this.state.correo
		}

		this.props.consultaTercero(params).then(resp => {
			if (resp.type === "INVALID_ENVIARCORREO") {
				// alert("no se envio correo")
			}
			this.handlerLoading(false)
		})
	}

	updateMensajeCulqiError = (value, isShow) => {
		this.setState({mensajeculqi: value, showVolver: isShow});
	}

	handlerScroll = (element) => {
		scroller.scrollTo(element, {
			duration: 500,
			delay: 0,
			smooth: "easeOutQuad"
		});
	}


	handlerPaso = (paso) => {
		this.handlerScroll("cotizadorTop")
		for (let i = 1; i <= 9; i++) {
			if (i === paso) {
				this.setState({
					["isPaso" + i]: true
				})
			} else {
				this.setState({
					["isPaso" + i]: false
				})
			}
		}
	}


	handlerOnChange = (e) => {
		//almaceno valr del RUC en el evento
		var numruc = e.target.value;
		//validando ruc cantidad de digitos
		if (e.target.name === "rucCli" && UtilsJs.validaruc(e.target.value, "ruc")) {
			this.props.handlerLoading(true)
			this.props.consultarDatosEmpresa(e.target.value).then(resp => {				
				if(resp.type === "RECEIVE_CONSULTAEMPRESA"){

				 
					if (resp.empresa.RazonSocial !== null  ) {
							this.setState({
								rucCli		: numruc,
								Actividad		: resp.empresa.Actividad,
								Direccion			: resp.empresa.Direccion,
								Distrito			: resp.empresa.Distrito,
								Provincia			: resp.empresa.Provincia,
								Departamento			: resp.empresa.Departamento,
								validruc	: true
							}, () => {
								this.handlerMascNombre(this.state.nombreapellido)							
								document.getElementById("label-nombreapellidoMasc").className = "active label-input"
								this.props.handlerUpdateNombre(this.state.nombre)
								this.props.handlerLoading(false)
							})
						
					} else {
						/** mensaje no contamos con sus datos */
						this.setState({
							showModalError: true,
							contenido: <p>No te tenemos registrado. (ERROR-ND)</p>,
							numruc		: numruc
						})
					}
				}else if(resp.type === "INVALIDATE_CONSULTACLIENTES"){
					this.setState({
						showModalError: true,
						contenido: <p>Hemos tenido un problema con nuestro sistema, intentelo más tarde. (ERROR-EQ)</p>,
						numruc		: numruc
						
					})
				}			
				
			})
		} else if (e.target.name === "nrodni" && !UtilsJs.validadni(e.target.value, "dni")) {
			this.setState({
				nrodni: '',
				apepaterno: '',
				apematerno: '',
				Nombres: '',
				nombre: '',
				nombreapellidoMasc: '',
				idpgenero: '',
				fecnacimiento: '',
				celular: '',
				correo: '',
				nombreapellido: '',
				validdni: "error",
				validnombre: false,
				validcelular: false,
				validcorreo: false,
				validgenero: false 
			})
		}

		if (e.target.name === "nroce") {
			if (UtilsJs.validace(e.target.value, "ce")) {
				this.setState({ validce: true })
			} else {
				this.setState({ validce: "error" })    
			}
		}

		this.setState({
			[e.target.name]: e.target.value
		}, () => {
			this.props.updateCotizacion(this.getDatosContratante())
		})
	}





	 
	render() {
		let { isPaso1, isPaso2, isPaso3, isPaso4, isPaso5, isPaso6, isPaso7, isPaso8, isPaso9, isLoading  } = this.state
		let modal = {
			contenido : this.state.contenido,  
			title:<div>LO SENTIMOS</div>, 
			handlerCloseModal:this.handlerCloseModalError ,
			showModal:this.state.showModalError
		}
		return (
			<Element name="cotizadorTop">
				<div>
					<Header />
					<section className="section-cotizador">
						{isPaso1 ? <PageContratante handlerLoading={this.handlerLoading}
													handlerUpdateNombre={this.handlerUpdateNombre} 
													handlerPaso={this.handlerPaso} 
													handlerUpdateDatoCorreo={this.handlerUpdateDatoCorreo}
													handlerCot={this.handlerCot}
													/> : null}

						{isPaso2 ? <PagePagoPrima 	handlerLoading={this.handlerLoading}
													nombre={this.state.nombre} 
													prima={this.props.cotizacion} 
													handlerPaso={this.handlerPaso} 
													handlerOnChange={this.handlerOnChange}
													/> : null}
						{isPaso3 ? <PageComprarCtz 	value={this.state.mensaje}
													handlerLoading={this.handlerLoading}
													handlerPaso={this.handlerPaso}
													handlerOnChange={this.handlerOnChangeMensaje}
													handlerEnvioCorreoMensaje={this.handlerEnvioCorreoMensaje} /> : null}
						{isPaso4 ? <PageInfo 			handlerPaso={this.handlerPaso} /> : null}
						{isPaso5 ? <PageThanku 	handlerLoading={this.handlerLoading}
														handlerPaso={this.handlerPaso} /> : null}
						{isPaso6 ? <PageResumenCtz handlerPaso={this.handlerPaso} handlerLoading={this.handlerLoading} /> : null}
						{isPaso7 ? <PageCulqi 	handlerPaso={this.handlerPaso} 
												handlerLoading={this.handlerLoading}
												updateMensajeCulqiError={this.updateMensajeCulqiError} /> : null}
						{isPaso8 ? <PageDescalificado nombre={this.state.nombre} handlerPaso={this.handlerPaso} /> : null}
						{isPaso9 ? <PageErrorCulqi value={this.state.mensajeculqi} showVolver={this.state.showVolver} handlerPaso={this.handlerPaso} /> : null}					
						{isLoading ? <Loading /> : null }
						<ModalError {...modal} />
					</section>
					<Footer />
				</div>

				
			</Element>
		)
	}
}


const mapStateToProps = store => {
	return {
		cotizacion: store.cotizacion.cotizacion,
		principalcliente: store.principalcliente.principalcliente,
		cliente: store.tercero.tercero,
		lead : store.lead.lead
	}
}

const mapDispatchToProps = dispatch => {
	return {
		consultaTercero: (params) => dispatch(fetchEnviarCorreo(params)),
		evaluacionCliente: (NroCliente, NroDoc, paso) => dispatch(fetchEvaluacionClientes(NroCliente, NroDoc, paso)),
		registroLead: (data) => dispatch(fetchRegistrarLead(data)),
		registrarLog:(dniCliente,modulo,accion) => dispatch(fetchRegistrarLog(dniCliente,modulo,accion)),

	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Registrar)