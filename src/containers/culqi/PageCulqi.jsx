import React, { Component } from 'react'
import { connect } from 'react-redux';
import { withRouter } from "react-router-dom"
import { fetchUpdateCotizacion } from '../../actions/cotizacion';
import { fetchGrabarTercero } from '../../actions/grabarTercero'
import { fetchCulqiToken } from '../../actions/culqiToken'
import { fetchCulqicargodirecto } from '../../actions/culqiCargoDirecto'
import { fetchEnviarCorreo } from '../../actions/envioCorreo'
import { fetchgestionarcompra } from '../../actions/gestionarcompra'
import { fetchRegistrarLog } from '../../actions/registrarLog'
import Button from '../../components/cotizador/commons/Button'
import Title from '../../components/cotizador/commons/Title'
import Input from '../../components/culqi/Input'
import LoadingProcesando from '../../components/commons/LoadingProcesando'
import ImgCard from '../../resources/images/iconos/card-ccv.png'
import imgpago from '../../resources/images/iconos/mpagos.png'
import imgprocesando from '../../resources/images/loading/procesando.png'
import {getTramaGestionarTercero, getTramaToken, getTramaCargoDirecto, getTramaCorreoBienvenida, getTramaGestionarCompra} from '../../utils/Script'
import {ROUTE} from '../../utils/Constantes'
import ModalInformativo from '../../components/commons/ModalInformativo';
import ModalError from '../../components/commons/ModalError'
import TerminosPolizaElectronica from '../../components/commons/TerminosPolizaElectronica'
import TerminosPrivacidadDatos from '../../components/commons/TerminosPrivacidadDatos'
import RenovacionAutomatica from '../../components/commons/RenovacionAutomatica'
import GTM from '../../utils/GTM'
import Pasos from '../../components/commons/Pasos'
import { fetchGrabarVenta } from '../../actions/guardarVenta'

class PageCulqi extends Component {
    constructor(props){
        super(props)
        this.state = {
            contenido : '',
            showModal : false ,
            showModalInformativo : false,
            responseTercero : [] ,
            responseToken : [] ,
            responseCargoDirecto : [],
            respGestionarCompra : [],
            inputfechavencimiento:'',
            inputcvvcard:'',
            inputCreditCard:'',
            inputnombre:'',
            inputapellido:'',
            inputemailcard:'',             
            creditCardClass:'',
            maxLengthCvv: '',
            chkCondicionesRimac: false,
            showModalError: false,
            nombrePageTku : '',
            varia: false,
            nombrecorreo: 'Cesar',
            directcorreo: 'cesarpinafrancia@gmail.com'
        }

        this.creditCardClass = ["card-type-icon", "show not-type-card"];
        this.isValidNumberCard = false;
        this.isValidDateExp = false;
        this.isValidCvv = false;
        this.isValidEmail = false;
        this.maxLengthCvv = 3;
          
    }


    getDatosContratante() {
      return {
        tipodocdni		        : this.props.cotizacion.tipodocdni	      ,
        tipodocce		          : this.props.cotizacion.tipodocce	        ,
        nrodni			          : this.props.cotizacion.nrodni		        ,
        nroce			            : this.props.cotizacion.nroce		          ,			
        fecnacimiento	        : this.props.cliente.fecnacimiento     ,
        apepaterno		        : this.props.cliente.apepaterno	   		,
        apematerno		        : this.props.cliente.apematerno	   		,
        nomcompleto		        : this.props.cliente.nomcompleto	   		,
        nombre			          : this.props.cliente.nombre		   			,
        idpgenero		          : this.props.cliente.idpgenero	   			,
        celular			          : this.props.cotizacion.celular		   		  ,
        correo			          : this.props.cotizacion.correo		        ,
        nombreapellido	      : this.props.cotizacion.nombre+' '+this.state.apepaterno ,
        idedepartamento	      : this.props.ubigeo.empresa.codDepartamento		,
        ideprovincia	        : this.props.ubigeo.empresa.codProvincia			,
        idedistrito		        : this.props.ubigeo.empresa.codDistrito			  ,
        dscdepartamento	      : this.props.ubigeo.empresa.Departamento		,
        dscprovincia	        : this.props.ubigeo.empresa.Distrito			,
        dscdistrito		        : this.props.ubigeo.empresa.Provincia			  ,
        direccion		          : this.props.ubigeo.empresa.Direccion				  ,
        nombreapellidoMasc    : this.props.cliente.nombre+' '+this.state.apepaterno,
        nombreMasc 		        : this.props.cliente.nombre				,
        apepaternoMasc	      : this.props.cliente.apepaterno	  ,
        apematernoMasc	      : this.props.cliente.apematerno	  ,
        validdni		          : this.props.cotizacion.validdni	        ,
        validce			          : this.props.cotizacion.validce		        ,
        validnombre		        : this.props.cotizacion.validnombre			  ,
        validapepaterno	      : this.props.cotizacion.validapepaterno		,
        validapematerno	      : this.props.cotizacion.validapematerno		,
        validcelular	        : this.props.cotizacion.validcelular			,
        validcorreo		        : this.props.cotizacion.validcorreo			  ,
        validfechanacimiento	: this.props.cotizacion.validfechanacimiento,
        validdireccion		    : true		,
        validdepartamento	    : true	,
        validdistrito			    : true		  ,
        validprovincia		    : true		,
        validgenero				    : this.props.cotizacion.validgenero		    ,
        idelead					      : this.props.lead.body[0].IdLead,
        precioprima           : this.props.cotizacion.precioprima       ,
        // ideTransaccion        : this.state.respGestionarCompra.compra.cotizacion.ideAcuerdo, 
        usuarioAuditoria	  	: 'MILLENNIALS'  ,
        ideProducto 				  : '4507'       ,
        idePlan 					    : '195128'           ,
        origen                : 'MIL'            ,
        indprotecciondatos  	: 'S', //CHECKBOX DE DATOS
        indcondcomerciales  	: 'S',
        ubigeo                : this.props.empresa.Ubigeo
      }
    }



    componentDidMount(){
     // GTM.navegarCotizador('Pago')
      if(this.props.lead == null || this.props.lead.body == null){
        this.props.history.push({pathname : ROUTE.URL_REGISTRAR});      
      }else{
        this.props.updateCotizacion(this.getDatosContratante());
      }
    }

    handlerCloseModalError = () => {
      this.setState({
        showModalError :  false
      })
    }

    handlerValidarCamposCulqi = () => {
      let {inputcvvcard, inputnombre, inputapellido, inputemailcard, inputfechavencimiento,chkCondicionesRimac} = this.state
      if ( inputcvvcard && inputnombre && inputapellido && inputemailcard && inputfechavencimiento && chkCondicionesRimac){
        return true; 
      }else {
        return false ;
      }
      return true ; 
    }

    registroVenta=(data)=>{
      this.props.registrarVenta(data).then(respLog=> {           
      })
      }

     
    registroLog=(dniCliente,modulo,accion)=>{
      this.props.registrarLog(dniCliente,modulo,accion).then(respLog=> {           
      })
    }

    functDatalayerStep4(){
      if(this.state.varia === false){
        window.dataLayer.push({
          event: 'checkoutOption',
          ecommerce: {
            checkout_option: {
              actionField: {'step': 4}
              }
          }
        });
        this.state.varia = true
      }
    }

    handlerCorreoBienvenida = (callback) => {
        this.props.correoBienvenida(getTramaCorreoBienvenida(this.props.cotizacion)).then(respBienvenida=> {
          
      })
    }

    enviarcorreoreturn = (data) =>{
       
    }
    
    
    handlerSiguiente=()=>{

       
       
      if(this.handlerValidarCamposCulqi()){
        //GTM
        window.dataLayer.push({
          event: 'virtualEvent',
          category: 'Seguro para Valientes',
          action: 'Paso 4: Compra',
          //label: this.props.principalcliente == null ? "0" : this.props.principalcliente.IdCliente,
          label: this.props.cliente == null ? "0" : this.props.cliente.numerodoc
          });

         this.setState({showModal:true }, ()=> this.props.handlerLoading(true))


        // ENVIANDO CORREO DE BIENVENIDA
        //this.handlerCorreoBienvenida(this.enviarcorreoreturn); 


        // 1. GRABAR TERCERO
        this.props.grabarTercero(getTramaGestionarTercero(this.props.cotizacion)).then(respTercero => {

          if(respTercero.type === "RECEIVE_GRAGAR_TERCERO"){

            if(respTercero.grabarTercero.tx.codigo === "0"){
              this.setState({responseTercero: respTercero.grabarTercero})


                
                // 2. OBTENER TOKEN
                 this.props.obtenerToken(getTramaToken(this.state)).then(repsToken=>{
                 var dniCliente = this.props.cotizacion.nrodni
                 var modulo = "Pago"
                 
                 if(repsToken==null){
                  this.registroLog(dniCliente,modulo,"[ERROR] El servicio de token no retornó información.")
                  this.props.handlerLoading(false)
                  this.setState({
                   showModal:false,
                   showModalError: true,
                   contenido: <p>Al parecer hemos tenido un problema con nuestro sistema. Lo sentimos, no se ha procesado tu compra, inténtalo más tarde.</p>,
                   })
                   return;
                 }
                 
                  if(repsToken.type === "RECEIVE_CULQI_TOKEN"){
                        
                        this.registroLog(dniCliente,modulo,"[EXITO] El servicio de token respondió sin problemas. " + JSON.stringify(repsToken))
                        
                         if(repsToken.culqitoken.objeto === "token"){
                           this.setState({responseToken: repsToken.culqitoken})
                           var nombre = this.state.inputnombre + " " + this.state.inputapellido;
                           var xIdTercero = '';
                           xIdTercero = this.state.responseTercero.tercero.registro.respuestaTercero.idetercero;
                            
                                var backup = {"ideTercero":xIdTercero,"token":repsToken.culqitoken.id,
                                          "correo":this.state.inputemailcard,"monto": this.props.calculo.Precio,
                                          "moneda":"SOL",
                                          "usuario":"MILLENNIALS",
                                          "numTarjeta":repsToken.culqitoken.tarjeta.numero,
                                          "titularTarjeta":nombre,
                                          "operador":repsToken.culqitoken.tarjeta.marca,
                                          "origen":"MIL"}
 
                          // 3. CARGO DIRECTO
                           this.props.cargoDirecto(backup).then(respCargo => {
                             
                               if(respCargo.type === "RECEIVE_CULQI_CARGODIRECTO"){
                                 
                                 if(respCargo.culqicargodirecto.tx.codigo === "0"){
                                   if(respCargo.culqicargodirecto.responseCode.toUpperCase() !== ("Error").toUpperCase()){
                                    this.registroLog(dniCliente,modulo,"Se envió CargoDirecto: " + JSON.stringify(respCargo))
                        
                                     this.setState({responseCargoDirecto: respCargo.culqicargodirecto})      
                                     this.registroLog(dniCliente,modulo,"[EXITO] El servicio de cargo directo respondió sin problemas. " + JSON.stringify(respCargo))
                                     this.registroLog(dniCliente,modulo,"[AFILIACION - DATOS PASO1] " + JSON.stringify(this.props.cliente))
                                     this.registroLog(dniCliente,modulo,"[AFILIACION - CLIENTE PASO2] " + JSON.stringify(this.props.empresa))
                                     this.registroLog(dniCliente,modulo,"[AFILIACION - COMPRADORES PASO3] " + JSON.stringify(this.props.listaempresa))
                                     var listaCompradores = [];
                                      
                                     for(var i = 0; i<this.props.listaempresa.length; i++){
                                        let comprador= null;
                                        comprador = {
                                                    "DNICliente"		: dniCliente,
                                                    "RUC"		        : this.props.listaempresa[i].RUC,
                                                    "score"         : "",
                                                    "fgBlackList"   : "N",
                                                    };
                                        
                                        listaCompradores.push(comprador);
                                     }
 
                                      let registro = { 
                                        "DNI": dniCliente,
                                        "numeroCelular": this.props.cotizacion.celular,
                                        "correo": this.props.cotizacion.correo,
                                        "RUCPN": this.props.empresa.RUC,
                                        "direccion": this.props.empresa.Direccion,
                                        "distrito": this.props.empresa.Distrito,
                                        "provincia": this.props.empresa.Provincia,
                                        "departamento": this.props.empresa.Departamento,
                                        "facturacionPromedio": "",
                                        "actividadEconomica": this.props.empresa.Actividad,
                                        "montoMaximoPorFactura": this.props.calculo.Cobertura,
                                        "copago": this.props.calculo.Copago,
                                        "precio": this.props.calculo.Precio,
                                        "listaRegistroCompra" : listaCompradores
                                      }


                                      this.registroVenta(registro);
                                      
                                      
                                      // ENVIANDO CORREO DE BIENVENIDA
                                      this.handlerCorreoBienvenida();
                                     
                                      this.props.handlerLoading(false)
                                      this.setState({showModal:false })
                                      
                                      this.props.handlerLoading(false)
                                      /**error de react */
                                      this.props.updateMensajeCulqiError("El pago se realizó correctamente." , false)
                                      this.setState({showModalError:true }, ()=>this.props.handlerPaso(2))

                                    this.registroLog(dniCliente,modulo,"[EXITO] El servicio de cargo directo funcionó correctamente. " + JSON.stringify(respCargo))                                 

                                    //})
                                    //cuando es correcta la transacción
                                    window.dataLayer.push({
                                        event: 'productTransaction',
                                        //dimension1: this.props.principalcliente == null ? "0" : this.props.principalcliente.IdCliente,
                                        dimension1: this.props.cliente == null ? "0" : this.props.cliente.numerodoc,
                                        ecommerce: {
                                          purchase: {
                                            actionField: {
                                            id: respCargo.culqicargodirecto.ideLogTransaccion,
                                            affiliation: 'Rimac Seguros Online',
                                            revenue: this.props.calculo.Precio,
                                            tax:'0.00',
                                            shipping: '0.00'
                                            },
                                            products: [{
                                            name: 'Seguro para Valientes',
                                            id: 'Rimac-SPV01',
                                            price: this.props.calculo.Precio,
                                            brand: 'Rimac Seguros',
                                            category: 'Riesgos Generales',
                                            variant: '',
                                            quantity: 1
                                          
                                            }]
                                          }
                                        }
                                      });


                                     
                                   }else {                                     
                                    this.registroLog(dniCliente,modulo,"[ERROR] El servicio de cargo directo canceló." + respCargo.culqicargodirecto.mesajeTecnico + " - " + JSON.stringify(respCargo))
                                    this.props.handlerLoading(false)                                  
                                    this.setState({
                                     showModal:false,
                                     showModalError: true,
                                     contenido: <p>Al parecer hemos tenido un problema con nuestro sistema. Lo sentimos, no se ha procesado tu compra. Inténtalo más tarde.</p>,
                                     })
                                   }
                                 }else {
                                  this.registroLog(dniCliente,modulo,"[ERROR] El servicio de cargo directo canceló." + respCargo.culqicargodirecto.mesajeTecnico + " - " + JSON.stringify(respCargo))
                                   this.props.handlerLoading(false)                                  
                                   this.setState({
                                    showModal:false,
                                    showModalError: true,
                                    contenido: <p>Al parecer hemos tenido un problema con nuestro sistema. Lo sentimos, no se ha procesado tu compra. Inténtalo más tarde.</p>,
                                    })
                                 }
                             }else if(respCargo.type === "INVALID_CULQI_CARGODIRECTO"){
                               
                                  // MENSAJE DE ERROR
                                  this.registroLog(dniCliente,modulo,"[ERROR] El servicio de cargo directo canceló." + respCargo.culqicargodirecto.mesajeTecnico + " - " + JSON.stringify(respCargo))
                                   this.props.handlerLoading(false)                                  
                                   this.setState({
                                    showModal:false,
                                    showModalError: true,
                                    contenido: <p>Al parecer hemos tenido un problema con nuestro sistema. Lo sentimos, no se ha procesado tu compra. Inténtalo más tarde. </p>,
                                    })
                                   /**error de react */                                   

                               }
                            })
                         }else {
                           
                          this.registroLog(dniCliente,modulo,"[ERROR] El servicio de token canceló. " + JSON.stringify(repsToken))
                                  
                          this.props.handlerLoading(false)                                  
                          this.setState({
                           showModal:false,
                           showModalError: true,
                           contenido: <p>Al parecer hemos tenido un problema con nuestro sistema. Lo sentimos, no se ha procesado tu compra, inténtalo más tarde. (E002)</p>,
                           })
                         }
                     }else if(repsToken.type === "INVALID_CULQI_TOKEN"){   
                       
                       this.props.handlerLoading(false)
                       /**error de react */                  
                        this.registroLog(dniCliente,modulo,"[ERROR] El servicio de token canceló como INVALID_CULQI_TOKEN " + JSON.stringify(repsToken))
                         this.props.handlerLoading(false)                                  
                         this.setState({
                          showModal:false,
                          showModalError: true,
                          contenido: <p>Al parecer hemos tenido un problema con nuestro sistema. Lo sentimos, no se ha procesado tu compra, inténtalo más tarde.  (E001)</p>,
                          })
                          
                     }
                 }) // Fin Token
                
                } else {
                  this.props.handlerLoading(false)
                  this.props.updateMensajeCulqiError(respTercero.grabarTercero.tx.excepcionLogicaValidacion.mensaje, false)
                  this.setState({showModal:false }, ()=>this.props.handlerPaso(5))
                }
              } else if(respTercero.type === "INVALIDATE_GRAGAR_TERCERO"){              
                this.props.handlerLoading(false)
                this.props.updateMensajeCulqiError("Lo sentimos hemos tenido un problema con nuestros sistemas. Inténtalo más tarde.", false)
                this.setState({showModal:false }, ()=>this.props.handlerPaso(5))
              } else {          
                return false;
              }
            }) // Fin Tercero
      


   
     }else{
       let spnnombrecard = document.getElementById('spvalidatarjetaculqi') ;
       spnnombrecard.innerText = 'Ingrese todos los datos';
     }

    }







    /**VALIDACIONES CULQI */

    validCreditCard=(e)=> {
        this.setState({
            inputcvvcard:'',
            inputnombre:'',
            inputapellido:'',
            inputemailcard:'', 
            inputfechavencimiento:''
        })
        var value = e.target.value;
        value =value.replace(/\D/,'');
        var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '')
        var typecard = this.getTypeCard(value)
        if (typecard === "amex") {
          this.maxLengthCvv = 4;
        }else{
            this.maxLengthCvv = 3;
        }
        this.creditCardClass = ["card-type-icon", "show type-card", typecard];
        var matches = v.match(/\d{4,16}/g);
        var match = matches && matches[0] || ''
        var parts = []
        for (var i=0, len=match.length; i<len; i+=4) {
          parts.push(match.substring(i, i+4))
        }
    
        if (parts.length) {
          this.setState({inputCreditCard: parts.join(' ')});
        } else {
          this.setState({inputCreditCard: value});
        }
        var spnnumbercard = document.getElementById('spvalidatarjetaculqi');
        var logcard = this.getLongCard(typecard)
        if (match.length === logcard) {
          this.isValidNumberCard = true;
          spnnumbercard.innerText='';
          this.datecard.focus();
        } else {
          this.isValidNumberCard = false;
          spnnumbercard.innerText = 'Ingrese una tarjeta de crédito válida.';
        }
        this.validarDatosPago();
      };
    
      getLongCard=(typecard)=>{
        if(typecard === "visa" || typecard === "mastercard" ){
          return 16
        }else if(typecard === "amex"){
          return 15
        }else if(typecard === "dinners"){
          return 14
        }
      }
      
      
      getTypeCard=(number) =>{
        number = number.replace(/ /g, '');
        // visa
        var re = new RegExp("^4");
        if (number.match(re) !== null)
            return "visa";
    
        re = new RegExp("^5[12345]|^2[234567]");
        if (number.match(re) !== null)       
        return "mastercard";
    
        // AMEX
        re = new RegExp("^3[47]");
        if (number.match(re) !== null)
            return "amex";
    
        // Diners
        re = new RegExp("^30|^36|^38|^39");
        if (number.match(re) !== null)
        return "dinners";
    
        return "";
      }
    /*
    validMonthExp=(e)=> {
        var date_exp = e.target.value;
        date_exp = date_exp.replace(
          /^([1-9]\/|[2-9])$/g, '0$1/' // 3 > 03/
        ).replace(
          /^(0[1-9]|1[0-2])$/g, '$1/' // 11 > 11/
        ).replace(
          /^([0-1])([3-9])$/g, '0$1/$2' // 13 > 01/3
        ).replace(
          /^(0?[1-9]|1[0-2])([0-9]{2})$/g, '$1/$2' // 141 > 01/41
        ).replace(
          /^([0]+)\/|[0]+$/g, '0' // 0/ > 0 and 00 > 0
        ).replace(
          /[^\d\/]|^[\/]*$/g, '' // To allow only digits and `/`
        ).replace(
          /\/\//g, '/' // Prevent entering more than 1 `/`
        );
        this.setState({inputfechamonth: date_exp});
        var spnexpcard = document.getElementById('spvalidatarjetaculqi');
        if (date_exp.length === 2) {
          this.isValidDateExp = true;
          spnexpcard.innerText = '';
          this.ccvCard.focus();
        } else {
          this.isValidDateExp = false;
          spnexpcard.innerText = 'Ingrese una fecha de válida.';
        }
        this.validarDatosPago();
    }*/

      validDateExp=(e)=> {
        var date_exp = e.target.value;
       date_exp = date_exp.replace(
          /^([1-9]\/|[2-9])$/g, '0$1/' // 3 > 03/
        ).replace(
          /^(0[1-9]|1[0-2])$/g, '$1/' // 11 > 11/
        ).replace(
          /^([0-1])([3-9])$/g, '0$1/$2' // 13 > 01/3
        ).replace(
          /^(0?[1-9]|1[0-2])([0-9]{2})$/g, '$1/$2' // 141 > 01/41
        ).replace(
          /^([0]+)\/|[0]+$/g, '0' // 0/ > 0 and 00 > 0
        ).replace(
          /[^\d\/]|^[\/]*$/g, '' // To allow only digits and `/`
        ).replace(
          /\/\//g, '/' // Prevent entering more than 1 `/`
        );
        this.setState({inputfechavencimiento: date_exp});
        var spnexpcard = document.getElementById('spvalidatarjetaculqi');
        if (date_exp.length === 5) {
          this.isValidDateExp = true;
          spnexpcard.innerText = '';
          this.ccvCard.focus();
        } else {
          this.isValidDateExp = false;
          spnexpcard.innerText = 'Ingrese una fecha de válida.';
        }
        this.validarDatosPago();
      }
    
      validCvv=(e)=> {
        var cvv = e.target.value;
        cvv = cvv.replace(/\D/,'');
        this.setState({inputcvvcard: cvv});
        var spncvvcard = document.getElementById('spvalidatarjetaculqi');
        if (cvv.length === this.maxLengthCvv) {
          this.isValidCvv = true;
          spncvvcard.innerText = '';
          this.nombreCard.focus();
        } else {
          this.isValidCvv = false;
          spncvvcard.innerText = 'Ingrese una código cvv válido.';
        }
        this.validarDatosPago();
      }
    
      validNombre=(e)=>{
        var nombre = e.target.value;
        this.setState({inputnombre: nombre});
        var spnnombrecard = document.getElementById('spvalidatarjetaculqi');
        if (nombre!=="") {
          this.isValidCvv = true;
          spnnombrecard.innerText = '';
          //this.apellidoCard.focus();
        } else {
          this.isValidCvv = false;
          spnnombrecard.innerText = 'Ingrese el nombre de la tarjeta';
        }
        this.validarDatosPago();
      }

      validApellido=(e)=>{    
        var apellido = e.target.value;
        this.setState({inputapellido: apellido});
        var spnapellidocard = document.getElementById('spvalidatarjetaculqi');
        if (apellido!=="") {
          this.isValidCvv = true;
          spnapellidocard.innerText = '';
         // this.emailCard.focus();
        } else {
          this.isValidCvv = false;
          spnapellidocard.innerText = 'Ingrese el apellido de la tarjeta';
        }
        this.validarDatosPago();
      }
    
    
      validEmail=(e)=> {
        var email = e.target.value;
        this.setState({inputemailcard: email });

        if(this.props.cotizacion != null){
          this.props.cotizacion.correo  = email; 
        } 
        var evalue = this.evaluarEMail(email);
        var spnemailcard = document.getElementById('spvalidatarjetaculqi');
        if (evalue) {
          spnemailcard.innerText = '';
        } else {
          spnemailcard.innerText='Ingrese un correo válido';
        }
        this.validarDatosPago();
      }
    
      evaluarEMail=(email)=>{
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var evalue = re.test(email.toLowerCase());
        if (evalue) {
          this.isValidEmail = true;
        } else {
          this.isValidEmail = false;
        }
        return evalue;
      }
    
      validarDatosPago=()=> {
        if (this.isValidNumberCard === true && this.isValidDateExp === true && this.isValidCvv === true && this.isValidEmail === true && this.state.chkpoliticasrimac === true) {
          this.setState({disabledBtn: ''});
        } else {
          this.setState({disabledBtn: 'disabled'});
        }
      }
    

    /**FIN VALIDACIONES CUQLI */   

    
    handlerchecked=()=> {
      this.setState({chkCondicionesRimac: !this.state.chkCondicionesRimac}, ()=>{
        var spnnombrecard = document.getElementById('spvalidatarjetaculqi');
        if(this.state.chkCondicionesRimac){
          spnnombrecard.innerHTML = ''
        }else{
          spnnombrecard.innerHTML = '(*) Debes aceptar las políticas de envío de la póliza y protección de datos personales'
        }
      })      
    }


    handlerCloseModal = () => {
      this.setState({ showModalInformativo: false })
    }
  
    handlerOpenModal =(caso)=> {
      switch (caso) {
        case 1:
        this.setState({ showModalInformativo: true, contenido: <TerminosPolizaElectronica /> })    
          break;
        case 2:
        this.setState({ showModalInformativo: true, contenido: <TerminosPrivacidadDatos /> })    
          break;
        case 3:
        this.setState({ showModalInformativo: true, contenido: <RenovacionAutomatica /> })    
          break;
      
        default:
          break;
      }      
    }

    handlerAnterior=()=>{
        this.props.handlerPaso(6)
    }

    render(){
        this.functDatalayerStep4();
        let title = <p className="title">Realiza tu pago</p>
        let modal = {
          contenido : this.state.contenido,  
          title:<div>LO SENTIMOS</div>, 
          handlerCloseModal:this.handlerCloseModalError ,
          showModal:this.state.showModalError
        }
        
        return(
          

            <section className="d-flex flex-column mb-full">
              <Pasos paso={3} />
              <div className="container d-flex mb-flex-column">
              <div className="boxWhite">
                    <div className="pageCulqi d-flex mb-flex-column flex-wrap ">
                      <Title value={title} />
                      <div className="pageCulqi-item mb-full d-flex flex-column cienp">
                        <b className="labelForm cienp d-flex justify-content-start">Resumen de mis datos</b>
                      </div>

                      <div className="pageCulqi-item tableResume d-flex cienp">
                        <div className="tableResume-item d-flex flex-column flex-align-start cuarentap mb-full">
                          <p>Nombre del comprador:</p>
                          <p>RUC de la empresa:</p>
                          <p>Actividad económica:</p>
                          <p>Dirección:</p>
                        </div>
                        <div className="tableResume-item d-flex flex-column align-items-end sesentap mb-full">
                          <p>{ this.props.cliente != null ?  this.props.cliente.nomcompleto : "Sin datos" }</p>
                          <p>{ this.props.empresa != null ?  this.props.empresa.RUC : "Sin datos" }</p>
                          <p>{ this.props.empresa != null ?  this.props.empresa.Actividad : "Sin datos" }</p>
                          <p>{ this.props.ubigeo != null ?  this.props.ubigeo.empresa.Direccion : "Sin datos" }</p>
                        </div>
                      </div>

                      <div className="pageCulqi-item mb-full d-flex flex-column w70">                           
                          <b className="labelForm cienp d-flex justify-content-start">Ingresa los datos de tu tarjeta para proceder con la compra</b>
                          <div className="formCulqi d-flex flex-wrap mb-full mar-b-20">
                              <Input 
                                  idculqi="inputCreditCard" 
                                  clase="inputCulqi"
                                  claseicon="credit_card"
                                  name="inputcard" 
                                  data-culqi="card[number]" 
                                  holder="Número de tarjeta" 
                                  value={this.state.inputCreditCard} 
                                  onchange={this.validCreditCard}
                                  creditCardClass={this.creditCardClass.join(' ')}/>                                    
                              <Input 
                                  idculqi="inputfechavencimiento" 
                                  clase="inputCulqi"
                                  claseicon="date_range"
                                  name="datecard" 
                                  holder="MM/AA" 
                                  maxleng="5" 
                                  value={this.state.inputfechavencimiento}
                                  onchange={this.validDateExp}
                                  referencia={(input) => { this.datecard = input; }}  />                                
                              <Input 
                                  idculqi="inputcvvcard" 
                                  clase="inputCulqi"
                                  claseicon="lock_outline"
                                  name="inputcvc" 
                                  data-culqi="card[cvv]" 
                                  holder="CVV" 
                                  maxleng={this.maxLengthCvv}
                                  value={this.state.inputcvvcard}
                                  onchange={this.validCvv}
                                  referencia={(input) => { this.ccvCard = input; }} />
                              <Input 
                                  idculqi="inputnombre" 
                                  clase="inputCulqi"
                                  claseicon="person_outline"
                                  name ="cardname"           
                                  data-culqi="card[name]" 
                                  holder="Nombre"  
                                  value={this.state.inputnombre}
                                  onchange={this.validNombre}
                                  referencia={(input) => { this.nombreCard = input; }} />                                
                              <Input 
                                  idculqi="inputapellido" 
                                  clase="inputCulqi"
                                  claseicon="person_outline"
                                  name ="cardlastname" 
                                  data-culqi="card[last_name]" 
                                  holder="Apellido" 
                                  value={this.state.inputapellido}
                                  onchange={this.validApellido}
                                  referencia={(input) => { this.apellidoCard = input; }}  />
                              <Input 
                                  idculqi="inputemailcard" 
                                  clase="inputCulqi"
                                  claseicon="mail_outline"
                                  name="inputemail" 
                                  data-culqi="card[email]" 
                                  holder="Email" 
                                  value={this.state.inputemailcard}
                                  onchange={this.validEmail}
                                  referencia={(input) => { this.emailCard = input; }} />                                
                          </div>
                          <span id="spvalidatarjetaculqi" className="spanError"></span>

                          <div className="w70 mb-full">
                              <input className="chk-condiciones-rimac" type="checkbox" name="checkbox" checked={this.state.chkCondicionesRimac}  onChange={this.handlerchecked} />
                              <p className="p-condiciones-rimac" >Acepto <a onClick={()=>{this.handlerOpenModal(3)}}>los términos y condiciones.</a></p>
                            
                          </div>	
                          <br/>
                         {/*this.props.calculo.Precio*/}
                          <Button
                              value={"Pagar S/" + (this.props.calculo != null ?  this.props.calculo.Precio : "0")}
                              clase="btn btnAction mar-b-20 w70 mb-full"
                              handlerOnClick={this.handlerSiguiente} />
                          
                          <img className="flex-shrink-0 w55 mb-full mar-b-20" src={imgpago} alt="" />
                      </div>

                      <div className="pageCulqi-item mb-full d-flex flex-column w30 mar-b-20 align-items-center pageCulqi-item-pdt">
                          <img className="flex-shrink-0 w80 " src={ImgCard} alt="" />
                      </div>
                        
                    </div>

                    
                </div>


              
                 <div className="cuarentaycincop ml20 mb-full">
                  <div className="boxWhite mb20">
                    <div className="boxResumenChk d-flex justify-content-center flex-column"> 
                      <b>RESUMEN DE COMPRA</b>
                      
                      <div className="tableResume d-flex">
                        <div className="tableResume-item">
                          <p>Suma asegurada:</p>
                          <p>Te cubrimos el:</p>
                          <p className="mt20">Pago total:</p>
                          {/* <p className="fs14">(Cuota 1 de 12)</p> */}
                        </div>
                        <div className="tableResume-item">
                          <p>S/ {this.props.calculo != null ?  this.props.calculo.Cobertura : "0"}</p>
                          <p> {this.props.calculo != null ?  this.props.calculo.Porcentaje : "0"} %</p>
                          <p className="tablePrice mt20 color-green fs22 bold">S/ {this.props.calculo != null ? this.props.calculo.Precio : "0"}</p>
                          <p className="color-green">mensual</p>
                        </div>
                      </div>

                    </div>
                  </div>

                  <div className="boxWhite">
                    <div className="boxResumenInfo">
                      <b className="boxResumenInfo-title d-flex align-items-center"><i class="material-icons">error</i> IMPORTANTE</b>

                      <p>Cubrimos máximo 06 comprobantes de pago electrónicos retrasados al año. (1 por mes)</p>

                      <p>Los comprobantes de pago electrónicos deben ser emitidos durante la vigencia del seguro.</p>

                    </div>
                  </div>
                </div>




              </div>
                

                
                {this.state.showModal?<LoadingProcesando imagen={imgprocesando} /> : null}

                <ModalInformativo contenido={this.state.contenido}
                                  showModal={this.state.showModalInformativo}
                                  handlerCloseModal={this.handlerCloseModal} />

                <ModalError {...modal} />     
            </section>
            
        )
    }
}



const mapStateToProps = store => {
	return {
        grabarTercero : store.grabarTercero,
        cotizacion: store.cotizacion.cotizacion,
        primactz: store.consultaprima,
        calculo: store.calculo.calculo,
        empresa: store.empresa.empresa,
        principalcliente: store.principalcliente.principalcliente,
        listaempresa: store.listaempresa.listaempresa,    
        cliente: store.tercero.tercero, 
        lead : store.lead.lead,
        ubigeo : store.ubigeoEmpresa
	}
}

const mapDispatchToProps = dispatch => {
	return {
        grabarTercero:(datostercero) => dispatch(fetchGrabarTercero(datostercero)),
        obtenerToken:(datostoken) => dispatch(fetchCulqiToken(datostoken)),
        cargoDirecto:(datospago) => dispatch(fetchCulqicargodirecto(datospago)),
        registrarLog:(dniCliente,modulo,accion) => dispatch(fetchRegistrarLog(dniCliente,modulo,accion)),
        correoBienvenida:(idPlantilla, para, nomCliente, telefono, correo) => dispatch(fetchEnviarCorreo(idPlantilla, para, nomCliente, telefono, correo)),
        gestionarcompra:(compra) => dispatch(fetchgestionarcompra(compra)),
        updateCotizacion: (cotizacion) => dispatch(fetchUpdateCotizacion(cotizacion)),
        registrarVenta:(xdni,xruc,xmontoMaximoPorFactura,xcopago,xprecio) => dispatch(fetchGrabarVenta(xdni,xruc,xmontoMaximoPorFactura,xcopago,xprecio)),
        
	}
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PageCulqi))

