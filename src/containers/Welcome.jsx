import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from "react-router-dom"
import Footer from '../components/commons/Footer';
import Header from '../components/commons/Header';
import Title from '../components/cotizador/commons/Title';
import imgWoman from '../resources/images/iconos/pc-woman.png';
import Pasos from '../components/commons/Pasos'
import { Element, scroller } from 'react-scroll';
import {ROUTE} from '../utils/Constantes'
import GTM from '../utils/GTM'

class Welcome extends Component {

	componentDidMount(){
		
		if(this.props.cotizacion.nombre === undefined){
			this.props.history.push({pathname : ROUTE.URL_LANDING})
		}else{
			let transaccion = {
				id			: this.props.cotizacion.ideTransaccion   				,
				price		: parseFloat(this.props.cotizacion.precioprima)			,
				ideProducto : this.props.cotizacion.ideProducto	 					,	
				tax 		: parseFloat(this.props.cotizacion.precioprima) * (0.18)
			}
			GTM.transaccionCompleta(transaccion)	
			scroller.scrollTo("welcome", {
				duration: 500,
				delay: 0,
				smooth: "easeOutQuad"
			});
		}
	}

	handlerUpdateNombre = (nombre) => {
		if(nombre){
			let nombres = nombre.split(" ");
			let nombreMasc = "";
			for (let i = 0; i < nombres.length; i++) {
				nombreMasc = nombreMasc + nombres[i].charAt(0).toUpperCase() + nombres[i].slice(1).toLowerCase() + " "
			}
			nombreMasc = nombreMasc.substring(0, nombreMasc.length-1)
			return nombreMasc;
		}else {
			return "";
		}
	}
	
	render() {
		
		let nombre = this.props.cotizacion.nombre 
		let title = <p className="bxTitle mb-full w100 mar-0-auto text-center">¡Felicidades, <b>{this.handlerUpdateNombre(nombre)}!</b> <br /> Ya estás protegid{this.props.cotizacion.idpgenero==="F"?"a":"o"} con el <b>seguro Respaldo Oncológico Ponle Corazón RIMAC.</b></p>
		return (
			<Element name="welcome">
				<div>
					<Header />					
						<section className="section-cotizador">
							<section className="container">
								<div className="boxWhite">
									<Pasos paso={5} />
									<div className="bxWelcome d-flex flex-column align-items-center">
										<Title value={title} />
										<div className="mb-full w55 d-flex flex-column align-items-center">
											<div className="bxInfo d-flex align-items-center">
												<div className="bxInfo-item d-flex flex-column">
													<span>Y NO SÓLO ESO:</span>
													<p><b>20%</b> del monto pagado ha ido a Ponle Corazón ¡Para mejorar la vida de muchas personas!</p>
												</div>
												<img src={imgWoman} alt="" />
											</div>
											<p>Enviaremos un correo de confirmación con toda la información de póliza a <b>{this.props.cotizacion.correo}</b> en las próximas horas.</p>
										</div>
									</div>
								</div>
							</section>
						</section>					
					<Footer />
				</div>
			</Element>
		)
	}
}


const mapStateToProps = store => {
	return {
		cotizacion: store.cotizacion.cotizacion
	}
}



export default withRouter(connect(mapStateToProps, null)(Welcome))