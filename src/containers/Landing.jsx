import React, { Component } from 'react';
import { withRouter } from "react-router-dom";
import { Element, scroller } from 'react-scroll';
import Footer from '../components/commons/Footer';
import Cobertura from '../components/landing/Cobertura';
import Header from '../components/landing/Header';
import Home from '../components/landing/Home';
import Porque from '../components/landing/Porque';
import SectionComoComprar from '../components/landing/SectionComoComprar';
import SectionComoFunciona from '../components/landing/SectionComoFunciona';
import comocomprarpaso1 from '../resources/images/sectioncomocomprar/5.png';
import comocomprarpaso2 from '../resources/images/sectioncomocomprar/6.png';
import comocomprarpaso3 from '../resources/images/sectioncomocomprar/7.png';
import comocomprarpaso4 from '../resources/images/sectioncomocomprar/8.png';
import comousarlopaso1 from '../resources/images/sectioncomousarlo/9.png';
import comousarlopaso2 from '../resources/images/sectioncomousarlo/10.png';
import comousarlopaso3 from '../resources/images/sectioncomousarlo/11.png';
import {ROUTE} from '../utils/Constantes'
import GTM from '../utils/GTM'
import { deslizarBtnMas } from '../resources/js/events.js';
import ModalVideo from '../components/commons/ModalVideo'
import Faqs from '../components/commons/Faqs'

class Landing extends Component {
	state = {
		showModalVideo : false,

		listacomofunciona: {
			header: {
				title: '¿Cómo comprar el Seguro?', subtitle: 'Olvídate de los trámites y asegúrate en 4 simples pasos 100% online', clases: { container: '', title: 'title', subtitle: 'subtitle' }
			},
			pasos: [
				{   
					tooltip: false,
					imagen: comocomprarpaso1,
					details: 'Regístrate en nuestra plataforma online.',
					clases: { container: 'd-flex-landing align-items-center justify-content-center', img: '', detail: '' }
				},
				{	
					tooltip: false,
					imagen: comocomprarpaso2,
					details: 'Ingresa el RUC de tus clientes y elige tu plan.',
					clases: { container: 'd-flex-landing align-items-center justify-content-center', img: '', detail: '' }
				},
				{	
					tooltip: false,
					imagen: comocomprarpaso3,
					details: 'Realiza tu pago',
					clases: { container: 'd-flex-landing align-items-center justify-content-center', img: '', detail: '' }
				}
				,
				{	
					tooltip: false,
					imagen: comocomprarpaso4,
					details: '¡Recibirás la confirmación de tu pago por correo!',
					clases: { container: 'd-flex-landing align-items-center justify-content-center', img: '', detail: '' }
				}
			]
		},

		listacomocomprar: {
			header: {
				title: '¿Cómo usar el seguro cuando lo necesites?', subtitle: 'Lo hicimos fácil para esos momentos difíciles.', clases: { container: '', title: 'title', subtitle: 'subtitle' }
			},
			pasos: [
				{
					tooltip: false,
					imagen: comousarlopaso1,
					details: 'Ingresa a nuestra plataforma online.',
					clases: { container: 'd-flex-landing align-items-center justify-content-center', img: '', detail: '' }
				},
				{
					tooltip: false,
					imagen: comousarlopaso2,
					details: 'Registra tu comprobante de pago retrasado.',
					clases: { container: 'd-flex-landing align-items-center justify-content-center', img: '', detail: '' }
				},
				{
					tooltip: false,
					imagen: comousarlopaso3,
					details: 'Ingresa tu cuenta bancaria para que realicemos el abono.',
					clases: { container: 'd-flex-landing align-items-center justify-content-center', img: '', detail: '' }
				}
			]
		}
	}

	
	handlerCotizador = (seccion) => {
		GTM.enviarCotizador(seccion)
		this.props.history.push({pathname : ROUTE.URL_REGISTRAR});
	}

	handlerScroll = (element) => {
		deslizarBtnMas("menu-btn-mas", "dropdown1", 0)
		scroller.scrollTo(element, {
			duration: 800,
			delay: 0,
			smooth: "easeOutQuad"
		});
	}

	handlerCloseVideo = () =>{
		this.setState({showModalVideo : false })
	}

	handlerShowVideo = () => {
		this.setState({showModalVideo : true })
		window.dataLayer.push({
            event    : 'virtualEvent',
            category : 'Seguro para Valientes',
            action   : 'Home - Clic Botón',
            label    :  'Mirar el video'

        });
	}

	render() {
		let btncomofunciona = {
			handleOnClick: this.handlerCotizador,
			value: '¡Estoy Listo!',
			clases: ' btn-landing-comprar btn-landing-pasos',
			labelgtm: 'Estás listo - Como Usarlo'
		}

		let btncomocomprar = {
			handleOnClick: this.handlerCotizador,
			value: 'Cómpralo Online',
			clases: ' btn-landing-comprar btn-landing-pasos',
			labelgtm: 'Cómpralo Online - Cómo Comprar'
		}

		return (
			<div>
				<Element name="header">
					<Header handlerScroll={this.handlerScroll}
						handlerOptionesMenu={GTM.optionesMenu}
						handlerCotizador={this.handlerCotizador} />
				</Element>
				<Element name="home">
					<Home className="d-flex justify-content-center"  handlerScroll={this.handlerScroll}
						handlerCotizador={this.handlerCotizador}
						handlerShowVideo={this.handlerShowVideo} />
				</Element>
				<Element name="porque">
					<Porque className="d-flex" />
				</Element>
				<Element name="cobertura">
					<Cobertura handlerVerExclusiones={GTM.verExclusiones} />
				</Element>
				<Element name="comofunciona">
					<SectionComoFunciona
						listacomofunciona={this.state.listacomofunciona}
						buttons={btncomofunciona} />
				</Element>
				<Element name="comocomprar">
					<SectionComoComprar
						listacomocomprar={this.state.listacomocomprar}
						buttons={btncomocomprar} />
				</Element>

				<Faqs />

				<Footer />

				<ModalVideo handlerCloseModal={this.handlerCloseVideo} 
							showModal={this.state.showModalVideo} />
			</div>
		)
	}
}

export default withRouter(Landing)