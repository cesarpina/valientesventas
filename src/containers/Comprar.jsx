import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchCONSULTAEMPRESA } from '../actions/consultarDatosEmpresa';
// import Input from '../components/Comprar/commons/Input'
// import Button from '../components/Comprar/commons/Button'
// import Pasos from '../components/commons/Pasos'
import UtilsJs from '../utils/Funciones';
import { Element, scroller } from 'react-scroll';
import Footer from '../components/commons/Footer';
import Header from '../components/commons/Header';
import {ROUTE} from '../utils/Constantes'
import GTM from '../utils/GTM'
import { deslizarBtnMas } from '../resources/js/events.js';
import PageCulqi from '../containers/culqi/PageCulqi';
import PageThanku from '../containers/cotizador/PageThanku';

class Comprar extends Component {

	constructor(props){
		super(props)
		this.state = {
			isPaso1: false, // Registrar -> Formulario de contacto
			isPaso2: true, // Gracias -> Gracias por tu compra
		}
	}
	
	state = {
		nrodni: '',
		rucCompradores : '',
		razonSocialCompradores : '',
		validruc : false,
		validrucComprador: false,
		validadistrito: false,
		score: '',
		array:[]
	}

	updateMensajeCulqiError = (value, isShow) => {
		this.setState({mensajeculqi: value, showVolver: isShow});
	}

	handlerCot = () => {
		// funcion
		this.props.history.push({pathname : ROUTE.URL_COMPRAR});
	}
	handlerLoading = (value) => {
		this.setState({isLoading : value})
	}

	handlerScroll = (element) => {
		//deslizarBtnMas("menu-btn-mas", "dropdown1", 0)
		scroller.scrollTo(element, {
			duration: 800,
			delay: 0,
			smooth: "easeOutQuad"
		});
	}

	handlerSiguiente = () => {
		this.props.handlerLoading(true)
			// GTM.pasosComprar('Paso 3: Preguntas', this.props.cotizacion.precioprima)
			this.props.handlerPaso(2)
	}

	handlerPaso = (paso) => {
		for (let i = 1; i <= 9; i++) {
			if (i === paso) {
				this.setState({
					["isPaso" + i]: true
				})
			} else {
				this.setState({
					["isPaso" + i]: false
				})
			}
		}
	}


	handlerCS = (rucCompradores) =>{
			 		
			var dni = this.props.principalcliente.DniCliente
			this.props.consultaEmpresa(dni, rucCompradores).then(resp => {		
				if(resp.type === "RECEIVE_CONSULTAEMPRESA"){
					 
					// var cliente = JSON.parse(resp.cliente.body);
					if (resp.empresa.RazonSocial !== null  ) {
							this.setState({
								rucCompradores		: rucCompradores,
								razonSocialCompradores		: resp.empresa.RazonSocial,
							}, () => {
								
								this.handlerLoading(false)
							});

							var obj = {rucCompradores		: rucCompradores,
								razonSocialCompradores		: resp.empresa.RazonSocial};
					
							var array3=this.state.array;
							array3.push(obj);
							this.setState({array:array3});
 
							//this.handlerListaEmpresa(this.state.array);
						
					} else {
						/** mensaje no contamos con sus datos */
						this.setState({
							showModalError: true,
							contenido: <p>No te tenemos registrado. (ERROR-ND)</p>,
							rucCompradores		: rucCompradores
						})
					}
				}else if(resp.type === "INVALIDATE_CONSULTACLIENTES"){
					this.setState({
						showModalError: true,
						contenido: <p>Hemos tenido un problema con nuestro sistema, intentelo más tarde. (ERROR-EQ)</p>,
						rucCompradores		: rucCompradores
						
					})
				}			
				
			})


	}



	handlerOnChange = (e) => {
		//almaceno valr del RUC en el evento
		
		
		//validando ruc cantidad de digitos
		if (e.target.name === "rucCli" && UtilsJs.validaruc(e.target.value, "ruc")) {
			this.handlerLoading(true)
			var numruc = e.target.value;
			var dni = this.props.principalcliente.DniCliente
			this.props.consultaEmpresa(dni, e.target.value).then(resp => { 
				if(resp.type === "RECEIVE_CONSULTAEMPRESA"){ 
					// var cliente = JSON.parse(resp.cliente.body);
					if (resp.empresa.RazonSocial !== null  ) {
							this.setState({
								rucCli		: numruc,
								Actividad		: resp.empresa.Actividad,
								Direccion			: resp.empresa.Direccion,
								Distrito			: resp.empresa.Distrito,
								Provincia			: resp.empresa.Provincia,
								Departamento			: resp.empresa.Departamento,
								validruc	: true
							}, () => {
								
								this.handlerLoading(false)
							})
						
					} else {
						/** mensaje no contamos con sus datos */
						this.setState({
							showModalError: true,
							contenido: <p>No te tenemos registrado. (ERROR-ND)</p>,
							numruc		: numruc
						})
					}
				}else if(resp.type === "INVALIDATE_CONSULTACLIENTES"){
					this.setState({
						showModalError: true,
						contenido: <p>Hemos tenido un problema con nuestro sistema, intentelo más tarde. (ERROR-EQ)</p>,
						numruc		: numruc
						
					})
				}			
				
			})
		} else if (e.target.name === "nrodni" && !UtilsJs.validadni(e.target.value, "dni")) {
			this.setState({
				nrodni: '',
				apepaterno: '',
				apematerno: '',
				Nombres: '',
				nombre: '',
				nombreapellidoMasc: '',
				idpgenero: '',
				fecnacimiento: '',
				celular: '',
				correo: '',
				nombreapellido: '',
				validdni: "error",
				validnombre: false,
				validcelular: false,
				validcorreo: false,
				validgenero: false 
			})
		}



		if (e.target.name === "ruc-comprador") {
			if (UtilsJs.validasolonumero(e.target.value, "ruc-comprador")) {
				this.setState({ rucCompradores: e.target.value })
			} else {
				this.setState({ validadistrito: "error" })
			}
		}

		if (e.target.name === "facturacion-promedio") {
			if (UtilsJs.validasolonumero(e.target.value, "facturacion-promedio")) {
				this.setState({ validadistrito: true })
			} else {
				this.setState({ validadistrito: "error" })
			}
		}

		if (e.target.name === "direccion-fiscal") {
			if (UtilsJs.validasololetras(e.target.value, "direccion-fiscal")) {
				this.setState({ validadistrito: true })
			} else {
				this.setState({ validadistrito: "error" })
			}
		}
		if (e.target.name === "distrito") {
			if (UtilsJs.validasololetras(e.target.value, "distrito")) {
				this.setState({ validadistrito: true })
			} else {
				this.setState({ validadistrito: "error" })
			}
		}
		if (e.target.name === "provincia") {
			if (UtilsJs.validasololetras(e.target.value, "provincia")) {
				this.setState({ validadistrito: true })
			} else {
				this.setState({ validadistrito: "error" })
			}
		}
		if (e.target.name === "departamento") {
			if (UtilsJs.validasololetras(e.target.value, "departamento")) {
				this.setState({ validadistrito: true })
			} else {
				this.setState({ validadistrito: "error" })
			}
		}

	}
	validacionSesion = () => { 
	 
		if(this.props.calculo==null){
			this.props.history.push({pathname : ROUTE.URL_REGISTRAR});
		}
	
		}





	render() {
	
		return (
			<h1>COMPRAR</h1>
		)
	}



	render() {
		this.validacionSesion();
		let { isPaso1, isPaso2} = this.state
		return (
			<Element name="ComprarTop">
				<div>
					<Header />
					<section className="section-Comprar">
						{isPaso1 ? <PageCulqi handlerLoading={this.handlerLoading}
												handlerUpdateNombre={this.handlerUpdateNombre} 
												handlerPaso={this.handlerPaso} 
												handlerOnChange={this.handlerOnChange}
												handlerUpdateDatoCorreo={this.handlerUpdateDatoCorreo}
												handlerCot={this.handlerCot}
												updateMensajeCulqiError={this.updateMensajeCulqiError}
												/> : null}

						{isPaso2 ? <PageThanku 	nombre={this.state.nombre} 
													prima={this.props.cotizacion} 
													handlerPaso={this.handlerPaso} 
													handlerOnChange={this.handlerOnChange}
													handlerCot={this.handlerCot }
													/> : null}
					</section>
					<Footer handlerPaso={this.handlerPaso} />
				</div>

				
			</Element>
		)
	}



}



const mapStateToProps = store => {
	return {
		empresa: store.empresa.empresa,//store.tercero.tercero,
		calculo: store.calculo.calculo,
		cotizacion: store.cotizacion.cotizacion,
		listaempresa: store.listaempresa.listaempresa,
        principalcliente: store.principalcliente.principalcliente
	}
}

const mapDispatchToProps = dispatch => {
	return {
		consultaEmpresa: (dni, ruc) => dispatch(fetchCONSULTAEMPRESA(dni, ruc)),
		
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Comprar)