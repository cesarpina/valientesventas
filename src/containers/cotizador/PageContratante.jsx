import React, { Component } from 'react';
import { Input as InputCalendar} from 'react-materialize';
import ModalInformativo from '../../components/commons/ModalInformativo'; 
import TerminosPrivacidadDatos from '../../components/commons/TerminosPrivacidadDatos'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import * as ConstantesAction from '../../actions/obtenerConstante'
import { fetchconsultaprima } from '../../actions/calcularprima';
import { fetchUpdateCotizacion } from '../../actions/cotizacion'; 
import { fetchPersonaEquifax } from '../../actions/personaEquifax';
import { fetchEvaluacionClientes } from '../../actions/evaluacionClientes';
import { fetchRegistrarLog } from '../../actions/registrarLog'
import GTM from '../../utils/GTM'
import Button from '../../components/cotizador/commons/Button';
import Input from '../../components/cotizador/commons/Input';
import Title from '../../components/cotizador/commons/Title';
import ModalError from '../../components/commons/ModalError';
import ImgContra from '../../resources/images/iconos/spv/contratante.png';
import UtilsJs from '../../utils/Funciones';
import calendar from '../../resources/images/iconos/calendar.png';
import {Input as InputMaterialize} from 'react-materialize';
import {ROUTE} from '../../utils/Constantes';


class PageContratante extends Component {

		constructor(props){
		super(props)
		this. state = {
		idelead: '',
		nombreapellidoMasc: '',
		nrodni: '',
		nroce: '',
		apepaterno: '',
		apematerno: '',
		Nombres: '',
		nombre: '',
		nombreMasc: '',
		apepaternoMasc: '',
		apematernoMasc: '',
		idpgenero: '',
		fecnacimiento: '',
		celular: '',
		correo: '',
		precioprima: 20,
		tipodocdni: true,
		tipodocce: false,
		nombreapellido: '',
		validdni: false,
		validce: false,
		validnombre: false,
		validapepaterno: false,
		validapematerno: false,
		validcelular: false,
		validcorreo: false,
		validfechanacimiento: false,
		validgenero: false,
		showModalError: false,
		contenido: '',
		showModalInformativo : false,
		persona:[],
		id : '',
		varia: false
	
	}  
} 


	functDatalayerStep1(varia){
		if(this.state.varia === false){
			window.dataLayer.push({
				event: 'checkoutOption',
				ecommerce: {
					checkout_option: {
						actionField: {'step': 1}
						}
				}
			});
			this.state.varia = true
		}
		
		
	}
	
	componentWillMount() {	
		this.props.constantesAction.loadConstantes();
	}

	handlerMascNombre = (nombre) => {
		let nombres = nombre.split(" ");
		let nombreMasc = "";
		for (let i = 0; i < nombres.length; i++) {
			nombreMasc = nombreMasc + nombres[i].charAt(0).toUpperCase() + nombres[i].slice(1).toLowerCase() + " "
		}

		nombreMasc = nombreMasc.substring(0, nombreMasc.length-1)
		this.setState({nombreapellidoMasc: nombreMasc})
	}

	calcularEdad = (fecha) => {
		var isOk = false
		var campo = fecha.split("/")
		var hoy = new Date()
        var cumpleanos = new Date(campo[1] +'/'+campo[0] +'/'+campo[2])
        var edad = hoy.getFullYear() - cumpleanos.getFullYear() 
		var m = hoy.getMonth() - cumpleanos.getMonth()
		if(edad < 67){
			isOk = true 
		}else if(edad === 67){
			if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {				
				isOk = true
			}	
		}
		return isOk
	}

	handlerCloseModal = () => {
		this.setState({ showModalInformativo: false })
	  }
	
	  handlerOpenModal =(caso)=> {
		switch (caso) { 
		  case 2:
		  this.setState({ showModalInformativo: true, contenido: <TerminosPrivacidadDatos /> })    
			break;	
		  default:
			break;
		}      
	  }

	  registroLog=(dniCliente,modulo,accion)=>{
		this.props.registrarLog(dniCliente,modulo,accion).then(respLog=> {           
		})
	  }

	handlerOnChange = (e) => {
		var numdni = e.target.value;
		if (e.target.name === "nrodni" && UtilsJs.validadni(e.target.value, "dni")) {
			this.props.handlerLoading(true)
			this.registroLog(numdni,"Datos de cliente","[LEAD "+numdni+"] " + "CONSULTO SUS DATOS PERSONALES")
			this.props.consultaTercero(e.target.value).then(resp => {				
				if(resp.type === "RECEIVE_PERSONAEQUIFAX"){
					 
					if (resp.tercero !== null  ) {
							this.setState({
								nrodni		: numdni,
								Nombres		: resp.tercero.nombre+ ' '+resp.tercero.apepaterno+ ' ' +resp.tercero.apematerno,
								//id			: resp.tercero.IdTercero,
								validnombre	: true,
								validdni	: true
							}, () => {
								this.handlerMascNombre(this.state.nombreapellido)							
								document.getElementById("label-nombreapellidoMasc").className = "active label-input"
								this.props.handlerUpdateNombre(this.state.nombre)
								this.props.handlerLoading(false)
								var obj = {	nrodni		: this.state.nrodni,
											Nombres		:  this.state.Nombres,
											idTecero 	: this.state.id
										
										};
								
								var xpersona=this.state.persona;
								xpersona=obj;
								this.setState({persona:xpersona}); 
							})
						
					} else {
						/** mensaje no contamos con sus datos */
						this.setState({
							showModalError: true,
							contenido: <p>El DNI ingresado no se encuentra registrado. </p>,
							nrodni		: numdni
						})
						this.setState({nrodni: ''})
						this.props.handlerLoading(false)
					}
				}else if(resp.type === "INVALIDATE_PERSONAEQUIFAX"){
					this.setState({
						showModalError: true,
						contenido: <p>Hemos tenido un problema con nuestro sistema, intentelo más tarde. (ERROR-EQ)</p>,
						nrodni		: numdni
						
					})
					this.props.handlerLoading(false)
				}			
				
			})
		} else if (e.target.name === "nrodni" && !UtilsJs.validadni(e.target.value, "dni")) {
			this.setState({
				nrodni: '',
				apepaterno: '',
				apematerno: '',
				Nombres: '',
				nombre: '',
				nombreapellidoMasc: '',
				idpgenero: '',
				fecnacimiento: '',
				celular: '',
				correo: '',
				nombreapellido: '',
				validdni: "error",
				validnombre: false,
				validcelular: false,
				validcorreo: false,
				validgenero: false 
			})
		}

 

		if (e.target.name === "nroce") {
			if (UtilsJs.validace(e.target.value, "ce")) {
				this.setState({ validce: true })
			} else {
				this.setState({ validce: "error" })    
			}
		}

		if (e.target.name === "nombreapellidoMasc") {
			this.props.handlerUpdateNombre(e.target.value)
			if (UtilsJs.validasololetras(e.target.value, "nombre")) {
				this.setState({ validnombre: true ,nombreapellido: e.target.value})
			} else {
				this.setState({ validnombre: "error",nombreapellido: e.target.value })
			}
		}

		if (e.target.name === "nombre") {
			this.props.handlerUpdateNombre(e.target.value)
			if (UtilsJs.validasololetras(e.target.value, "nombre")) {
				this.setState({ validnombre: true })
			} else {
				this.setState({ validnombre: "error" })
			}
		}

		if (e.target.name === "apepaterno") {
			if (UtilsJs.validasololetras(e.target.value, "apepaterno")) {
				this.setState({ validapepaterno: true })
			} else {
				this.setState({ validapepaterno: "error" })
			}
		}

		if (e.target.name === "apematerno") {
			if (UtilsJs.validasololetras(e.target.value, "apematerno")) {
				this.setState({ validapematerno: true })
			} else {
				this.setState({ validapematerno: "error" })
			}
		}

		if (e.target.name === "correo") {
			if (UtilsJs.validacorreo(e.target.value, "correo")) {
				this.setState({ validcorreo: true })
			} else {
				this.setState({ validcorreo: "error" })
			}
		}

		if (e.target.name === "celular") {
			if (UtilsJs.validatelefono(e.target.value, "celular")) {
		 
				this.setState({ validcelular: true })
			} else {
				this.setState({ validcelular: "error" })
			}
		}

		this.setState({
			[e.target.name]: e.target.value
		}, () => {
			this.props.updateCotizacion(this.getDatosContratante())
		})
	}

	getDatosContratante() {
		return {
			tipodocdni	: this.state.tipodocdni				,
			tipodocce	: this.state.tipodocce				,
			nroce		: this.state.nroce					,	
			nrodni		: this.state.nrodni					,
			apepaterno	: this.state.apepaterno				,
			apematerno	: this.state.apematerno				,
			Nombres	: this.state.Nombres			,
			nombre		: this.state.nombre					,
			idpgenero	: this.state.idpgenero				,
			fecnacimiento	: this.state.fecnacimiento		,
			precioprima		: this.state.precioprima		,
			ideProducto     : "4507"						,
			celular			: this.state.celular			,
			correo			: this.state.correo				,
			nombreapellido	: this.state.nombreapellido		,
			validdni		: this.state.validdni			,
			validce			: this.state.validce			,
			validnombre		: this.state.validnombre		,
			validapepaterno	: this.state.validapepaterno	,
			validapematerno	: this.state.validapematerno	,
			validcelular	: this.state.validcelular		,
			validcorreo		: this.state.validcorreo		,
			usuario			: this.props.constantes.PDIG_USUARIO_COT_IEG	,
			usuarioCot		: this.props.constantes.PDIG_USUARIO_COT_IEG    ,
			validfechanacimiento	: this.state.validfechanacimiento	,
			validgenero				: this.state.validgenero			,
			nombreapellidoMasc		: this.state.nombreapellidoMasc		,
			idelead					: this.state.idelead				,
		}
	}




	handlerSiguiente = (principalcliente) => { 
		this.registroLog(this.state.nrodni,"Datos de cliente","[LEAD "+this.state.nrodni+"] " + "AVANZO AL FORMULARIO DE DATOS DE NEGOCIO")
		let { validdni, validnombre, validapepaterno, validapematerno, validcelular, validcorreo, validce, validfechanacimiento } = this.state
		let params = {}
		if (this.state.tipodocdni) {
			if (validdni && validnombre && validcelular && validcorreo && (validdni !== "error" && validnombre !== "error" && validcelular !== "error" && validcorreo !== "error")) {
				this.props.handlerLoading(true)
				this.props.handlerUpdateDatoCorreo(this.state.nrodni, "DNI", this.state.celular, this.state.correo)
				params = {
					Nombres: principalcliente.Nombres +' '+ principalcliente.ApellidoPaterno + ' ' + principalcliente.ApellidoMaterno,
					useragent: "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36",

				}

				
				this.props.resgistrarLead(params).then(resp => {					
					this.setState({}, () => {	//this.setState({idelead : resp.lead.idelead}, () => {	
						this.props.updateCotizacion(this.getDatosContratante())
						// GTM.pasosCotizador('Paso 1: Datos','Envío Satisfactorio')
						this.props.handlerLoading(false)						
						this.props.handlerPaso(2)
					})				
				})
				
			}
		}
		if (this.state.tipodocce) {
			if (validce && validnombre && validapepaterno && validapematerno && validcelular && validcorreo && validfechanacimiento && (validce !== "error" && validnombre !== "error" && validcelular !== "error" && validcorreo !== "error" && validfechanacimiento !== "error")) {
				this.props.handlerLoading(true)
				this.props.handlerUpdateDatoCorreo(this.state.nroce, "CE", this.state.celular, this.state.correo)
				params = {
					idptipodocumento: "CE",
					numerodocumento: this.state.nroce,
					Nombres: this.state.nombre + " "+ this.state.apepaterno + " " +this.state.apematerno,
					fecnacimiento: this.state.fecnacimiento,
					idpgenero: "N",
					numerotelf: this.state.celular,
					email: this.state.correo,
					precio: this.props.cotizacion.precioprima,
					useragent: "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36",
					nombres: this.state.nombre,
					apepaterno: this.state.apepaterno,
					apematerno: this.state.apematerno,
					ideProd: "4507",
					fgVentaFinalizada: "N"
				}

				this.props.resgistrarLead(params).then(resp => {
					this.setState({idelead : resp.lead.idelead}, () => {
						this.props.updateCotizacion(this.getDatosContratante())		
						// GTM.pasosCotizador('Paso 1: Datos','Envío Satisfactorio')				
						this.props.handlerLoading(false)
						this.props.handlerPaso(2)
					})
				})
			}
		}
		this.setState({
			validdni: (this.state.validdni === false ? "error" : this.state.validdni),
			validce: (this.state.validce === false ? "error" : this.state.validce),
			validapepaterno: (this.state.validapepaterno === false ? "error" : this.state.validapepaterno),
			validapematerno: (this.state.validapematerno === false ? "error" : this.state.validapematerno),
			validnombre: (this.state.validnombre === false ? "error" : this.state.validnombre),
			validcelular: (this.state.validcelular === false ? "error" : this.state.validcelular),
			validcorreo: (this.state.validcorreo === false ? "error" : this.state.validcorreo)
		})
	}

	handlerTipoDoc = (e) => {
		if (e.target.value === "dni") {
			this.setState({ tipodocdni: true, tipodocce: false })
		} else if (e.target.value === "ce") {
			this.setState({ tipodocdni: false, tipodocce: true })
		}

		this.setState({
			nrodni: '',
			Nombres: '',
			nroce: '',
			apepaterno: '',
			apematerno: '',
			Nombres: '',
			nombre: '',
			idpgenero: '',
			fecnacimiento: '',
			celular: '',
			correo: '',
			nombreapellido: '',
			validdni: false,
			validce: false,
			validnombre: false,
			validapematerno: false,
			validapepaterno: false,
			validcelular: false,
			validcorreo: false,
			validfechanacimiento: false
		})
	}

	onChangeFechaVigencia = (e) => {
		this.setState({fecnacimiento: e.target.value,validfechanacimiento: true})
		this.props.consultaprima({fechaNacimiento : e.target.value, plan: "1581"}).then(resp => {	
			if(resp.type === "RECEIVE_CONSULTA_PRIMA"){
				this.setState({ precioprima: resp.datosprima.precioPrima },()=>this.props.updateCotizacion(this.getDatosContratante()))
			}
			else if(resp.type === "INVALID_CONSULTA_PRIMA"){		
				this.setState({
					showModalError: true,
					contenido: <p>Hemos tenido un problema con nuestro sistema, intentelo más tarde o llámanos al 411-1111</p>,
					nrodni: '',
					apepaterno: '',
					apematerno: '',
					Nombres: '',
					nombre: '',
					idpgenero: '',
					fecnacimiento: '',
					celular: '',
					correo: '',
					nombreapellido: '',
					validce: false,
					validdni: false,
					validnombre: false,
					validcelular: false,
					validcorreo: false
				})
			}
		})	
	}

	handlerCloseModalError = () => {
		this.setState({
			showModalError :  false
		})
	}

	handlerNextPage = () => {

		
	}

	render() {
		let fecha = new Date();
		let month = fecha.getMonth() + 1;
		let day = fecha.getDate();
		let yearMax = fecha.getFullYear() - 18;		
		let yearMin = fecha.getFullYear() - 90;	
		day = day < 10 ? '0'+day:day;	
		month = month < 10 ? '0'+month : month;

		const optionsFecIniVigencia = {
			min: day + '/' + month + '/' + yearMin ,
			max: day + '/' + month + '/' + yearMax ,
			today: '',
			clear: '',
			close: 'OK',
			format: 'dd/mm/yyyy',
			closeOnSelect: false,
			monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
			monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
			weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
			weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
			selectYears: true,
			selectMonths: true,
	  	};

		let modal = {
			contenido : this.state.contenido,  
			title:<div>LO SENTIMOS</div>, 
			handlerCloseModal:this.handlerCloseModalError ,
			showModal:this.state.showModalError
		}

		let { validdni, validce, validnombre, validapepaterno, validapematerno, validcelular, validcorreo } = this.state;

		let title = <p className="bxTitle mb-full" >Ingresa tus datos para adquirir este seguro</p>;
		
		this.functDatalayerStep1();
		
		return (
			
			<section className="container">
				<div className="bxContratante d-flex mb-flex-column justify-content-center 2c">
					<div className="boxTrasnp d-flex flex-column mb-align-items-center mb-full">
						
						<span>¡Adquiere el seguro para Valientes de RIMAC!</span>
						{/* <p>Este seguro incluye:</p> */}
						<div className="boxTrasnp-item d-flex align-items-center justify-content-start">
							{/*<img src={IcoDiscount} alt="" />
							<p>¡Aprovecha la Pre Venta! Obtén un 20% de descuento en el precio mensual por todo el año.</p>*/}
							 
						</div>
						<img src={ImgContra} alt=""/>
					</div>
					<div className="boxWhite flex-grow0">
						<div className="bxform mb-fluid">
							<Title className="bx" value={title} />
							<div className="div-contenedor-tipodoc">
								<div className="d-flex justify-content-between mb-flex-column">
									<label>
										<input className="with-gap" name="group3" type="radio" value="dni" checked={this.state.tipodocdni} onChange={this.handlerTipoDoc} />
										<span>DNI</span>
									</label>
									{/* <label>
										<input className="with-gap" name="group3" type="radio" value="ce" disabled />
										<span>Carnet de Extranjería</span>
									</label> */}
								</div>
							</div>						
						
							{this.state.tipodocdni ?
								<div>
									<Input
										name="nrodni"
										holder="DNI"
										value={this.state.nrodni}
										handlerOnChange={this.handlerOnChange}
										longitud="8"
										check={validdni} />

									<Input
										name="nombreapellidoMasc"
										holder="Nombre y Apellido"
										value={this.state.Nombres}
										handlerOnChange={this.handlerOnChange}
										disableCampo={true}
										check={validnombre} />
								</div> :
								<div>
									<Input
										name="nroce"
										holder="CE"
										value={this.state.nroce}
										handlerOnChange={this.handlerOnChange}
										minlog="8"
										longitud="12"
										check={validce} />
									<Input
										name="nombre"
										holder="Nombre"
										value={this.state.nombre}
										handlerOnChange={this.handlerOnChange}
										check={validnombre} />
									<Input
										name="apepaterno"
										holder="Apellido Paterno"
										value={this.state.apepaterno}
										handlerOnChange={this.handlerOnChange}
										check={validapepaterno} />
									<Input
										name="apematerno"
										holder="Apellido Materno"
										value={this.state.apematerno}
										handlerOnChange={this.handlerOnChange}
										check={validapematerno} />
									
									<div className="input-contratante">
										<InputCalendar 
											id="date"
											type="date"
											name="on"
											className="datepicker"
											placeholder="Fecha Nacimiento"
											options={optionsFecIniVigencia}
											onChange={this.onChangeFechaVigencia} >		
										</InputCalendar>	
										<img className="input-check input-img-calendar" src={calendar} alt="" /> 									
									</div>

								</div>
							}

							<Input
								name="celular"
								holder="Celular"
								value={this.state.celular}
								handlerOnChange={this.handlerOnChange}
								longitud="9"
								check={this.state.validcelular}
							/>
							<Input
								name="correo"
								holder="Correo"
								value={this.state.correo}
								handlerOnChange={this.handlerOnChange}
								check={this.state.validcorreo}
							/>

							
							<p>
								<label className="linkTerminos">
									<input type="checkbox" className="filled-in" id="checkTerminos"  />
									<span>He leído y acepto la <a onClick={()=>{this.handlerOpenModal(2)}}>Política de tratamiento y protección de datos personales</a></span>
								</label>
							</p>
							
							<Button
								value="Empezar"
								clase="btnSucces z-depth-1 full waves-effect"
								name="btnAction"
								handlerOnClick={() => this.props.handlerCot(this.state)} />

							

						</div>
					</div>
				</div>
				<ModalError {...modal} />
				<ModalInformativo contenido={this.state.contenido}
                                  showModal={this.state.showModalInformativo}
                                  handlerCloseModal={this.handlerCloseModal} />

			</section>
		)
	}
}


const mapStateToProps = store => {
	return {
		cliente: store.tercero.tercero,
		lead: store.lead.lead,
		cotizacion: store.cotizacion.cotizacion,
		constantes: store.constantes,
		principalcliente: store.principalcliente.principalcliente,
		listaempresa: store.listaempresa.listaempresa
	}
}

const mapDispatchToProps = dispatch => {
	return {
		consultaTercero: (dni) => dispatch(fetchPersonaEquifax(dni)),
		evaluacionClientes: (dni) => dispatch(fetchEvaluacionClientes(dni)),
		updateCotizacion: (cotizacion) => dispatch(fetchUpdateCotizacion(cotizacion)),
		registrarLog:(dniCliente,modulo,accion) => dispatch(fetchRegistrarLog(dniCliente,modulo,accion)),
		consultaprima: (datosprima) => dispatch(fetchconsultaprima(datosprima)),
		constantesAction : bindActionCreators(ConstantesAction, dispatch) 
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(PageContratante)