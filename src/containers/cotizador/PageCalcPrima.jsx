import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchCONSULTAEMPRESA, fetchUpdateEmpresa } from '../../actions/consultarDatosEmpresa';
import { fetchEvaluacionClientes } from '../../actions/evaluacionClientes';
import { fetchCALCULATEPRICE } from '../../actions/calculateprice';
import Input from '../../components/cotizador/commons/Input'
import Button from '../../components/cotizador/commons/Button'
import Pasos from '../../components/commons/Pasos'
import UtilsJs from '../../utils/Funciones';
import { Element, scroller } from 'react-scroll';
import {ROUTE} from '../../utils/Constantes'
import GTM from '../../utils/GTM'
import { deslizarBtnMas } from '../../resources/js/events.js';
import Check from '../../components/icons/Check'
import Denegado from '../../components/icons/Denegado'
import Cargando from '../../components/icons/Cargando'
import ModalError from '../../components/commons/ModalError'
import Loading from '../../components/commons/Loading'
import { fetchRegistrarLog } from '../../actions/registrarLog'
import { fetchUbigeoDepartamento, fetchUbigeoDistrito, fetchUbigeoProvincia,fetchUbigeo } from '../../actions/ubigeo';
import imgcheck from '../../resources/images/iconos/check.png'
import error from '../../resources/images/iconos/error.png'
import {Input as InputMaterialize} from 'react-materialize' 

class PageCalcPrima extends Component {
	constructor(props){
		super(props)
		this.state = {
			isPaso1: true, // Registrar -> Formulario de contacto
			isPaso2: false, // Comprar1 -> Datos del negocio y Clientes
			isLoading : false,
			nrodni: '',
		rucCompradores : '',
		razonSocialCompradores : '',
		validruc : false,
		validrucComprador: false,
		score: '',
		array:[],
		rucCli		: '',
		Actividad		: '',
		Direccion			: '',
		Distrito			: '',
		Provincia			: '',
		Departamento			: '',
		FacturacionMensual			: '',
		validActividad	: false,
		validDireccion 	: false,
		validaDistrito	: false,
		validaProvincia : false,
		validaDepartamento: false,
		showModalError : false,
		validaRucComprador : false,
		varia: false,
		departamentos: [],
		idedepartamento: '', 
		ideprovincia:'',
		provincias:[],
		validdepartamento	: "ini",
		validdistrito		: "ini",
		validprovincia		: "ini",
		validacionResumen	: "",
		idedistrito :'',
		distritos:[],
		ubigeoCliente:[],
		ideUbigeo : '',
		dscprovincia : '',
		dscdepartamento : ''
		}
	}

	componentDidMount() {
		 
		this.props.handlerLoading(true)
		 
		let data = {"tipo": "DEPARTAMENTO"}
		this.props.consultadepartamento(data).then(resp => {
			 
			 
			if (resp.departamentos.body !== null) {
				this.setState({
					departamentos: resp.departamentos.body,
				})
			} else {
				this.setState({
					departamentos: [],
				})
				 
			}
			this.props.handlerLoading(false)
		})
 
	}
     
	functDatalayerStep2(){
		if(this.state.varia === false){
			window.dataLayer.push({
				event: 'checkoutOption',
				ecommerce: {
					checkout_option: {
						actionField: {'step': 2}
						}
				}
			});
			this.state.varia = true
		}
	}


	handlerLoading = (value) => {
		this.setState({isLoading : value})
	}

	handlerScroll = (element) => {
		//deslizarBtnMas("menu-btn-mas", "dropdown1", 0)
		scroller.scrollTo(element, {
			duration: 800,
			delay: 0,
			smooth: "easeOutQuad"
		});
	}

    handlerSiguiente = () => {
		 
		this.props.updateEmpresa(this.getDatosEmpresa()); 
		 
		// window.dataLayer.push({
		// 	event: 'virtualEvent',
		// 	category: 'Seguro para Valientes',
		// 	action: 'Paso 2: Calificación del Cliente',
		// 	label: this.props.principalcliente == null ? "0" : this.props.principalcliente.IdCliente
		// 	});
		//nueva lista para la api
		var _arrayCompradores = [];

		this.state.array.map(o=>{
			_arrayCompradores.push({ Ruc: o.rucCompradores, 
				Score : o.score});
		});

 
		var scoring = false;
		this.state.array.map(o=>{
		 
			 if(o.estado === "cargando"){
				scoring = true;				
			 }
		});
		if(scoring){
			this.setState({
				showModalError: true,
				contenido: 'Estamos evaluando a tu cliente'
			});
			return;
		}

		if(this.state.validruc){
			//this.state.rucCli
			if(this.state.rucCli.length <= 10){
				this.setState({
					showModalError: true,
					contenido: 'Debes ingresar el RUC de tu negocio.'
				});
			}else if(this.state.FacturacionMensual.length == 0){
				//&& isNaN(this.state.FacturacionMensual)
				this.setState({
					showModalError: true,
					contenido: 'Debes ingresar tu facturación mensual. (ERROR-ND)'
				});
				return;
			}

				if(this.state.array.length == 0){
					this.setState({
						showModalError: true,
						contenido: 'Debes agregar a tus clientes para continuar. (ERROR-ND)'
					});
				}else{
					//nueva lista para la api
					var _arrayCompradores = [];

					 
					this.state.array.map(o=>{
						if(o.estado === "valido"){
							_arrayCompradores.push({ Ruc: o.rucCompradores, 
								Score : o.score});	
						 }					
					});
 
					 
					this.props.handlerLoading(true)
					this.props.calculateprice(_arrayCompradores).then(resp => {		
						if(resp.type === "RECEIVE_CALCULATEPRICE"){

							
							var _strcompradores = '';

							this.state.array.map(o=>{
								_strcompradores += o.rucCompradores + ' | '+ o.estado  +' - ';
							});
							
							// var calculo = JSON.parse(resp.calculo.body);
							// TAG MANAGER (DEBERÍA IR EN LA VALIDACION CUANDO TODO EL PAGE ESTE OK, CON CAMPOS LLENOS Y VALIDADOS)
							
							var _strcompradores = '';

							this.state.array.map(o=>{
								//RECORRER Y ANIDAR UN -
								_strcompradores += ` - ${o.rucCompradores}'|'${o.estado}`;
							});

							 

							window.dataLayer.push({
								event: 'virtualEvent',
								category: 'Seguro para Valientes',
								action: 'Paso 2: Datos Clientes',
								//label: `${ this.props.principalcliente == null ? "0" : this.props.principalcliente.IdCliente}  ${_strcompradores}`
								label: `${ this.props.cliente == null ? "0" : this.props.cliente.numerodoc}  ${_strcompradores}`
								
							});
						 

							this.props.handlerLoading(false)
						 
								// GTM.pasosCotizador('Paso 3: Preguntas', this.props.cotizacion.precioprima)
							this.props.handlerPaso(2);
							
							// var calculo = JSON.parse(resp.calculo.body);
							// TAG MANAGER (DEBERÍA IR EN LA VALIDACION CUANDO TODO EL PAGE ESTE OK, CON CAMPOS LLENOS Y VALIDADOS)
							

						}
					});
	
				}
					
			}else{
			this.setState({
				showModalError: true,
				contenido: <p>El RUC ingresado debe ser de tu negocio. (ERROR-ND)</p>,
				validruc: false
			})
		}
		
	
	}

	registroLog=(dniCliente,modulo,accion)=>{
		this.props.registrarLog(dniCliente,modulo,accion).then(respLog=> {           
		})
	  }
  

	handlerCloseModalError = () => {
		this.setState({
			showModalError :  false
		})
	}

	handlerNext = (param) => {
	 
		this.props.history.push({pathname : ROUTE.URL_COMPRAR});
		
	}



	handlerCS = (rucCompradores) =>{
		var numruc = rucCompradores;
		var dni = this.props.cliente.numerodoc
		 
		var ruccliente = this.state.rucCli;
		if(ruccliente==numruc){
			//mensaje de error
			this.setState({
				showModalError: true,
				contenido: <p>Ingrese un RUC diferente al de tu negocio. (ERROR-ND)</p>,
				rucCompradores		: rucCompradores,
				validrucComprador:false
			})
			this.setState({rucCompradores:''});
			return;
		} 
		var array3=this.state.array;
		if(array3 != null ){
			for (var i = 0; i < array3.length ; i++){
				var obj = array3[i];
				if(obj.rucCompradores === rucCompradores){
					this.setState({
						showModalError: true,
						contenido: <p>El RUC ya está registrado. (ERROR-ND)</p>,
						rucCompradores		: rucCompradores,
						validrucComprador:false
					})
					this.setState({rucCompradores:''});
					return;
				}
			}
		}

		if(rucCompradores == '' || rucCompradores === ''){
			this.setState({
				showModalError: true,
				contenido: <p>Por favor ingresar el RUC de tu cliente. (ERROR-ND)</p>,
				rucCompradores		: rucCompradores,
				validrucComprador:false
			})
		return;
		}

			//validando ruc cantidad de digitos
			if (rucCompradores != null) {
				 
				//formato de consumir servicios
				this.props.consultaEmpresa(dni, rucCompradores, "2").then(resp => {
					
					if(resp === undefined || resp == null ){
						this.setState({
							showModalError: true,
							contenido: <p>El RUC ingresado no existe. (ERROR-ND)</p>,
							rucCompradores		: rucCompradores,
							validrucComprador:false
						})
					}else if(resp.type === "RECEIVE_LISTAEMPRESA"){
					 
						var obj = null;
						// var cliente = JSON.parse(resp.cliente.body);
						if (resp.listaempresa.RazonSocial !== null  ) {
							
								this.setState({
									rucCompradores		: ''									
								}, () => {
									this.props.handlerLoading(false)									
									obj = {rucCompradores		: rucCompradores,
										razonSocialCompradores		: resp.listaempresa.RazonSocial,
										estado: 'cargando',
										icono : <Cargando />,
										};
							
									var array3=this.state.array;
									array3.push(obj);
									this.setState({array:array3});

									this.setState({rucCompradores:''});
		 
								})

								//black list
								 
								this.props.evaluacionCliente(dni, "6", numruc, 2).then(resp => {
								 

									if(resp.type === "RECEIVE_EVALUACIONCLIENTES"){
										 
										var _array=this.state.array;

										if (resp.cliente.puntaje != null &&  resp.cliente.puntaje != 0) {



											_array.map(o=>{
												if(o.rucCompradores == rucCompradores){
													o.icono = <Check />;
													o.estado = 'valido';
													o.score = resp.cliente.puntaje;
												}

											
											})
											this.registroLog(dni,"COTIZACION","[EVALUACIÓN DE COMPRADOR] - RUC: " + numruc + " - Puntaje: " + resp.cliente.puntaje)
					

											this.props.listaempresa.map(j=>{
												if(rucCompradores == j.RUC){
													j.estado = 'Valido';
												}
											});

											this.setState({array:_array}); 
											// this.props.history.push({pathname : ROUTE.URL_COTIZADOR});
											
										} else {
											 
										//colocar icono x
											_array.map(o=>{
												if(o.rucCompradores == rucCompradores){
													o.icono = <Denegado />;
													o.estado = 'denegado';
													o.score = "0";
												}

											});

											
											this.props.listaempresa.map(j=>{
												if(rucCompradores == j.RUC){
													j.estado = 'Denegado';
												}
											});
											
											this.setState({array:_array});
											 
										}
									}else if(resp.type === "INVALIDATE_CONSULTACLIENTES"){
										//mensaje de error no puede pasar
										this.setState({
											showModalError: true,
											contenido: <p>Hemos tenido un problema con nuestros sistemas, intentelo más tarde.</p>,
											
										})
									}			
									
								})




							
						} else {
							/** mensaje no contamos con sus datos */
							this.setState({
								showModalError: true,
								contenido: <p>No te tenemos registrado. (ERROR-ND)</p>,
								numruc		: numruc
							})
						}
					}else if(resp.type === "INVALIDATE_LISTAEMPRESA"){
						this.setState({
							showModalError: true,
							contenido: <p>Hemos tenido un problema con nuestros sistemas, intentelo más tarde. (ERROR-EQ)</p>,
							numruc		: numruc
							
						})
					}			
					
				})
			}else{
				this.setState({
					showModalError: true,
					contenido: <p>Por favor ingresar el RUC de tu cliente. (ERROR-ND)</p>,
					rucCompradores		: rucCompradores,
					validrucComprador:false
				})
			}
	

	}


	
	handlerOnClick = (rucCompradores) => {
            if(rucCompradores!=""){
			this.props.consultaEmpresa(rucCompradores).then(resp => {		
			if(resp.type === "RECEIVE_CONSULTAEMPRESA"){
				  
				if (resp.empresa.RazonSocial !== null  ) {
						this.setState({
							rucCompradores		: rucCompradores,
							razonSocialCompradores		: resp.empresa.RazonSocial,
						}, () => {
							
							this.props.handlerLoading(false)
						})
					
				} else {
					/** mensaje no contamos con sus datos */
					this.setState({
						showModalError: true,
						contenido: <p>El RUC ingresado no existe. (ERROR-ND)</p>,
						rucCompradores		: rucCompradores
					})
				}
			}else if(resp.type === "INVALIDATE_CONSULTACLIENTES"){
				this.setState({
					showModalError: true,
					contenido: <p>Hemos tenido un problema con nuestros sistemas, intentelo más tarde. (ERROR-EQ)</p>,
					rucCompradores		: rucCompradores
					
				})
			}			
			
		})
	}
	}


	getDatosEmpresa= ()=>{
		var selectDistrito=document.getElementById("selectDistrito"); 
		var xdistrito = selectDistrito.options[selectDistrito.selectedIndex].text;
		var xvaluedistrito = selectDistrito.options[selectDistrito.selectedIndex].value;
		
		var selectProvincia=document.getElementById("selectProvincia"); 
		var xprovincia = selectProvincia.options[selectProvincia.selectedIndex].text;
		var xvalueprovincia = selectProvincia.options[selectProvincia.selectedIndex].value;
		
		var selectDepartamento=document.getElementById("selectDepartamento"); 
		var xdepartamento = selectDepartamento.options[selectDepartamento.selectedIndex].text;
		var xvaluedepartamento = selectDepartamento.options[selectDepartamento.selectedIndex].value;
		 
		return {
			Actividad : this.props.empresa.Actividad, 
			Departamento : xdepartamento, 
			Direccion : this.state.Direccion, 
			Distrito : xdistrito, 
			Provincia : xprovincia, 
			RUC : this.props.empresa.RUC, 
			RazonSocial : this.props.empresa.RazonSocial, 
			Ubigeo : this.state.ideUbigeo, 
			estado : 'Listo',
			codDepartamento : xvaluedepartamento,
			codProvincia :	xvalueprovincia,
			codDistrito :	xvaluedistrito
		  }

		 

	}

	handlerDeleteItem = (index)=>{
		 
		var _array = [];
		this.state.array.map((o,i)=>{
			if(i!=index){
				_array.push(o);
			}
		});
		var _array2 = [];
		this.props.listaempresa.map((o,i)=>{
			if(i!=index){
				_array2.push(o);
			}
		});
		//this.props.listaempresa = _array2;
		this.setState({array:_array});
	 
	}
	handlerLoading = (value) => {
		this.setState({isLoading : value})
	}
	handlerOnChange = (e) => {
		//almaceno valr del RUC en el evento
		var numruc = e.target.value;
		var dni = this.props.cliente.numerodoc;
		//validando ruc cantidad de digitos
		

		if (e.target.name === "rucCli" && UtilsJs.validaruc(e.target.value, "ruc")) {
			  
			if(e.target.value.indexOf(this.props.cliente.numerodoc)!=-1){
				
				this.setState({ 
					validruc: true
				})

				this.props.handlerLoading(true)
				this.props.consultaEmpresa(dni, e.target.value, 1).then(resp => {


			 
				 
				if(resp == null || resp === null ){
					this.setState({
						rucCli		: numruc,
						Actividad		: '',
						Direccion			: '',
						Distrito			: '',
						idDistrito			: '',
						Provincia			: '',
						idProvincia			: '',
						Departamento		: '',
						idDepartamento		: '',
						validruc	: false,
						validActividad	: false,
						validDireccion 	: false,
						validaDistrito	: false,
						validaProvincia : false,
						validaDepartamento: false,
						validaFacturacion :false

					}, () => {
						this.props.handlerLoading(false) 
						this.setState({
							showModalError: true,
							contenido: <p>El RUC de tu negocio no existe. </p>,
							numruc		: numruc
						})
					})
					
				}else if(resp.type === "RECEIVE_CONSULTAEMPRESA"){					 
					// var cliente = JSON.parse(resp.cliente.body);
					if (resp.empresa.RazonSocial !== null  ) {
					 
							this.setState({
								rucCli		: numruc,
								Actividad		: resp.empresa.Actividad,
								Direccion			: resp.empresa.Direccion,
								Distrito			: resp.empresa.Distrito,
								Provincia			: resp.empresa.Provincia,
								Departamento			: resp.empresa.Departamento,
								idUbigeo				: resp.empresa.Ubigeo,
								validruc	: true,
								validActividad	: true,
								validDireccion 	: true,
								validaDistrito	: true,
								validaProvincia : true,
								validaDepartamento: true,
								validaFacturacion :false

							}, () => {
								this.props.handlerLoading(false)
								this.setState({ validaFacturacion: "error" })
							})


							this.handlerObtenerUbigeo(this.state.idUbigeo);
						
					} else {
						/** mensaje no contamos con sus datos */
						this.setState({
							showModalError: true,
							contenido: <p>No te tenemos registrado. (ERROR-ND)</p>,
							numruc		: numruc
						})
					}
				}else if(resp.type === "INVALIDATE_CONSULTAEMPRESA"){
					this.setState({
						showModalError: true,
						contenido: <p>Hemos tenido un problema con nuestros sistemas, intentelo más tarde. (ERROR-EQ)</p>,
						numruc		: numruc
						
					})
				}			
				
			})
				
			}else{
				this.setState({
					showModalError: true,
					contenido: <p>El RUC ingresado debe ser de tu negocio. (ERROR-ND)</p>,
					validruc: "error"
				})
				return;
			}
			
		} else if (e.target.name === "nrodni" && !UtilsJs.validadni(e.target.value, "dni")) {
			this.setState({
				nrodni: '',
				apepaterno: '',
				apematerno: '',
				Nombres: '',
				nombre: '',
				nombreapellidoMasc: '',
				idpgenero: '',
				fecnacimiento: '',
				celular: '',
				correo: '',
				nombreapellido: '',
				validdni: "error",
				validnombre: false,
				validcelular: false,
				validcorreo: false,
				validgenero: false 
			})
		}

		
		if (e.target.name === "rucCli") {
			if (UtilsJs.validasolonumero(e.target.value, "rucCli")) {
				this.setState({ rucCli: e.target.value })
				//this.setState({ validruc: true })
			} else {
				this.setState({ rucCli: e.target.value })
				this.setState({ validruc: "error" })
			}
		}

		if (e.target.name === "ruc-comprador") {
			if (UtilsJs.validasolonumero(e.target.value, "ruc-comprador")) {
				this.setState({ rucCompradores: e.target.value })
				this.setState({ validrucComprador: true })
			} else {
				this.setState({ validadistrito: "error" })
			}
		}
 
		if (e.target.name === "facturacion-promedio") {
			 
			if(this.state.FacturacionMensual.length == 0){
				this.setState({ validaFacturacion: "error" })
			}else{
				this.setState({ validaFacturacion: true })
			}

			if (UtilsJs.validasolonumero(e.target.value, "facturacion-promedio")) {
				 
				this.setState({ FacturacionMensual: e.target.value })
				//this.setState({ validaFacturacion: true })
			} else {
			  
				//this.setState({ validaFacturacion: "error" })
			}
		}
 

		if (e.target.name === "direccion-fiscal") {
				this.setState({ 'Direccion': e.target.value })
				this.setState({ validDireccion: true })
		}
		if (e.target.name === "distrito") {
			if (UtilsJs.validasololetras(e.target.value, "distrito")) {
				this.setState({ 'distrito': e.target.value })
				this.setState({ validaDistrito: true })
			} else {
				this.setState({ validaDistrito: "error" })
			}
		}
		if (e.target.name === "provincia") {
			if (UtilsJs.validasololetras(e.target.value, "provincia")) {
				this.setState({ 'provincia': e.target.value })
				this.setState({ validaProvincia: true })
			} else {
				this.setState({ validaProvincia: "error" })
			}
		}
		if (e.target.name === "departamento") {
			if (UtilsJs.validasololetras(e.target.value, "departamento")) {
				this.setState({ 'departamento': e.target.value })
				this.setState({ validaDepartamento: true })
			} else {
				this.setState({ validaDepartamento: "error" })
			}
		}

	}
	handlerObtenerProvincias = (e) => {
	 
		this.props.handlerLoading(true)
		if(e.target.value !== ""){
			this.setState({ idedepartamento: e.target.value, dscdepartamento : this.busquedadscubigeo(e.target.value, this.state.departamentos, "DEPARTAMENTO")  , validacionResumen: "" }, () => {
				this.props.consultaprovincia(this.state.idedepartamento).then(resp => {
					this.setState({
						provincias: resp.provincias.body,
						validdepartamento: true
					}, () => {						 
						this.props.handlerLoading(false)
					})
				})
			})
		}else{
			this.props.handlerLoading(false)
			this.setState({validdepartamento: "error"})
		}
	}

	busquedadscubigeo = ( codigo, ubigeo, caso ) => {
		for (let i = 0; i < ubigeo.length; i++) {
			if(codigo === ubigeo[i].idedepartamento && caso === "DEPARTAMENTO"){
				return ubigeo[i].dscdepartamento
			}else if(codigo === ubigeo[i].ideprovincia && caso === "PROVINCIA"){
				return ubigeo[i].dscprovincia
			}else if(codigo === ubigeo[i].idedistrito && caso === "DISTRITO"){
				return ubigeo[i].dscdistrito
			}
		}
	}

	handlerObtenerDistritos = (e) => {
		this.props.handlerLoading(true)
		if(e.target.value !== ""){
			this.setState({ ideprovincia: e.target.value, dscprovincia:  this.busquedadscubigeo(e.target.value, this.state.provincias, "PROVINCIA") , validacionResumen: ""}, () => {
				this.props.consultardistrito(this.state.idedepartamento,this.state.ideprovincia).then(resp => {
					this.setState({
						distritos: resp.distritos.body,
						validprovincia: true  
					}, () => {
						 
						this.props.handlerLoading(false)
					})
				})
			})
		}else {
			this.props.handlerLoading(false)
			this.setState({validprovincia: "error"})
		}
	}

	handlerObtenerUbigeo = (idUbigeo) => {
		this.props.handlerLoading(true)
		if(idUbigeo !== ""){
			 this.props.consultarUbigeo(idUbigeo).then(resp => {
				  
					var descripcionDepar = resp.distritos.body[0].DSCDEPARTAMENTO;
					var desprovincia = resp.distritos.body[0].DSCPROVINCIA;
					var desdistrito = resp.distritos.body[0].DSCDISTRITO;

					var codigoDepar = resp.distritos.body[0].IDEDEPARTAMENTO;
					var codigoProvincia = resp.distritos.body[0].IDEPROVINCIA;
					var codiogDistrito = resp.distritos.body[0].IDEDISTRITO;

					
					this.props.consultaprovincia(codigoDepar).then(resp => {
						this.setState({
							provincias: resp.provincias.body,
							validdepartamento: true
						}, () => {		
							
							this.props.consultardistrito(codigoDepar,codigoProvincia).then(resp => {
								this.setState({
									distritos: resp.distritos.body,
									validprovincia: true,
									idedistrito : codiogDistrito,
									idedepartamento : codigoDepar,
									ideprovincia : 	codigoProvincia,
									ideUbigeo : idUbigeo
								}, () => {
									
									var det = 0;
									var selectDepartamento=document.getElementById("selectDepartamento"); 
									var selectProvincia=document.getElementById("selectProvincia"); 
									var selectDistrito=document.getElementById("selectDistrito");

									for(var i=0;i<selectDepartamento.length;i++){											 
										if(selectDepartamento.options[i].text == descripcionDepar ) { 							
											selectDepartamento.selectedIndex=i;
											det = i;

											var elems = document.querySelectorAll('select')[0];
											
											console.log(elems);


										}
									}

									for(var i=0;i<selectProvincia.length;i++){	 
										if(selectProvincia.options[i].text == desprovincia ) { 							
											selectProvincia.selectedIndex=i; 
										}
									}

									for(var i=0;i<selectDistrito.length;i++){	 
										if(selectDistrito.options[i].text == desdistrito ) { 							
											selectDistrito.selectedIndex=i; 
										}
									}

									
									// console.log(instance.getSelectedValues());

									this.props.handlerLoading(false) 
								})
							})				 
							 
						})
					})

					
					 
				})
		 
		}else {
			this.props.handlerLoading(false)
			this.setState({validprovincia: "error"})
		}
	}

	handlerOnChangeDistrito = (e) => {
		
		var selectDistrito=document.getElementById("selectDistrito"); 
 		for(var i=0;i<selectDistrito.length;i++){	
			 
			if(selectDistrito.options[i].value == e.target.value ) { 							
				selectDistrito.selectedIndex=i; 			 
				this.setState({distrito: selectDistrito.options[i].text})
				this.props.updateEmpresa(this.getDatosEmpresa());  
				return;
			}
		}
	}

	render() {
		
		let isLoading = false
		let fecha = new Date();
		let month = fecha.getMonth() + 1;
		let day = fecha.getDate();
		let yearMax = fecha.getFullYear() - 18;		
		let yearMin = fecha.getFullYear() - 90;	
		day = day < 10 ? '0'+day:day;	
		month = month < 10 ? '0'+month : month;

		let { validdepartamento } = this.state

		const optionsFecIniVigencia = {
			min: day + '/' + month + '/' + yearMin ,
			max: day + '/' + month + '/' + yearMax ,
			today: '',
			clear: '',
			close: 'OK',
			format: 'dd/mm/yyyy',
			closeOnSelect: false,
			monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
			monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
			weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
			weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
			selectYears: true,
			selectMonths: true,
	  	};

		let modal = {
			contenido : this.state.contenido,  
			title:<div>LO SENTIMOS</div>, 
			handlerCloseModal:this.handlerCloseModalError ,
			showModal:this.state.showModalError
		}

		let { validdni, validce, validnombre, validapepaterno, validapematerno, validcelular, validcorreo } = this.state;

		let title = <p className="bxTitle mb-full" >Ingresa tus datos para adquirir este seguro</p>;

		this.functDatalayerStep2();
		return (
			
			<div>
				<Pasos paso={1} />
				<section className="container d-flex flex-column">			
				
				<div className="d-flex justify-content-between mb-flex-column">
					<div className="boxWhite">
						
						<div className="bxPagoPrima d-flex justify-content-center flex-column align-items-center">
							
							<h2 className="title">Completa los siguientes datos sobre tu negocio</h2>
							<div className="miNegocio d-flex flex-wrap justify-content-between mb-flex-column">
								<b className="labelForm cienp d-flex justify-content-start">Mi negocio</b>
								<Input
									clase=" mb-full"
									name="rucCli"
									holder="RUC"
									handlerOnChange={this.handlerOnChange}
									longitud="11"
									check={this.state.validruc} />
								<Input
									clase=" mb-full"
									name="actividad-economica"
									value={this.state.Actividad}
									handlerOnChange={this.handlerOnChange}
									holder="Actividad económica"
									check={this.state.validActividad} />
								
								<Input
									clase=" mb-full"
									name="facturacion-promedio"
									longitud="10"
									value={this.state.FacturacionMensual}
									handlerOnChange={this.handlerOnChange}
									holder="Facturación promedio al mes"
									check={this.state.validaFacturacion} />
							 
							 
								
								 

								<InputMaterialize className="mb-full" s={12} 
								type='select' label="Departamento" 
								defaultValue={this.state.departamentos}
								onChange={this.handlerObtenerProvincias}
								id="selectDepartamento"
								>
								<option value="" >Departamento</option>
										{this.state.departamentos ? (
											this.state.departamentos.map((item, index) => (
												<option key={index} value={item.CODIGODEPARTAMENTO}> {item.DESCRIPCION} </option>
											))
										) : null}
									</InputMaterialize>

								{/* <select type='select' className="mb-full" id="selectDepartamento"  defaultValue={this.state.idedepartamento} onChange={this.handlerObtenerProvincias} >
										<option value="" >Departamento</option>
										{this.state.departamentos ? (
											this.state.departamentos.map((item, index) => (
												<option key={index} value={item.CODIGODEPARTAMENTO}> {item.DESCRIPCION} </option>
											))
										) : null}
									</select> */}
									 
									
									<select className="mb-full" id="selectProvincia"  
									defaultValue={this.state.ideprovincia} 
									onChange={this.handlerObtenerDistritos} >
									<option value="" >Provincia</option>
										{this.state.provincias ? (
											this.state.provincias.map((item, index) => (
												<option key={index} value={item.CODIGOPROVINCIA}>{item.DESCRIPCIONPROVINCIA}</option>
											))
										) : null}
									</select>
									 


								{/**<InputMaterialize className="mb-full" s={12} 
								type='select' label="Provincia" 
								defaultValue={this.state.ideprovincia}
								onChange={this.handlerObtenerDistritos}
								>
								<option value="" >Provincia</option>
										{this.state.provincias ? (
											this.state.provincias.map((item, index) => (
												<option key={index} value={item.CODIGOPROVINCIA}>{item.DESCRIPCIONPROVINCIA}</option>
											))
										) : null}
								</InputMaterialize>**/}

									<select className="mb-full" id="selectDistrito"  
									defaultValue={this.state.idedistrito} 
									onChange={this.handlerOnChangeDistrito} >
									<option value="" >Distrito</option>
									{this.state.distritos ? (
											this.state.distritos.map((item, index) => (
												<option key={index} value={item.IDEDISTRITO}>{item.DSCDISTRITO}</option>
											))
										) : null}
									</select>

								{/**<InputMaterialize className="mb-full" s={12} 
								type='select' label="Distrito" 
								defaultValue={this.state.idedistrito}
								onChange={this.handlerOnChangeDistrito}
								>
								<option value="" >Distrito</option>
								{this.state.distritos ? (
											this.state.distritos.map((item, index) => (
												<option key={index} value={item.IDEDISTRITO}>{item.DSCDISTRITO}</option>
											))
										) : null}
								</InputMaterialize>**/}

								<Input
									clase="cienp"
									name="direccion-fiscal"
									value={this.state.Direccion}
									handlerOnChange={this.handlerOnChange}
									holder="Dirección fiscal"
									check={this.state.validDireccion}
									/>	 
							 
							</div>
							<div className="misClientes d-flex flex-wrap justify-content-start">
								<b className="labelForm cienp d-flex justify-content-start">Mis Clientes</b>
								<Input
									clase=" mb-noventap btnInput"
									name="ruc-comprador"
									holder="Ingresa su RUC"
									value={this.state.rucCompradores}
									handlerOnChange={this.handlerOnChange}
									longitud="11"
									check={this.validrucComprador} 
									/>
								<Button
									clase="btn btnAction btnBrowser"
									handlerOnClick={() => this.handlerCS(this.state.rucCompradores)}
									value="+" />

								<div  className="boxResultRUC cienp d-flex flex-wrap justify-content-between">

									{
										(this.state.array || [] || null).map((obj, index) => {
											return (

												<div id={"i" + index} className="boxResultRUC-item mb-full d-flex justify-content-between align-items-center">
													<div className="boxResultRUC-item-cal d-flex flex-column align-items-center">
														{obj.icono}
													</div>
													<div className="boxResultRUC-item-detail cienp d-flex mb-full flex-column">
														<p><b>RUC: </b>{obj.rucCompradores}</p>
														<p><b>RS: </b>{obj.razonSocialCompradores}</p>
													</div>
													<Button
													clase="btnDrop"
													handlerOnClick={()=>this.handlerDeleteItem(index)}
													value="-" />
												</div>

											)
										})
									}

									

															
								</div>
							</div>



							<Button
								clase="btn btnAction btnAction"
								value="CONTINUAR"
								handlerOnClick={this.handlerSiguiente} />

							
						</div>

					</div>

					<div className="cuarentaycincop ml20 mb-full">
						<div className="boxInfor boxWhite mb-full">
							<span className="titleHelp cienp d-flex justify-content-center">¿NECESITAS AYUDA?</span>
							<p><b>Actividad económica: </b>Deberás considerar la misma que registraste en SUNAT como actividad principal.</p>
							{/* <p><b>Facturación más alta:</b> El monto de tu factura más alta del mes.</p> */}
							<p><b>Facturación promedio:</b> El monto promedio de tus facturas al mes.</p>
							<p><b>Dirección fiscal: </b>Dirección de negocio registrada en la SUNAT.</p>
							<p><b>Tope de facturación: </b>Rango de montos que RIMAC te puede cubrir.</p>
							<p><b>Precio: </b>Monto único mensual a pagar.</p>
						</div>
						<div className="boxAlert boxWhite mt20 mb-full">
							<span className="titleImportant cienp d-flex justify-content-start">IMPORTANTE</span>
							<p>Cubrimos máximo 06 comprobantes de pago electrónicos retrasados al año. (1 por mes)</p>
							<p>Los comprobantes de pago electrónicos deben ser emitidos durante la vigencia del seguro.</p>
						</div>
					</div>

				</div>
				<ModalError {...modal} />
				{isLoading ? <Loading /> : null }
			</section>
			</div>


			
		)
	}

}

const mapStateToProps = store => {
	return {
		calculo: store.calculo.calculo,
		cliente: store.tercero.tercero,
		empresa: store.empresa.empresa,//store.tercero.tercero,
		principalcliente: store.principalcliente.principalcliente,
		listaempresa: store.listaempresa.listaempresa,
		departamentos : store.departamentos.departamentos,
		provincias : store.provincias.provincias,
		distritos	: store.distritos.distritos,
		ubigeo : store.ubigeoEmpresa
	}
}

const mapDispatchToProps = dispatch => {
	return {
		consultaEmpresa: (dni, ruc, tipo) => dispatch(fetchCONSULTAEMPRESA(dni, ruc, tipo)),
		evaluacionCliente: (xNroCliente,xtipoDoc, xNroDoc, xpaso) => dispatch(fetchEvaluacionClientes(xNroCliente,xtipoDoc, xNroDoc, xpaso)),
		calculateprice: (array) => dispatch(fetchCALCULATEPRICE(array)),
		registrarLog:(dniCliente,modulo,accion) => dispatch(fetchRegistrarLog(dniCliente,modulo,accion)),
		consultadepartamento: (data) => dispatch(fetchUbigeoDepartamento(data)),
		consultaprovincia: (data) => dispatch(fetchUbigeoProvincia(data)),
		consultardistrito: (idedepartamento,ideprovincia) => dispatch(fetchUbigeoDistrito(idedepartamento,ideprovincia)),
		consultarUbigeo: (idUbigeo) => dispatch(fetchUbigeo(idUbigeo)),
		updateEmpresa: (empresa) => dispatch(fetchUpdateEmpresa(empresa)),
		
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(PageCalcPrima)
