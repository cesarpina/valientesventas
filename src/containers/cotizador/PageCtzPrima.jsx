import React, {Component } from 'react'
import { withRouter } from "react-router-dom"
import Input from '../../components/cotizador/commons/Input'
import Button from '../../components/cotizador/commons/Button'
import Pasos from '../../components/commons/Pasos'
import {ROUTE} from '../../utils/Constantes'
import { connect } from 'react-redux';
import Select from '../../components/compra/select';
import {Input as InputMaterialize} from 'react-materialize' 
 class PageCtzPrima extends Component {

	constructor(props){
		super(props);
		this.state = {
			cubrimos: this.props.calculo.Porcentaje,
			precio: this.props.calculo.Precio,
			monto: this.props.calculo.Monto,
			cobertura: this.props.calculo.Cobertura,
			varia: false
		}

		 
	}
	functDatalayerStep3(){
		if(this.state.varia === false){
			 
			window.dataLayer.push({
				event: 'checkoutOption',
				ecommerce: {
					checkout_option: {
						actionField: {'step': 3}
						}
				}
			});
			this.state.varia = true
		}
	}
 
    handlerNext = () => {
		 
		this.props.history.push({pathname : ROUTE.URL_COMPRAR}); 
	}

	onchangemonto = (e) =>{
		 
		this.props.calculo.Cobertura = e.target.value;
		this.setState({
			cobertura: e.target.value
		});
		 
	}

    render(){
		 
		this.functDatalayerStep3();
        return(
            <div className="d-flex flex-column">
			<Pasos paso={2} />
				<section className="container d-flex flex-column">			
				
				<div className="d-flex justify-content-between mb-flex-column">
					<div className="boxWhite">
						
						<div className="bxPagoPrima d-flex justify-content-center flex-column align-items-center">
							
							<h2 className="title">Selecciona la cobertura máxima de tu preferencia</h2>
							
							<div className="bxInput d-flex flex-column justify-content-start align-items-center cienp">
									<b className="labelForm d-flex justify-content-start cienp">Mi facturación</b>
									
								 
								<InputMaterialize className="mb-full" s={12} 
								type='select' label="Monto máximo por factura" 
								defaultValue={this.state.cobertura}
								onChange={this.onchangemonto}
								>
								<option value="" >Seleccione</option>								 
									{this.state.monto.map(o=>(<option value={o}>S/ {o}</option>))}
								</InputMaterialize>
									
							</div>

							<div class="cienp">
								<div className="cienp d-flex justify-content-between">
									<div className="boxPrice cuarentap mb-full">
										<span className="mb-full cienp d-flex justify-content-center">CUBRIMOS</span>
										<div className="boxPriceCost boxPorcentaje">
											<b className="mb-full cienp d-flex justify-content-center">{this.state.cubrimos} %</b>
											<p className="mb-full cienp d-flex justify-content-center">de tu factura</p>
										</div>
									</div>
									<div className="boxPrice cuarentap mb-full">
										<span className="mb-full cienp d-flex justify-content-center">PRECIO</span>
										<div className="boxPriceCost">
											<b className="mb-full cienp d-flex justify-content-center">S/ {this.state.precio}</b>
											<p className="mb-full cienp d-flex justify-content-center">mensuales</p>
										</div>
									</div>
								</div>
							</div>


							<Button
								clase="btn btnAction btnAction mt20"
                                value="CONTINUAR CON LA COMPRA"
                                handlerOnClick={() => this.props.handlerCot()}
							/>
							{/* {<a className="link d-flex align-items-center" onClick={()=>handlerPaso(7)}>Checkout</a>} */}

						</div>

					</div>

					<div className="cuarentaycincop ml20 mb-full">
						<div className="boxInfor boxWhite mb-full">
							<span className="titleHelp cienp d-flex justify-content-center">¿NECESITAS AYUDA?</span>
							
							<p><b>Cobertura máxima: </b>Monto máximo de tu facturación que podemos recibir.</p>
							<p><b>Precio: </b>Monto único mensual a pagar.</p>
						</div>
						<div className="boxAlert boxWhite mb-full mt20">
							<span className="titleImportant cienp d-flex justify-content-start">IMPORTANTE</span>
							<p>Cubrimos máximo 06 comprobantes de pago electrónicos retrasados al año.</p>
							<p>Los comprobantes de pago electrónicos deben ser emitidos durante la vigencia del seguro.</p>
						</div>
					</div>

				</div>

				

			
			</section>
			
		</div>
        )
    }
}



const mapStateToProps = store => {
	return {
		calculo: store.calculo.calculo,
		cotizacion: store.cotizacion.cotizacion,
		empresa: store.empresa.empresa,//store.tercero.tercero,
		cliente: store.tercero.tercero,
		principalcliente: store.principalcliente.principalcliente,
		listaempresa: store.listaempresa.listaempresa,
		ubigeo : store.ubigeoEmpresa,
	}
}

const mapDispatchToProps = dispatch => {
	return {
	
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(PageCtzPrima)