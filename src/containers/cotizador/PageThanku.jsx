import React, {Component } from 'react'
import { withRouter } from "react-router-dom"
import { connect } from 'react-redux'; 
import ImgThank from '../../resources/images/thankyou/thanku.png'  
import {ROUTE} from '../../utils/Constantes'

class PageThanku extends Component {


	functDatalayerThanks(){ 
	  }

    handlerNext = () => { 
		this.props.history.push({pathname : ROUTE.URL_REGISTRAR});
		
	}
    render(){
		this.functDatalayerThanks();
        return(
            <section className="d-flex flex-column mb-full">
				<div className="container">
					<div className="boxWhite d-flex flex-column align-items-center boxThanku mt20 mb20">
						<img src={ImgThank} alt="" />
						<h2 className="titleThanku">¡Felicidades {this.props.cliente == null ? "Sin datos" : this.props.cliente.nomcompleto}!</h2>
						<span>Tu pago se realizó con éxito</span>
						<p>Enviaremos un correo de confirmación con toda la información al correo</p>
						<p>{this.props.cotizacion == null ? "Sin datos" : this.props.cotizacion.correo} en los próximos minutos.</p>

						 
					</div>
				</div>
				
			</section>
        )
    }
}



const mapStateToProps = store => {
	return {
		cotizacion: store.cotizacion.cotizacion,            
        cliente: store.tercero.tercero,
	}
}

const mapDispatchToProps = dispatch => {
	return {
	}
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PageThanku))
