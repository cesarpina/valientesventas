import React, { Component } from 'react';
import Button from '../../components/cotizador/commons/Button';
import Title from '../../components/cotizador/commons/Title';
import PreguntaItem from '../../components/cotizador/PreguntaItem';
import UtilsJS from '../../utils/Funciones';
import BtnVolver from '../../components/commons/BtnVolver'
import Pasos from '../../components/commons/Pasos'
import { connect } from 'react-redux';
import { fetchUpdateCotizacion } from '../../actions/cotizacion';
import { fetchRegistrarLead } from '../../actions/registrarLead';
import GTM from '../../utils/GTM';

class PageCuestionario extends Component {
	state = {
		respuesta1: {
			orden: 1,
			name: "respuesta1",
			positivo: false,
			negativo: false
		},
		respuesta2: {
			orden: 2,
			name: "respuesta2",
			positivo: false,
			negativo: false
		},
		respuesta3: {
			orden: 3,
			name: "respuesta3",
			positivo: false,
			negativo: false
		},
		respuesta4: {
			orden: 4,
			name: "respuesta4",
			positivo: false,
			negativo: false
		},

		textoValidacion: ""
	}

	componentDidMount(){
		GTM.navegarCotizador('Preguntas')
	}

	handlerUpdateLead = (subcripcion) => {
		let indicador = subcripcion? "SUSCRIPCION APROBADA":"SUSCRIPCION RECHAZADA"

		
		let params = {
			idelead:  this.props.cotizacion.idelead,
			idptipodocumento: this.props.cotizacion.tipodocdni? "DNI": "CE",
			numerodocumento: this.props.cotizacion.tipodocdni? this.props.cotizacion.nrodni : this.props.cotizacion.nroce,
			nomcompleto: this.props.cotizacion.nomcompleto,
			fecnacimiento: this.props.cotizacion.fecnacimiento,
			idpgenero: this.props.cotizacion.idpgenero,
			numerotelf: this.props.cotizacion.celular,
			email: this.props.cotizacion.correo,
			precio: this.props.cotizacion.precioprima,
			useragent: "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36",
			nombres: this.props.cotizacion.nombre,
			apepaterno: this.props.cotizacion.apepaterno,
			apematerno: this.props.cotizacion.apematerno,
			ideProd: "4507",
			fgVentaFinalizada: "N",
			observacion : indicador
		}

		this.props.resgistrarLead(params).then(resp => {					
			this.props.handlerLoading(false)		
		})
	}

	handlerSiguiente = () => {
		this.props.handlerLoading(true)
		let valid = UtilsJS.validarCuestionario(this.state)
		if (valid && !(valid === 1 || valid === 2 || valid === 3 || valid === 4)) { /**exitoso */
			this.handlerUpdateLead(true)
			GTM.pasosCotizador('Paso 3: Preguntas', this.props.cotizacion.precioprima)
			this.props.handlerPaso(6)
			this.setState({ textoValidacion: "" })
		} else if (valid === 1 || valid === 2 || valid === 3 || valid === 4) { /**descalificado */
			this.handlerUpdateLead(false)
			GTM.pasosCotizador('Paso 3.1: Resultado denegado', this.props.cotizacion.precioprima)
			this.props.handlerPaso(8)
		} else { /** falta completar el cuestionario */
			this.setState({ textoValidacion: "*Por favor responda todo el cuestionario" })
		}

	}

	hanlderCheckBox = (order, value) => {
		this.setState({
			[order]: value
		})
	}

	handlerAnterior=()=>{
		this.props.handlerPaso(2)
	}

	handlerPregunta = (e, orden) => {
		this.setState({ textoValidacion: "" })


		if (e.target.value === "S") {
			this.setState({ [e.target.name]: { positivo: true, negativo: false, orden: orden, name: e.target.name } })
		} else if (e.target.value === "N") {
			this.setState({ [e.target.name]: { positivo: false, negativo: true, orden: orden, name: e.target.name } })
		}
	}

	render() {
		let title = <p className="bxTitle mb-full center">¡Vas por buen camino!<br />Ahora necesitamos que respondas estas preguntas</p>
		return (
			<section className="container">
				<BtnVolver handlerAnterior={this.handlerAnterior} /> 
				<div className="boxWhite">
					<Pasos paso={3} />
					<div className="frmQuestion">
						<Title value={title} />
						<PreguntaItem
							hanlderCheckBox={this.hanlderCheckBox}
							pregunta="He sido diagnosticado de cáncer,tumor maligno u otro tipo de enfermedad oncológica."
							respuesta={this.state.respuesta1}
							handlerPregunta={this.handlerPregunta} />
						<PreguntaItem
							hanlderCheckBox={this.hanlderCheckBox}
							pregunta="Realizaré o me encuentro realizando pruebas o exámenes para descartar alguna enfermedad oncológica."
							respuesta={this.state.respuesta2}
							handlerPregunta={this.handlerPregunta} />
						<PreguntaItem
							hanlderCheckBox={this.hanlderCheckBox}
							pregunta="En los últimos 5 años, he fumado o fumo más de 11 cigarrillos diarios."
							respuesta={this.state.respuesta3}
							handlerPregunta={this.handlerPregunta} />
						<PreguntaItem
							hanlderCheckBox={this.hanlderCheckBox}
							pregunta="Parientes directos han sido diagnosticados de cáncer de mama o de colon."
							respuesta={this.state.respuesta4}
							handlerPregunta={this.handlerPregunta} />

						<p className="texto-valida-cuestionario center">{this.state.textoValidacion}</p>

						<p className="texto-confirm-cuestionario center">Toma en cuenta que al darle click en “continuar” estás declarando que la información brindada es exacta y veraz.</p>
						<div className="center">
							<Button
								value="Continuar"
								clase="btn btnAction"
								handlerOnClick={this.handlerSiguiente} />
						</div>
					</div>
				</div>
			</section>
		)
	}
}

const mapStateToProps = store => {
	return {
		cotizacion: store.cotizacion.cotizacion
	}
}

const mapDispatchToProps = dispatch => {
	return {
		resgistrarLead: (params) => dispatch(fetchRegistrarLead(params)),
		updateCotizacion: (cotizacion) => dispatch(fetchUpdateCotizacion(cotizacion)),
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(PageCuestionario)