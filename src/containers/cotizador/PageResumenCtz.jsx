import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchUpdateCotizacion } from '../../actions/cotizacion';
import { fetchUbigeoDepartamento, fetchUbigeoDistrito, fetchUbigeoProvincia } from '../../actions/ubigeo';
import Input from '../../components/cotizador/commons/Input';
import Title from '../../components/cotizador/commons/Title';
import imgSecure from '../../resources/images/iconos/imgsecure.png';
import imgpago from '../../resources/images/iconos/mpagos.png';
import Pasos from '../../components/commons/Pasos';
import UtilsJs from '../../utils/Funciones';
import imgcheck from '../../resources/images/iconos/check.png'
import error from '../../resources/images/iconos/error.png'
import GTM from '../../utils/GTM'

class PageResumenCtz extends Component {
	state = {
		tipodocdni		: '',
		tipodocce		:'',
		nrodni			: '',
		nroce			: '',
		nombre			: '',
		apepaterno		: '',
		apematerno		: '',
		fecnacimiento	: '',
		celular			: '',
		correo			: '',
		idpgenero		: '',
		idedepartamento	: '',
		ideprovincia	: '',
		idedistrito		: '',
		dscdepartamento	: '',
		dscprovincia	: '',
		dscdistrito		: '',
		nomcompleto		: '',
		direccion		: '',
		nombreMasc		: '',
		apepaternoMasc	: '',
		apematernoMasc	: '',
		validnombre		: false,
		validapepaterno	: false,
		validapematerno	: false,
		validcelular	: false,
		validcorreo		: false,
		validgenero     : "ini",
		validdireccion  	: "ini",
		validdepartamento	: "ini",
		validdistrito		: "ini",
		validprovincia		: "ini",
		validacionResumen	: "",
		departamentos	: [],
		provincias		: [],
		distritos		: [],				
		disableDNI		: false,
		disableCE		: false,
	}
	
	componentDidMount() {
		GTM.navegarCotizador('Datos')

		this.props.handlerLoading(true)
		let { tipodocdni, tipodocce } = this.props.cotizacion		
		this.setState({ tipodocdni	: 	this.props.cotizacion.tipodocdni,
						tipodocce	:	this.props.cotizacion.tipodocce,
						nrodni		: 	this.props.cotizacion.nrodni,
						nroce		: 	this.props.cotizacion.nroce,
						nombre		: 	this.props.cotizacion.nombre,
						apepaterno	: 	this.props.cotizacion.apepaterno,
						apematerno	: 	this.props.cotizacion.apematerno,
						fecnacimiento: 	this.props.cotizacion.fecnacimiento,
						celular		: 	this.props.cotizacion.celular,
						correo		: 	this.props.cotizacion.correo,
						idpgenero	: 	this.props.cotizacion.idpgenero,
						nomcompleto		: this.props.cotizacion.nomcompleto,						
						validnombre		: this.props.cotizacion.validnombre,
						validapepaterno	: this.props.cotizacion.validapepaterno,
						validapematerno	: this.props.cotizacion.validapematerno,
						validcelular	: this.props.cotizacion.validcelular,
						validcorreo		: this.props.cotizacion.validcorreo
					})
		
		this.handlerUpdateNombre(this.props.cotizacion.nombre, "nombre")
		this.handlerUpdateNombre(this.props.cotizacion.apepaterno, "apepaterno")
		this.handlerUpdateNombre(this.props.cotizacion.apematerno, "apematerno")

		if (tipodocdni) {
			this.setState({
				disableDNI: true,
				disableCE: false
			})
		} else if (tipodocce) {
			this.setState({
				disableDNI: false,
				disableCE: true
			})
		}

		this.props.consultadepartamento().then(resp => {
			if (resp.departamentos !== null) {
				this.setState({
					departamentos: resp.departamentos,
				}, () => this.props.updateCotizacion(this.getDatosContratante()))
			} else {
				this.setState({
					departamentos: [],
				})
				alert('Error Cargar Departamentos')
			}
			this.props.handlerLoading(false)
		})
	}

	busquedadscubigeo = ( codigo, ubigeo, caso ) => {
		for (let i = 0; i < ubigeo.length; i++) {
			if(codigo === ubigeo[i].idedepartamento && caso === "DEPARTAMENTO"){
				return ubigeo[i].dscdepartamento
			}else if(codigo === ubigeo[i].ideprovincia && caso === "PROVINCIA"){
				return ubigeo[i].dscprovincia
			}else if(codigo === ubigeo[i].idedistrito && caso === "DISTRITO"){
				return ubigeo[i].dscdistrito
			}
		}
	}

	handlerUpdateNombre = (nombre, type) => {
		let nombres = nombre.split(" ");
		let nombreMasc = "";
		for (let i = 0; i < nombres.length; i++) {
			nombreMasc = nombreMasc + nombres[i].charAt(0).toUpperCase() + nombres[i].slice(1).toLowerCase() + " "
		}
		nombreMasc = nombreMasc.substring(0, nombreMasc.length-1)
		switch (type) {
			case "nombre":
				this.setState({ nombreMasc : nombreMasc })
				break;
			case "apepaterno":
				this.setState({ apepaternoMasc : nombreMasc })
				break;
			case "apematerno":
				this.setState({ apematernoMasc : nombreMasc })
				break;
			default:
				break;
		}
	}

	handlerObtenerProvincias = (e) => {
		this.props.handlerLoading(true)
		if(e.target.value !== ""){
			this.setState({ idedepartamento: e.target.value, dscdepartamento : this.busquedadscubigeo(e.target.value, this.state.departamentos, "DEPARTAMENTO")  , validacionResumen: "" }, () => {
				this.props.consultaprovincia(this.state.idedepartamento).then(resp => {
					this.setState({
						provincias: resp.provincias,
						validdepartamento: true
					}, () => {
						this.props.updateCotizacion(this.getDatosContratante())  
						this.props.handlerLoading(false)
					})
				})
			})
		}else{
			this.props.handlerLoading(false)
			this.setState({validdepartamento: "error"})
		}
	}

	handlerObtenerDistritos = (e) => {
		this.props.handlerLoading(true)
		if(e.target.value !== ""){
			this.setState({ ideprovincia: e.target.value, dscprovincia:  this.busquedadscubigeo(e.target.value, this.state.provincias, "PROVINCIA") , validacionResumen: ""}, () => {
				this.props.consultardistrito(this.state.ideprovincia).then(resp => {
					this.setState({
						distritos: resp.distritos,
						validprovincia: true  
					}, () => {
						this.props.updateCotizacion(this.getDatosContratante())
						this.props.handlerLoading(false)
					})
				})
			})
		}else {
			this.props.handlerLoading(false)
			this.setState({validprovincia: "error"})
		}
	}

	handlerOnChangeDistrito = (e) => {
		if(e.target.value !== ""){
			this.setState({ idedistrito: e.target.value, dscdistrito : this.busquedadscubigeo(e.target.value, this.state.distritos, "DISTRITO") , validdistrito: true, validacionResumen: "" }, () => this.props.updateCotizacion(this.getDatosContratante()))
		}else{
			this.setState({validdistrito: "error"})
		}
	}

	handlerOnChangeDireccion = (e) => {
		if(UtilsJs.validadireccion(e.target.value, "direccion") === true ){
			this.setState({validdireccion: true, validacionResumen: ""})
		}else if(UtilsJs.validadireccion(e.target.value, "direccion") === "errorDirFormat") {
			this.setState({validdireccion: "error", validacionResumen: "No ingresar caracteres ( # / . ; _° ' ) sólo permite letras, números y guión"})
		}else if(UtilsJs.validadireccion(e.target.value, "direccion") === "errorDirLargo"){
			this.setState({validdireccion: "error", validacionResumen: "Debe tener más de 6 cararacteres."})
		}else {
			this.setState({validdireccion: "error", validacionResumen: "Ingrese la dirección"})
		}

		this.setState({ direccion: e.target.value }, () => this.props.updateCotizacion(this.getDatosContratante()))
	}

	handlerSiguiente = () => {

		this.setState({
			validnombre 	  : this.state.validnombre 	  	 === true ? true : "error",
			validapepaterno   : this.state.validapepaterno   === true ? true : "error",
			validapematerno   : this.state.validapematerno   === true ? true : "error",
			validcelular 	  : this.state.validcelular 	 === true ? true : "error",
			validcorreo		  : this.state.validcorreo 	  	 === true ? true : "error",
			validgenero		  : UtilsJs.validgenero(this.state.idpgenero,"genero")? true : "error",
			validdireccion	  : this.state.validdireccion 	 === true ? true : "error",
			validdepartamento : this.state.validdepartamento === true ? true : "error",
			validdistrito	  : this.state.validdistrito 	 === true ? true : "error",
			validprovincia	  : this.state.validprovincia 	 === true ? true : "error"
		}, () => {
			let {  validnombre, validapepaterno, validapematerno, validcelular, validcorreo ,validgenero, validdireccion, validdepartamento, validdistrito,validprovincia } = this.state

			if( validnombre === true && validapepaterno === true && validapematerno === true && validcelular === true && validcorreo  === true && validgenero === true && validdireccion === true && validdepartamento === true && validdistrito === true && validprovincia === true ){
				GTM.pasosCotizador('Paso 4: Datos', this.props.cotizacion.precioprima)
				this.setState({validacionResumen: ""})
				this.props.handlerPaso(7)
			}else{			
				this.setState({validacionResumen: this.state.validacionResumen ? this.state.validacionResumen : "Complente todos todos los campo"})
			}
		})	
	}

	handlerAnterior = () => {
		this.props.handlerPaso(5)
	}

	handlerOnChangeCotizacion() {
		this.props.updateCotizacion(this.getDatosContratante())
	}

	handlerDatosContratante = (e) => {
		this.setState({validacionResumen: ""})

	
		if (e.target.name === "nombre" || e.target.name === "nombreMasc") {
			if (UtilsJs.validasololetras(e.target.value, "nombre")) {
				this.setState({ validnombre: true, nombre: e.target.value })
			} else {
				this.setState({ validnombre: "error", nombre: e.target.value })
			}
		}

		if (e.target.name === "apepaterno" || e.target.name === "apepaternoMasc") {
			if (UtilsJs.validasololetras(e.target.value, "apepaterno")) {
				this.setState({ validapepaterno: true, apepaterno: e.target.value })
			} else {
				this.setState({ validapepaterno: "error", apepaterno: e.target.value })
			}
		}

		if (e.target.name === "apematerno" || e.target.name === "apematernoMasc") {
			if (UtilsJs.validasololetras(e.target.value, "apematerno")) {
				this.setState({ validapematerno: true, apematerno: e.target.value })
			} else {
				this.setState({ validapematerno: "error", apematerno: e.target.value })
			}
		}

		if (e.target.name === "correo") {
			if (UtilsJs.validacorreo(e.target.value, "correo")) {
				this.setState({ validcorreo: true })
			} else {
				this.setState({ validcorreo: "error" })
			}
		}

		if (e.target.name === "celular") {
			if (UtilsJs.validatelefono(e.target.value, "telefono")) {
				this.setState({ validcelular: true })
			} else {
				this.setState({ validcelular: "error" })
			}
		}

		if(e.target.name === "idpgenero"){
			if(e.target.value === "M" || e.target.value === "F"){
				this.setState({validgenero : true})
			}else {
				this.setState({validgenero : "error"})
			}
		}


		this.setState({ [e.target.name]: e.target.value }, () => this.props.updateCotizacion(this.getDatosContratante()))
	}

	getDatosContratante() {
		return {
			tipodocdni		: this.props.cotizacion.tipodocdni	   ,
			tipodocce		: this.props.cotizacion.tipodocce	   ,
			nrodni			: this.props.cotizacion.nrodni		   ,
			nroce			: this.props.cotizacion.nroce		   ,			
			fecnacimiento	: this.props.cotizacion.fecnacimiento  ,
			apepaterno		: this.state.apepaterno	   			   ,
			apematerno		: this.state.apematerno	   			   ,
			nomcompleto		: this.state.nomcompleto	   		   ,
			nombre			: this.state.nombre		   			   ,
			idpgenero		: this.state.idpgenero	   			   ,
			celular			: this.state.celular		   		   ,
			correo			: this.state.correo		               ,
			nombreapellido	: this.state.nombre+' '+this.state.apepaterno  ,
			idedepartamento	: this.state.idedepartamento		   ,
			ideprovincia	: this.state.ideprovincia			   ,
			idedistrito		: this.state.idedistrito			   ,
			dscdepartamento	: this.state.dscdepartamento		   ,
			dscprovincia	: this.state.dscprovincia			   ,
			dscdistrito		: this.state.dscdistrito			   ,
			direccion		: this.state.direccion				   ,
			usuario			: this.props.cotizacion.usuario  	   ,
			usuarioCot		: this.props.cotizacion.usuarioCot     ,
			nombreMasc 		: this.state.nombreMasc				,
			apepaternoMasc	: this.state.apepaternoMasc			,
			apematernoMasc	: this.state.apematernoMasc			,
			validdni		: this.props.cotizacion.validdni	,
			validce			: this.props.cotizacion.validce		,
			validnombre		: this.state.validnombre			,
			validapepaterno	: this.state.validapepaterno		,
			validapematerno	: this.state.validapematerno		,
			validcelular	: this.state.validcelular			,
			validcorreo		: this.state.validcorreo			,			
			nombreapellidoMasc: this.props.cotizacion.nombreapellidoMasc        ,
			validfechanacimiento	: this.props.cotizacion.validfechanacimiento,
			validdireccion			: this.state.validdireccion		,
			validdepartamento		: this.state.validdepartamento	,
			validdistrito			: this.state.validdistrito		,
			validprovincia			: this.state.validprovincia		,
			validgenero				: this.state.validgenero		,
			idelead					: this.props.cotizacion.idelead , 
			precioprima				: this.props.cotizacion.precioprima ,
			ideProducto				: this.props.cotizacion.ideProducto 
		}
	}

	render() {
		let title = <p className="title-page-ctz">Resumen de Seguro</p>
		let { tipodocdni, nrodni, nroce, nombreMasc, apepaternoMasc, apematernoMasc, fecnacimiento, celular, correo, idpgenero, validnombre, validapepaterno, validapematerno, validcorreo, validcelular, validdireccion, validdepartamento, validdistrito, validprovincia, validgenero } = this.state
		return (
			<section className="container">
				<div className="boxWhite">
					<Pasos paso={4} />
					<div className="bxResume d-flex flex-column">
						<Title value={title} />
						<div className="bxResumen-onlymobile bxResume-items">
							<div className="bxChekout z-depth-3 mb-full d-flex flex-column">
								<b>Estás comprando:</b>
								<p>Respaldo Oncológico Ponle Corazón y colaborando con los niños de Ponle Corazón</p>
								<b>Total a pagar por mes:</b>
								<span className="bxPrice"><i>s/</i>{this.props.datosprima.precioPrima}</span>								
							</div>
						</div>
						<div className="bxResume-item d-flex mb-flex-column">
							<div className="bxResume-items bxReItForm d-flex flex-wrap justify-content-start flex-grow-1">
								<span className="frmSubt">INFORMACIÓN DE TITULAR</span>
								<div className="d-flex mb-flex-column">
									<label>
										<input className="with-gap" name="group3" type="radio" value="dni" checked={this.state.tipodocdni} onChange={()=>console.log('')} />
										<span>DNI</span>
									</label>

									<label>
										<input className="with-gap" name="group3" type="radio" value="ce" checked={this.state.tipodocce} onChange={()=>console.log('')} />
										<span>Carnet de Extranjería</span>
									</label>
								</div>
								<div className="bxInput">
									<Input
										name="tipodocdni"
										clase="mb-full"
										holder="Número de Documento"
										value={tipodocdni?nrodni:nroce}
										handlerOnChange={this.handlerDatosContratante}
										disableCampo={true}
										check={true}
										classnrodocctz=" input-checked-nrodoc"
									/>
								</div>
								<div className="bxInput">
									<Input
										name="nombreMasc"
										holder="Nombre"
										value={nombreMasc}
										handlerOnChange={this.handlerDatosContratante}
										disableCampo={tipodocdni? true : false}
										check={tipodocdni ? true : (validnombre!=="error"?true:validnombre)}
									/>
								</div>
								<div className="bxInput">
									<Input
										name="apepaternoMasc"
										holder="Apellido Paterno"
										clase="mb-full"
										value={apepaternoMasc}
										handlerOnChange={this.handlerDatosContratante}
										disableCampo={tipodocdni? true : false}
										check={tipodocdni ? true : (validapepaterno!=="error"?true:validapepaterno)}
									/>
								</div>
								<div className="bxInput">
									<Input
										name="apematernoMasc"
										holder="Apellido Materno"
										clase="mb-full"
										value={apematernoMasc}
										handlerOnChange={this.handlerDatosContratante}
										disableCampo={tipodocdni ? true : false}
										check={tipodocdni ? true : (validapematerno!=="error"?true:validapematerno)}
									/>
								</div>
								<div className="bxInput">
									<select className="mb-full" name="idpgenero" value={idpgenero} onChange={this.handlerDatosContratante} disabled={tipodocdni  ? true : false}>
										<option value="" >Género</option>
										<option value="M">Hombre</option>
										<option value="F">Mujer</option>
									</select>
									{tipodocdni ? <img className="input-check-select" src={imgcheck} alt="" />  : (validgenero ===  "ini" ? null : (validgenero ==="error" ? <img className="input-check-select input-check-select-error" src={error} alt="" /> :  <img className="input-check-select" src={imgcheck} alt="" />)  )}
								</div>


								<div className="bxInput">
									<Input
										name="fecnacimiento"
										holder="Nacimiento"
										clase="mb-full"
										value={fecnacimiento}
										handlerOnChange={this.handlerDatosContratante}
										disableCampo={true}
										check={true}
									/>
								</div>
								<div className="bxInput">
									<Input
										name="celular"
										holder="Telefono"
										clase="mb-full"
										value={celular}
										handlerOnChange={this.handlerDatosContratante}
										disableCampo={tipodocdni ? true : false}
										longitud= "9"
										check={tipodocdni ? true : (validcelular!=="error"?true:validcelular)}
									/>
								</div>
								<div className="bxInput">
									<Input
										name="correo"
										holder="Correo"
										clase="mb-full"
										value={correo}
										handlerOnChange={this.handlerDatosContratante}
										disableCampo={tipodocdni ? true : false}
										check={tipodocdni ? true : (validcorreo!=="error"?true:validcorreo)}
									/>
								</div>
								<div className="bxInput">
									<select className="mb-full" defaultValue={this.state.idedepartamento} onChange={this.handlerObtenerProvincias} >
										<option value="" >Departamento</option>
										{this.state.departamentos ? (
											this.state.departamentos.map((item, index) => (
												<option key={index} value={item.idedepartamento}> {item.dscdepartamento} </option>
											))
										) : null}
									</select>
									{ (validdepartamento !== "ini" && validdepartamento!=="error"? <img className="input-check-select" src={imgcheck} alt="" /> : (validdepartamento==="error"? <img className="input-check-select-error" src={error} alt="" />: null)  )}
								</div>

								<div className="bxInput">
									<select className="mb-full" defaultValue={this.state.ideprovincia} onChange={this.handlerObtenerDistritos}>
										<option value="" >Provincia</option>
										{this.state.provincias ? (
											this.state.provincias.map((item, index) => (
												<option key={index} value={item.ideprovincia}>{item.dscprovincia}</option>
											))
										) : null}
									</select>
									{ (validprovincia !== "ini" && validprovincia!=="error"?<img className="input-check-select" src={imgcheck} alt="" /> : (validprovincia ==="error" ?<img className="input-check-select-error" src={error} alt="" />: null ))}
								</div>

								<div className="bxInput">
									<select className="mb-full" defaultValue={this.state.idedistrito} onChange={this.handlerOnChangeDistrito}>
										<option value="" >Distrito</option>
										{this.state.distritos ? (
											this.state.distritos.map((item, index) => (
												<option key={index} value={item.idedistrito}>{item.dscdistrito}</option>
											))
										) : null}
									</select>
									{ (validdistrito !== "ini" && validdistrito !=="error" ?<img className="input-check-select" src={imgcheck} alt="" /> : (validdistrito === "error"?<img className="input-check-select-error" src={error} alt="" />: null ))}
									
								</div>
								<div className="bxInput">
									<Input
										name="direccion"
										holder="Direccion completa"
										value={this.state.direccion}
										clase="mb-full"
										handlerOnChange={this.handlerOnChangeDireccion}
										check={(validdireccion !== "ini" && validdireccion !=="error" ? true: (validdireccion==="error"? "error" : null) )}
									/>

									<span className="text-validacion-error">{this.state.validacionResumen}</span>
								</div>
							</div>
							<div className="bxResume-items">
								<div className="bxChekout z-depth-3 mb-full d-flex flex-column">
									<b>Estás comprando:</b>
									<p>Respaldo Oncológico Ponle Corazón y colaborando con los niños de Ponle Corazón</p>
									<b>Total a pagar por mes:</b>
									<span className="bxPrice"><i>s/</i>{this.props.datosprima.precioPrima}</span>
									<button className="btn btnAction" onClick={this.handlerSiguiente}>PAGAR</button>
									<br/>
									<p>Toma en cuenta que tu póliza llegará al correo electrónico que registraste al</p>
									<p>inicio de tu compra en máximo 2 días. La vigencia empezará en ese mismo día.</p>
									<hr />
									<img className="flex-shrink-0" src={imgpago} alt="" />
								</div>
  
								<div className="bxSecure d-flex flex-column justify-content-center">
									<div className="bxSecure-item d-flex align-items-center">
										<img className="flex-shrink-0" src={imgSecure} alt="" />
										<p>Promesa de seguridad</p>
									</div>
									<p>Nos tomamos en serio tu seguridad y por eso trabajamos duro para asegurarnos que tu compra digital sea hecha sin problemas.</p>
								</div>
							</div>
						</div>
						<div className="bxResume-item">
							{/* <span className="text-validacion-error">{this.state.validacionResumen}</span> */}
							<div className="d-flex flex-column">
								<span className="frmSubt">INFORMACIÓN DE HEREDEROS</span>
								<p>En caso te sucede algo, el monto será otorgado a tus beneficiarios legales:</p>
								<p>Tu esposo/esposa, hijos menores de 16 años (si los tuvieras) y tus padres. </p>
								<p>Posterior a tu compra, podrás indicar al (los) beneficiario (s) legal(es)</p>
								<p>de tu elección enviándonos un email a: pamela.nogales@rimac.com.pe</p>
							</div>
						</div>
					</div>
				</div>
			</section>
		)
	}
}



const mapStateToProps = store => {
	const { departamentos } = store.departamentos
	const { provincias } = store.provincias
	const { distritos } = store.distritos
	const { cotizacion } = store.cotizacion
	const { datosprima } = store.consultaprima
	return { departamentos, provincias, distritos, cotizacion, datosprima }
}

const mapDispatchToProps = dispatch => {
	return {
		consultadepartamento: () => dispatch(fetchUbigeoDepartamento()),
		consultaprovincia: (data) => dispatch(fetchUbigeoProvincia(data)),
		consultardistrito: (data) => dispatch(fetchUbigeoDistrito(data)),
		updateCotizacion: (cotizacion) => dispatch(fetchUpdateCotizacion(cotizacion))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(PageResumenCtz)