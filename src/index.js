import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

import TagManager from 'react-gtm-module'

const tagManagerArgs = {
	gtmId: 'GTM-T4MHD9Z'
}

TagManager.initialize(tagManagerArgs)

const root = document.getElementById('root');
ReactDOM.render(<App />, root );
registerServiceWorker();
