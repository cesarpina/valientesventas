import { API } from "../utils/Constantes";
export const REQUEST_CULQI_CARGODIRECTO = 'REQUEST_CULQI_CARGODIRECTO'
export const RECEIVE_CULQI_CARGODIRECTO = 'RECEIVE_CULQI_CARGODIRECTO'
export const INVALID_CULQI_CARGODIRECTO = 'INVALID_CULQI_CARGODIRECTO'

function requestculqicargodirecto() {
	return {
		type: REQUEST_CULQI_CARGODIRECTO
	}
}

function receiveculqicargodirecto(culqicargodirecto) {
	return {
		type: RECEIVE_CULQI_CARGODIRECTO,
		culqicargodirecto
	}
}

function invalidculqicargodirecto() {
	return {
		type: INVALID_CULQI_CARGODIRECTO
	}
}

 

export function fetchCulqicargodirecto(data) {
	return dispatch => {
		dispatch(requestculqicargodirecto())
		var url = API.URL_CULQI_CARGODIRECTO,
			params = {
				method: 'POST',
				body: JSON.stringify(data),
				mode: 'cors',
				redirect: 'follow',
				headers: new Headers({
					'Content-Type': 'application/json',
				})
			} 
		var request = new Request(url, params);
		return fetch(request)
			.then(response => response.json())
			.then(json => dispatch(receiveculqicargodirecto(json)))
			.catch(err => dispatch(invalidculqicargodirecto()))
	}
}

