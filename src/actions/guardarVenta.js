import { API } from "../utils/Constantes";
export const REQUEST_GRAGAR_VENTA = 'REQUEST_GRAGAR_VENTA'
export const RECEIVE_GRAGAR_VENTA = 'RECEIVE_GRAGAR_VENTA'
export const INVALID_GRAGAR_VENTA = 'INVALID_GRAGAR_VENTA'

function requestgrabarventa() {
	return {
		type: REQUEST_GRAGAR_VENTA
	}
}

function receivegrabarventa(grabarVenta) {
	return {
		type: RECEIVE_GRAGAR_VENTA,
		grabarVenta
	}
}

function invalidgrabarventa() {
	return {
		type: INVALID_GRAGAR_VENTA
	}
}

export function fetchGrabarVenta(data) {
	return dispatch => {
		dispatch(requestgrabarventa())
		var url = API.URL_GRABAR_VENTA,
			params = {
				method: 'POST',
				body: JSON.stringify(data),
				mode: 'cors',
				redirect: 'follow',
				headers: new Headers({
					'Content-Type': 'application/json',
				})
            }
        
		var request = new Request(url, params);
		return fetch(request)
			.then(response => response.json())
			.then(json => dispatch(receivegrabarventa(json)))
			.catch(err => dispatch(invalidgrabarventa()))
	}
}