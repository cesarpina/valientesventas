import { API } from "../utils/Constantes";
export const REQUEST_GRAGAR_TERCERO = 'REQUEST_GRAGAR_TERCERO'
export const RECEIVE_GRAGAR_TERCERO = 'RECEIVE_GRAGAR_TERCERO'
export const INVALID_GRAGAR_TERCERO = 'INVALID_GRAGAR_TERCERO'

function requestgrabartercero() {
	return {
		type: REQUEST_GRAGAR_TERCERO
	}
}

function receivegrabartercero(grabarTercero) {
	return {
		type: RECEIVE_GRAGAR_TERCERO,
		grabarTercero
	}
}

function invalidgrabartercero() {
	return {
		type: INVALID_GRAGAR_TERCERO
	}
}

export function fetchGrabarTercero(data) {
	return dispatch => {
		dispatch(requestgrabartercero())
		var url = API.URL_GRABAR_TERCERO,
			params = {
				method: 'POST',
				body: JSON.stringify(data),
				mode: 'cors',
				redirect: 'follow',
				headers: new Headers({
					'Content-Type': 'application/json',
				})
			}

		var request = new Request(url, params);
		return fetch(request)
			.then(response => response.json())
			.then(json => dispatch(receivegrabartercero(json)))
			.catch(err => dispatch(invalidgrabartercero()))
	}
}