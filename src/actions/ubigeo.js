import { API } from '../utils/Constantes';

export const REQUEST_DEPARTAMENTO = 'REQUEST_DEPARTAMENTO'
export const RECEIVE_DEPARTAMENTO = 'RECEIVE_DEPARTAMENTO'
export const INVALID_DEPARTAMENTO = 'INVALID_DEPARTAMENTO'

export const REQUEST_PROVINCIA = 'REQUEST_PROVINCIA'
export const RECEIVE_PROVINCIA = 'RECEIVE_PROVINCIA'
export const INVALID_PROVINCIA = 'INVALID_PROVINCIA'

export const REQUEST_DISTRITO = 'REQUEST_DISTRITO'
export const RECEIVE_DISTRITO = 'RECEIVE_DISTRITO'
export const INVALID_DISTRITO = 'INVALID_DISTRITO'

function requestDepartamento() {
	return {
		type: REQUEST_DEPARTAMENTO
	}
}

function receiveDepartamento(departamentos) {
	return {
		type: RECEIVE_DEPARTAMENTO,
		departamentos 
	}
}

function invalidDepartamento() {
	return {
		type: INVALID_DEPARTAMENTO
	}
}

function requestProvincia() {
	return {
		type: REQUEST_PROVINCIA
	}
}

function receiveProvincia(provincias) {
	return {
		type: RECEIVE_PROVINCIA,
		provincias
	}
}

function invalidProvincia() {
	return {
		type: INVALID_PROVINCIA
	}
}

function requestDistrito() {
	return {
		type: REQUEST_DISTRITO
	}
}

function receiveDistrito(distritos) {
	return {
		type: RECEIVE_DISTRITO,
		distritos 
	}
}

function invalidDistrito() {
	return {
		type: INVALID_DISTRITO
	}
}

export function fetchUbigeoDepartamento(data) {
	var departamento = 'DEPARTAMENTO';
	return dispatch => {
		dispatch(requestDepartamento())
		var url = API.URL_UBIGEO_DEPARTAMENTO,
			params = {
				method: 'POST',
				body: JSON.stringify(data),
				mode: 'cors',
				redirect: 'follow',
				headers: new Headers({
					'Content-Type': 'application/json',
				})
			}
			 
		var request = new Request(url, params);
		 
		return fetch(request)
			.then(response => response.json())
			.then(json => dispatch(receiveDepartamento(json)))
			.catch(err => dispatch(invalidDepartamento()))
	}
}

export function fetchUbigeoProvincia(idedepartamento) {
	return dispatch => {
		dispatch(requestProvincia())
		var url = API.URL_UBIGEO_PROVINCIA,
			params = {
				method: 'POST',
				body: JSON.stringify({ "tipo": "PROVINCIA", "departamento": idedepartamento }),
				mode: 'cors',
				redirect: 'follow',
				headers: new Headers({
					'Content-Type': 'application/json',
				})
			}
		var request = new Request(url, params);
		return fetch(request)
			.then(response => response.json())
			.then(json => dispatch(receiveProvincia(json)))
			.catch(err => dispatch(invalidProvincia()))
	}
}

export function fetchUbigeoDistrito(idedepartamento,ideprovincia) {
	return dispatch => {
		dispatch(requestDistrito())
		var url = API.URL_UBIGEO_DISTRITO,
			params = {
				method: 'POST',
				body: JSON.stringify({ "tipo": "DISTRITO", "departamento": idedepartamento, "provincia": ideprovincia }),
				mode: 'cors',
				redirect: 'follow',
				headers: new Headers({
					'Content-Type': 'application/json',
				})
			}

		 
		var request = new Request(url, params);
		return fetch(request)
			.then(response => response.json())
			.then(json => dispatch(receiveDistrito(json)))
			.catch(err => dispatch(invalidDistrito()))
	}
}

export function fetchUbigeo(idUbigeo) {
	return dispatch => {
		dispatch(requestDistrito())
		var url = API.URL_UBIGEO_DISTRITO,
			params = {
				method: 'POST',
				body: JSON.stringify({ "tipo": "UBIGEO", "departamento": idUbigeo }),
				mode: 'cors',
				redirect: 'follow',
				headers: new Headers({
					'Content-Type': 'application/json',
				})
			}

		 
		var request = new Request(url, params);
		return fetch(request)
			.then(response => response.json())
			.then(json => dispatch(receiveDistrito(json)))
			.catch(err => dispatch(invalidDistrito()))
	}
}