import { API, CTTES } from "../utils/Constantes";
export const REQUEST_OBTENER_CONSTANTES = 'REQUEST_OBTENER_CONSTANTES'
export const RECEIVE_OBTENER_CONSTANTES = 'RECEIVE_OBTENER_CONSTANTES'
export const INVALID_OBTENER_CONSTANTES = 'INVALID_OBTENER_CONSTANTES'


export const UPDATE_CONSTANTES = "UPDATE_CONSTANTES"
export const RESET_STORE = "RESET_STORE"

const dispatchUpdateConstantes = (cttes) => {
    return {
        type: UPDATE_CONSTANTES, 
        payload: cttes
    }
}

export const loadConstantes = () => {
    let lstKeysCttes = [        
        CTTES.KEY_PDIG_USUARIO_COT_IEG     
    ];

    return (dispatch, getState) => {
        lstKeysCttes.map(item => {
            loadConstante(dispatch, getState, item);
        });
    }
}

const loadConstante = (dispatch, getState, ctteKey) => {
    let config = {
        method  : 'POST',
        body    : JSON.stringify({"ideconstante" : ctteKey}),            
        mode    : 'cors',
        redirect: 'follow',
        headers : new Headers ({
            'Content-Type' : 'application/json',
        })
    }
    let request = new Request( API.URL_OBTENER_CONSTANTES , config );

    fetch(request)
        .then(response => response.json())
        .then(response => {
            if( response.tx && response.tx.codigo === "0"){
                let newCttes = {...getState().constantes};
                newCttes[ctteKey] = response.valor || response.valorclob;
                dispatch(dispatchUpdateConstantes(newCttes));
            }
        })
        .catch(error => {
            console.log("Error al invocar servicio de Constantes ->" + error);
        });
}


// const convertArrayCttsToMap = (array, storeCttes) => {
//     let newCttes = {...storeCttes};
//     array.map((item) => {
//         var key = item.ideconstante;
//         newCttes[key] = item;
//     })

//     return newCttes;
// }
