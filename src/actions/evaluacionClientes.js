import { API } from "../utils/Constantes";


export const REQUEST_EVALUACIONCLIENTES = 'REQUEST_EVALUACIONCLIENTES'
export const RECEIVE_EVALUACIONCLIENTES = 'RECEIVE_EVALUACIONCLIENTES'
export const INVALIDATE_EVALUACIONCLIENTES = 'INVALIDATE_EVALUACIONCLIENTES'

function requestEVALUACIONCLIENTES() {
	return {
		type: REQUEST_EVALUACIONCLIENTES
	}
}

function receiveEVALUACIONCLIENTES(cliente) {
	return {
		type: RECEIVE_EVALUACIONCLIENTES,
		cliente
	}
}

function invalidateEVALUACIONCLIENTES() {
	return {
		type: INVALIDATE_EVALUACIONCLIENTES
		
	}
}

export function fetchEvaluacionClientes(xNroCliente,xtipoDoc, xNroDoc, xpaso) {
	return dispatch => {
		dispatch(requestEVALUACIONCLIENTES());
		var url = API.URL_EVALUACION_EMPRESA,
			params = {
				method: 'POST',
				body: JSON.stringify({ NroCliente: xNroCliente, TipoDoc: xtipoDoc,NroDoc: xNroDoc, paso: xpaso }),
				mode: 'cors',
				redirect: 'follow',
				headers: {'Content-Type': 'application/json'
				}
			};
			
		var request = new Request(url, params); 
		return fetch(request)
			.then(response => response.json())
			.then(json => dispatch(receiveEVALUACIONCLIENTES(JSON.parse(json.body))))
			.catch(err => dispatch(invalidateEVALUACIONCLIENTES()))
	} 
}

