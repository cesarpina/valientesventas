import { API } from "../utils/Constantes";

export const REQUEST_PERSONAEQUIFAX = 'REQUEST_PERSONAEQUIFAX'
export const RECEIVE_PERSONAEQUIFAX = 'RECEIVE_PERSONAEQUIFAX'
export const INVALIDATE_PERSONAEQUIFAX = 'INVALIDATE_PERSONAEQUIFAX'

function requestPersonaEquifax() {
	return {
		type: REQUEST_PERSONAEQUIFAX
	}
}

function receivePersonaEquifax(tercero) {
	return {
		type: RECEIVE_PERSONAEQUIFAX,
		tercero
	}
}

function invalidatePersonaEquifax() {
	return {
		type: INVALIDATE_PERSONAEQUIFAX
	}
}

export function fetchPersonaEquifax(dni) {
	return dispatch => {
		dispatch(requestPersonaEquifax());
		var url = API.URL_PERSONA_EQUIFAX,
			params = {
				method: 'POST',
				body: JSON.stringify({ nrodni: dni }),
				mode: 'cors',
				redirect: 'follow',
				headers: {
					'Content-Type': 'application/json'
				}
			};
		var request = new Request(url, params);
		return fetch(request)
			.then(response => response.json())
			.then(json => dispatch(receivePersonaEquifax(json)))
			.catch(err => dispatch(invalidatePersonaEquifax()))
	}
}

