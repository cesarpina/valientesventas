import { API } from "../utils/Constantes";

export const REQUEST_CALCULATEPRICE = 'REQUEST_CALCULATEPRICE'
export const RECEIVE_CALCULATEPRICE = 'RECEIVE_CALCULATEPRICE'
export const INVALIDATE_CALCULATEPRICE = 'INVALIDATE_CALCULATEPRICE'

function requestCALCULATEPRICE() {
	return {
		type: REQUEST_CALCULATEPRICE
	}
}

function receiveCALCULATEPRICE(calculo) {
	return {
		type: RECEIVE_CALCULATEPRICE,
		calculo
	}
}

function invalidateCALCULATEPRICE() {
	return {
		type: INVALIDATE_CALCULATEPRICE
	}
}

export function fetchCALCULATEPRICE(array) {
	return dispatch => {
		dispatch(requestCALCULATEPRICE());
		var url = API.URL_CALCULATE_PRICE,
			params = {
				method: 'POST',
				body: JSON.stringify(array),
				mode: 'cors',
				redirect: 'follow',
				headers: {'Content-Type': 'application/json'
				}
			};
			
		var request = new Request(url, params);
		return fetch(request)
			.then(response => response.json())
			.then(json => dispatch(receiveCALCULATEPRICE(JSON.parse(json.body))))
			.catch(err => dispatch(invalidateCALCULATEPRICE()))
	}
}

