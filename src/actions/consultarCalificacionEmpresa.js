import { API } from "../utils/Constantes";


export const REQUEST_EVALUACION_EMPRESA = 'REQUEST_EVALUACION_EMPRESA'
export const RECEIVE_EVALUACION_EMPRESA = 'RECEIVE_EVALUACION_EMPRESA'
export const INVALIDATE_EVALUACION_EMPRESA = 'INVALIDATE_EVALUACION_EMPRESA'

function requestEVALUACION_EMPRESA() {
	return {
		type: REQUEST_EVALUACION_EMPRESA
	}
}

function receiveEVALUACION_EMPRESA(empresa) {
	return {
		type: RECEIVE_EVALUACION_EMPRESA,
		empresa
	}
}

function invalidateEVALUACION_EMPRESA() {
	return {
		type: INVALIDATE_EVALUACION_EMPRESA
	}
}

export function fetchEVALUACION_EMPRESA(xNroCliente,xNroDoc,xPaso) {
	return dispatch => {
		dispatch(requestEVALUACION_EMPRESA());
		var url = API.URL_EVALUACION_EMPRESA,
			params = {
				method: 'POST',
				body: JSON.stringify({NroCliente: xNroCliente, NroDoc: xNroDoc, Paso: xPaso }),
				mode: 'cors',
				redirect: 'follow',
				headers: {'Content-Type': 'application/json'
				}
			};
			
		 
		var request = new Request(url, params);
		
		return fetch(request)
			.then(response => response.json())
			.then(json => dispatch(receiveEVALUACION_EMPRESA(JSON.parse(json.body))))
			.catch(err => dispatch(invalidateEVALUACION_EMPRESA()))
	}
}

