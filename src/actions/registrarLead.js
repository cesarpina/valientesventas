import {API} from '../utils/Constantes'

export const REQUEST_REGISTRARLEAD = 'REQUEST_REGISTRARLEAD'
export const RECEIVE_REGISTRARLEAD = 'RECEIVE_REGISTRARLEAD'
export const INVALID_REGISTRARLEAD = 'INVALID_REGISTRARLEAD'

function requestRegistrarLead(){
    return{
        type: REQUEST_REGISTRARLEAD
    }
}

function receiveRegistrarLead(lead){
    return{
        type: RECEIVE_REGISTRARLEAD,
        lead
    }
}

function invalidRegistrarLead(){
    return{
        type: INVALID_REGISTRARLEAD
    }
}

export function fetchRegistrarLead(data){
    return dispatch => {
        dispatch(requestRegistrarLead())
        var url = API.URL_REGISTRAR_LEAD ,
        params = {
            method: 'POST',
            body: JSON.stringify(data),
            mode: 'cors',
            redirect: 'follow',
            headers: new Headers ({
                'Content-Type' : 'application/json',
            })
        }
         
        var request = new Request( url , params );
        return fetch(request)
            .then(response => response.json())
            .then(json => dispatch(receiveRegistrarLead(json)))
            .catch(err => dispatch(invalidRegistrarLead()))
    }
}

