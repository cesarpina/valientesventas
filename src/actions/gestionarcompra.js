import { API } from "../utils/Constantes";
export const REQUEST_GESTIONAR_COMPRA = 'REQUEST_GESTIONAR_COMPRA'
export const RECEIVE_GESTIONAR_COMPRA = 'RECEIVE_GESTIONAR_COMPRA'
export const INVALID_GESTIONAR_COMPRA = 'INVALID_GESTIONAR_COMPRA'

function requestgestionarcompra() {
	return {
		type: REQUEST_GESTIONAR_COMPRA
	}
}

function receivegestionarcompra(compra) {
	return {
		type: RECEIVE_GESTIONAR_COMPRA,
		compra
	}
}

function invalidgestionarcompra() {
	return {
		type: INVALID_GESTIONAR_COMPRA
	}
}

export function fetchgestionarcompra(data) {
	return dispatch => {
		dispatch(requestgestionarcompra())
		var url = API.URL_GESTIONAR_COMPRA,
			params = {
				method: 'POST',
				body: JSON.stringify(data),
				mode: 'cors',
				redirect: 'follow',
				headers: new Headers({
					'Content-Type': 'application/json',
				})
			}
		var request = new Request(url, params);
		return fetch(request)
			.then(response => response.json())
			.then(json => dispatch(receivegestionarcompra(json)))
			.catch(err => dispatch(invalidgestionarcompra()))
	}
}

