import { API } from "../utils/Constantes";
import { CULQI } from "../utils/Constantes";
import { Buffer } from 'buffer'
export const REQUEST_CULQI_TOKEN = 'REQUEST_CULQI_TOKEN'
export const RECEIVE_CULQI_TOKEN = 'RECEIVE_CULQI_TOKEN'
export const INVALID_CULQI_TOKEN = 'INVALID_CULQI_TOKEN'

function requestculqitoken() {
	return {
		type: REQUEST_CULQI_TOKEN
	}
}

function receiveculqitoken(culqitoken) {
	return {
		type: RECEIVE_CULQI_TOKEN,
		culqitoken
	}
}

function invalidculqitoken() {
	return {
		type: INVALID_CULQI_TOKEN
	}
}

export function fetchCulqiToken(data) {
	return dispatch => {
		dispatch(requestculqitoken())
		var url = API.URL_CULQI_TOKEN,
			params = {
				method: "POST", 
				body: JSON.stringify(data),				
				mode: 'cors',
				redirect: 'follow',
				headers: new Headers({
					'Content-Type': 'application/json',
					'Authorization' : 'Bearer ' + CULQI.KEY_BEARER
				})
			}
			 
		var request = new Request(url, params);
		return fetch(request)
			.then(response => response.json())
			.then(json => dispatch(receiveculqitoken(json)))
			.catch(err => console.error(err),dispatch(invalidculqitoken()))
	}
}

