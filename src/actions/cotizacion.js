export const RESET_STORE = 'RESET_STORE';
export const UPDATE_COTIZACION = 'UPDATE_COTIZACION';

export const fetchUpdateCotizacion = (cotizacion) => {
	return {
		type: UPDATE_COTIZACION,
		cotizacion
	}
} 

export const resetStore = () => {
	return { type: RESET_STORE }
}