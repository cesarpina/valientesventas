import { API } from "../utils/Constantes";


export const REQUEST_CONSULTACLIENTES = 'REQUEST_CONSULTACLIENTES'
export const RECEIVE_CONSULTACLIENTES = 'RECEIVE_CONSULTACLIENTES'
export const INVALIDATE_CONSULTACLIENTES = 'INVALIDATE_CONSULTACLIENTES'

function requestConsultaClientes() {
	return {
		type: REQUEST_CONSULTACLIENTES
	}
}

function receiveConsultaClientes(principalcliente) {
	return {
		type: RECEIVE_CONSULTACLIENTES,
		principalcliente
	}
}

function invalidateConsultaClientes() {
	return {
		type: INVALIDATE_CONSULTACLIENTES
	}
}

export function fetchConsultaClientes(dni) {
	return dispatch => {
		dispatch(requestConsultaClientes());
		var url = API.URL_CONSULTA_CLIENTES,
			params = {
				method: 'POST',
				body: JSON.stringify({ Dni: dni }),
				mode: 'cors',
				redirect: 'follow',
				headers: {'Content-Type': 'application/json'
				}
			};
			
		var request = new Request(url, params);
		return fetch(request)
			.then(response => response.json())
			.then(json => dispatch(receiveConsultaClientes(JSON.parse(json.body))))
			.catch(err => dispatch(invalidateConsultaClientes()))
	}
}

