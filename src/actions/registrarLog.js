import {API} from '../utils/Constantes'

export const REQUEST_REGISTRARLOG = 'REQUEST_REGISTRARLOG'
export const RECEIVE_REGISTRARLOG = 'RECEIVE_REGISTRARLOG'
export const INVALID_REGISTRARLOG = 'INVALID_REGISTRARLOG'

function requestRegistrarLog(){
    return{
        type: REQUEST_REGISTRARLOG
    }
}

function receiveRegistrarLog(){
    return{
        type: RECEIVE_REGISTRARLOG
    }
}

function invalidRegistrarLog(){
    return{
        type: INVALID_REGISTRARLOG
    }
}

export function fetchRegistrarLog(dniCliente,modulo,accion){
    return dispatch => {
        dispatch(requestRegistrarLog())
        var url = API.URL_LOG_EVENT ,
        params = {
            method: 'POST',
            body: JSON.stringify({
                "DNICliente": dniCliente,
                "modulo": modulo,
                "accion": accion
              }),
            mode: 'cors',
            redirect: 'follow',
            headers: new Headers ({
                'Content-Type' : 'application/json',
            })
        }
        var request = new Request( url , params );
        return fetch(request)
            .then(response => response.json())
            .then(json => dispatch(receiveRegistrarLog(json)))
            .catch(err => dispatch(invalidRegistrarLog()))
    }
}

