import { API } from "../utils/Constantes";
export const REQUEST_CONSULTAEMPRESA = 'REQUEST_CONSULTAEMPRESA'
export const RECEIVE_CONSULTAEMPRESA = 'RECEIVE_CONSULTAEMPRESA'
export const INVALIDATE_CONSULTAEMPRESA = 'INVALIDATE_CONSULTAEMPRESA'

export const REQUEST_LISTAEMPRESA = 'REQUEST_LISTAEMPRESA'
export const RECEIVE_LISTAEMPRESA = 'RECEIVE_LISTAEMPRESA'
export const INVALIDATE_LISTAEMPRESA = 'INVALIDATE_LISTAEMPRESA'

//datos de empresa del cliente
function requestCONSULTAEMPRESA() {
	return {
		type: REQUEST_CONSULTAEMPRESA
	}
}

function receiveCONSULTAEMPRESA(empresa) {
	return {
		type: RECEIVE_CONSULTAEMPRESA,
		empresa
	}
}

function invalidateCONSULTAEMPRESA() {
	return {
		type: INVALIDATE_CONSULTAEMPRESA
	}
}

//lista empresas
function requestLISTAEMPRESA() {
	return {
		type: REQUEST_LISTAEMPRESA
	}
}

function receiveLISTAEMPRESA(listaempresa) {
	return {
		type: RECEIVE_LISTAEMPRESA,
		listaempresa
	}
}

function invalidateLISTAEMPRESA() {
	return {
		type: INVALIDATE_LISTAEMPRESA
	}
}

export function fetchCONSULTAEMPRESA(xDni, xRuc, tipo) {

	//var tipo = xRuc.substr(0,2) == '10' ? "Natural" : "Juridico"; 
	return dispatch => {
		dispatch(tipo == 1 ? requestCONSULTAEMPRESA() : requestLISTAEMPRESA());
		var url = API.URL_CONSULTA_EMPRESA,
			params = {
				method: 'POST',
				body: JSON.stringify({Dni: xDni, Ruc: xRuc }),
				mode: 'cors',
				redirect: 'follow',
				headers: {'Content-Type': 'application/json'
				}
			};
			 
		var request = new Request(url, params);
		
		return fetch(request)
			.then(response => response.json())
			.then(json => dispatch(tipo == 1 ? receiveCONSULTAEMPRESA(JSON.parse(json.body)) : receiveLISTAEMPRESA(JSON.parse(json.body))))
			.catch(err => console.log(err), dispatch(tipo == 1 ? invalidateCONSULTAEMPRESA() : invalidateLISTAEMPRESA()))
	}
}


export const RESET_STORE = 'RESET_STORE';
export const UPDATE_EMPRESA = 'UPDATE_EMPRESA';

export const fetchUpdateEmpresa = (empresa) => { 
	return {
		type: UPDATE_EMPRESA,
		empresa
	}
} 

export const resetStore = () => {
	return { type: RESET_STORE }
}