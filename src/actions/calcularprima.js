import { API } from "../utils/Constantes";
export const REQUEST_CONSULTA_PRIMA = 'REQUEST_CONSULTA_PRIMA'
export const RECEIVE_CONSULTA_PRIMA = 'RECEIVE_CONSULTA_PRIMA'
export const INVALID_CONSULTA_PRIMA = 'INVALID_CONSULTA_PRIMA'

function requestconsultaprima() {
	return {
		type: REQUEST_CONSULTA_PRIMA
	}
}

function receiveconsultaprima(datosprima) {
	return {
		type: RECEIVE_CONSULTA_PRIMA,
		datosprima
	}
}

function invalidconsultaprima() {
	return {
		type: INVALID_CONSULTA_PRIMA
	}
}

export function fetchconsultaprima(data) {
	return dispatch => {
		dispatch(requestconsultaprima())
		var url = API.URL_CONSULTA_PRIMA,
			params = {
				method: 'POST',
				body: JSON.stringify(data),
				mode: 'cors',
				redirect: 'follow',
				headers: new Headers({
					'Content-Type': 'application/json',
				})
			}
		var request = new Request(url, params);
		return fetch(request)
			.then(response => response.json())
			.then(json => dispatch(receiveconsultaprima(json)))
			.catch(err => dispatch(invalidconsultaprima()))
	}
}

