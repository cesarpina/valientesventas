import { API } from "../utils/Constantes";
export const REQUEST_ENVIARCORREO = 'REQUEST_ENVIARCORREO'
export const RECEIVE_ENVIARCORREO = 'RECEIVE_ENVIARCORREO'
export const INVALID_ENVIARCORREO = 'INVALID_ENVIARCORREO'

function requestenviarcorreo() {
	return {
		type: REQUEST_ENVIARCORREO
	}
}

function receiveenviarcorreo(correo) {
	return {
		type: RECEIVE_ENVIARCORREO,
		correo
	}
}

function invalidenviarcorreo() {
	return {
		type: INVALID_ENVIARCORREO
	}
}

// export function fetchEnviarCorreo(idPlantilla, para, nomCliente) {
// 	return dispatch => {
// 		dispatch(requestenviarcorreo())
// 		var url = API.URL_ENVIAR_CORREO,
// 			params = {
// 				method: 'POST',
// 				body: JSON.stringify({"IdPlantilla": idPlantilla ,"para": para,"cliente": nomCliente}),
// 				mode: 'cors',
// 				redirect: 'follow',
// 				headers: new Headers({
// 					'Content-Type': 'application/json',
// 				})
// 			}
// 		var request = new Request(url, params);
// 		return fetch(request)
// 			.then(response => response.json())
// 			.then(json => dispatch(receiveenviarcorreo(json)))
// 			.catch(err => dispatch(invalidenviarcorreo()))
// 	}
// }


export function fetchEnviarCorreo(tramaCorreoBienvenida) {
	return dispatch => {
		dispatch(requestenviarcorreo())
		var url = API.URL_ENVIAR_CORREO,
			params = {
				method: 'POST',
				body: JSON.stringify(
					{
						"IdPlantilla": tramaCorreoBienvenida.xplantilla,
						"para": tramaCorreoBienvenida.correo,
						"telefono": tramaCorreoBienvenida.telefono,
						"correo": tramaCorreoBienvenida.correo,
						"cliente": tramaCorreoBienvenida.nombre+" "+tramaCorreoBienvenida.apepaterno+" "+tramaCorreoBienvenida.apematerno,
						"documento": tramaCorreoBienvenida.numerodocumento,
						"Factura": "FAC-00001",
						"RUC": "20202020201",
						"razonSocial": "Empresa Los Valientes",
						"scoreAnterior": "350",
						"scoreActual": "400",
						"montoComprobante": "1000",
						"montoRetencionDetraccion": "300",
						"montoNeto": "700",
						"porcentajeCobertura": "80%",
						"numeroCuenta": "191-0101010101-6",
						"montoARecibir": "22"
					}),
				mode: 'cors',
				redirect: 'follow',
				headers: new Headers({
					'Content-Type': 'application/json',
				})
			}
 

		var request = new Request(url, params);
		return fetch(request)
			.then(response => response.json())
			.then(json => dispatch(receiveenviarcorreo(json)))
			.catch(err => dispatch(invalidenviarcorreo()))
	}
}
