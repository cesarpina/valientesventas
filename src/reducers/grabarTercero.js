import { REQUEST_GRAGAR_TERCERO, RECEIVE_GRAGAR_TERCERO, INVALID_GRAGAR_TERCERO } from '../actions/grabarTercero';

function grabaTercero(state, action) {
	switch (action.type) {
		case REQUEST_GRAGAR_TERCERO:
			return Object.assign({}, state, {
				isFetching: true,
				didInvalidate: false
			})
		case RECEIVE_GRAGAR_TERCERO:
			return Object.assign({}, state, {
				isFetching: false,
				didInvalidate: false,
				grabarTercero: action.grabarTercero
			})
		case INVALID_GRAGAR_TERCERO:
			return Object.assign({}, state, {
				isFetching: false,
				didInvalidate: true
			})
		default:
			return state
	}
}

export default function grabarTerceroReducer(state = {
	isFetching: false,
	didInvalidate: false,
	grabarTercero: [],
	lastUpdated: ''
}, action) {
	switch (action.type) {
		case INVALID_GRAGAR_TERCERO:
		case REQUEST_GRAGAR_TERCERO:
		case RECEIVE_GRAGAR_TERCERO:
			return Object.assign({}, state,
				grabaTercero(state, action)
			);
		default:
			return state
	}
}