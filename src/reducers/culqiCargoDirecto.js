import { INVALID_CULQI_CARGODIRECTO, RECEIVE_CULQI_CARGODIRECTO, REQUEST_CULQI_CARGODIRECTO} from '../actions/culqiCargoDirecto';

function culqicargodirecto(state, action) {
	switch (action.type) {
		case REQUEST_CULQI_CARGODIRECTO:
			return Object.assign({}, state, {
				isFetching: true,
				didInvalidate: false
			})
		case RECEIVE_CULQI_CARGODIRECTO:
			return Object.assign({}, state, {
				isFetching: false,
				didInvalidate: false,
				culqicargodirecto: action.culqicargodirecto
			})
		case INVALID_CULQI_CARGODIRECTO:
			return Object.assign({}, state, {
				isFetching: false,
				didInvalidate: true
			})
		default:
			return state
	}
}

export default function culqicargodirectoReducer(state = {
	isFetching: false,
	didInvalidate: false,
	culqicargodirecto: [],
	lastUpdated: ''
}, action) {
	switch (action.type) {
		case INVALID_CULQI_CARGODIRECTO:
		case REQUEST_CULQI_CARGODIRECTO:
		case RECEIVE_CULQI_CARGODIRECTO:
			return Object.assign({}, state,
				culqicargodirecto(state, action)
			);
		default:
			return state
	}
}