import { INVALIDATE_CONSULTAEMPRESA, RECEIVE_CONSULTAEMPRESA, REQUEST_CONSULTAEMPRESA } from '../actions/consultarDatosEmpresa';

function consulta(state, action) {
	switch (action.type) {
			//caso previo a la consulta
		case REQUEST_CONSULTAEMPRESA:
			return Object.assign({}, state, {
				isFetching: true,
				didInvalidate: false
			})
			//caso de que todo esta OK
		case RECEIVE_CONSULTAEMPRESA:
			return Object.assign({}, state, {
				isFetching: false,
				didInvalidate: false,
				empresa: action.empresa
			})
			//caso de error
		case INVALIDATE_CONSULTAEMPRESA:
			return Object.assign({}, state, {
				isFetching: false,
				didInvalidate: true
			})
		default:
			return state
	}
}

export default function consultaempresa(state = {
	isFetching: false,
	didInvalidate: false,
	empresa: null,
	lastUpdated: ''
}, action) {
	switch (action.type) {
		case INVALIDATE_CONSULTAEMPRESA:
		case REQUEST_CONSULTAEMPRESA:
		case RECEIVE_CONSULTAEMPRESA:
			return Object.assign({}, state,
				consulta(state, action)
			);
		default:
			return state
	}
}