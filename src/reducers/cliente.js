import { INVALIDATE_CONSULTACLIENTES, RECEIVE_CONSULTACLIENTES, REQUEST_CONSULTACLIENTES } from '../actions/consultaClientes';

function consulta(state, action) {
	switch (action.type) {
			//caso previo a la consulta
		case REQUEST_CONSULTACLIENTES:
			return Object.assign({}, state, {
				isFetching: true,
				didInvalidate: false
			})
			//caso de que todo esta OK
		case RECEIVE_CONSULTACLIENTES:
			return Object.assign({}, state, {
				isFetching: false,
				didInvalidate: false,
				principalcliente: action.principalcliente
			})
			//caso de error
		case INVALIDATE_CONSULTACLIENTES:
			return Object.assign({}, state, {
				isFetching: false,
				didInvalidate: true
			})
		default:
			return state
	}
}

export default function consultacliente(state = {
	isFetching: false,
	didInvalidate: false,
	principalcliente: null,
	lastUpdated: ''
}, action) {
	switch (action.type) {
		case INVALIDATE_CONSULTACLIENTES:
		case REQUEST_CONSULTACLIENTES:
		case RECEIVE_CONSULTACLIENTES:
			return Object.assign({}, state,
				consulta(state, action)
			);
		default:
			return state
	}
}