import { RESET_STORE, UPDATE_EMPRESA } from '../actions/consultarDatosEmpresa';


export const initialState = {
	empresa: []
}

export default function gestionarEmpresaReducer(state = initialState, action) {
	const { type, empresa } = action;

	switch (type) {

		case RESET_STORE: {
			return initialState;
		}

		case UPDATE_EMPRESA: {
			return { ...state, empresa }
		}

		default: {
			return state;
		}
	}
} 