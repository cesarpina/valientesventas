import { INVALIDATE_LISTAEMPRESA, RECEIVE_LISTAEMPRESA, REQUEST_LISTAEMPRESA } from '../actions/consultarDatosEmpresa';

function consulta(state, action) {
	switch (action.type) {
			//caso previo a la consulta
		case REQUEST_LISTAEMPRESA:
			return Object.assign({}, state, {
				isFetching: true,
				didInvalidate: false
			})
			//caso de que todo esta OK
		case RECEIVE_LISTAEMPRESA:
			return Object.assign({}, state, {
				isFetching: false,
				didInvalidate: false,
				listaempresa: state.listaempresa.concat(action.listaempresa)
			})
			//caso de error
		case INVALIDATE_LISTAEMPRESA:
			return Object.assign({}, state, {
				isFetching: false,
				didInvalidate: true
			})
		default:
			return state
	}
}

export default function listaempresa(state = {
	isFetching: false,
	didInvalidate: false,
	listaempresa: [],
	lastUpdated: ''
}, action) {
	switch (action.type) {
		case INVALIDATE_LISTAEMPRESA:
		case REQUEST_LISTAEMPRESA:
		case RECEIVE_LISTAEMPRESA:
			return Object.assign({}, state,
				consulta(state, action)
			);
		default:
			return state
	}
}