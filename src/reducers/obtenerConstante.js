import {UPDATE_CONSTANTES,  RESET_STORE} from '../actions/obtenerConstante'

const initialState = {
    PDIG_USUARIO : 'PRODDIGWEB'
}

export default function reducer(state = initialState, action){
const {type, payload} = action;

switch(type){
        case RESET_STORE: {
            return initialState;
        }

        case UPDATE_CONSTANTES: {
            return payload;
        }

        default: {
            return state;
        }
    }
}