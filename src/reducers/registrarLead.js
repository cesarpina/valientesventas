import {
    REQUEST_REGISTRARLEAD,
    RECEIVE_REGISTRARLEAD,
    INVALID_REGISTRARLEAD
} from '../actions/registrarLead'


function registrarLead(state, action) {
    switch(action.type) {

        case REQUEST_REGISTRARLEAD:
            return Object.assign({}, state, {
                isFetching : true,
                didInvalidate: false
            })
        case RECEIVE_REGISTRARLEAD:
            return Object.assign({}, state, {
                isFetching: false,
                didInvalidate: false,
                lead: action.lead,
                lastUpdated: action.receivedAt 
            })
        case INVALID_REGISTRARLEAD:
            return Object.assign({}, state, {
                isFetching: false,
                didInvalidate: true
            })
        default:
            return state        
    }
}

export default function registrarLeadReducer(state = {
        isFetching: false,
        didInvalidate: false,
        lead:[],
        lastUpdated: ''
    }   , action) {
    switch (action.type) {
        case INVALID_REGISTRARLEAD:    
        case REQUEST_REGISTRARLEAD:
        case RECEIVE_REGISTRARLEAD: 
            return Object.assign({}, state, 
                registrarLead(state, action)
            );
        default:
            return state
    }
}