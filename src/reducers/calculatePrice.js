import { INVALIDATE_CALCULATEPRICE, RECEIVE_CALCULATEPRICE, REQUEST_CALCULATEPRICE } from '../actions/calculateprice';

function consultaprima(state, action) {
	switch (action.type) {
			//caso previo a la consulta
		case REQUEST_CALCULATEPRICE:
			return Object.assign({}, state, {
				isFetching: true,
				didInvalidate: false
			})
			//caso de que todo esta OK
		case RECEIVE_CALCULATEPRICE:
			return Object.assign({}, state, {
				isFetching: false,
				didInvalidate: false,
				calculo: action.calculo
			})
			//caso de error
		case INVALIDATE_CALCULATEPRICE:
			return Object.assign({}, state, {
				isFetching: false,
				didInvalidate: true
			})
		default:
			return state
	}
}

export default function consultaCalculo(state = {
	isFetching: false,
	didInvalidate: false,
	calculo: null,
	lastUpdated: ''
}, action) {
	switch (action.type) {
		case INVALIDATE_CALCULATEPRICE:
		case REQUEST_CALCULATEPRICE:
		case RECEIVE_CALCULATEPRICE:
			return Object.assign({}, state,
				consultaprima(state, action)
			);
		default:
			return state
	}
}