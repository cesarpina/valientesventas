import { combineReducers } from 'redux';
import { sessionImmutableReducer } from 'redux-react-session';
import gestionarCotizacionReducer from './cotizacion';
import enviarCorreoReducer from './envioCorreo';
import registrarLeadReducer from './registrarLead';
import { consultarDepartamentoReducer, consultarDistritoReducer, consultarProvinciaReducer } from './ubigeo';
import culqiTokenReducer from './culqiToken';
import grabarTerceroReducer from './grabarTercero';
import personaEquifaxReducer from './personaEquifax';
import consultaprimaReducer from './calcularprima';
import culqicargodirectoReducer from './culqiCargoDirecto'
import gestionarcompraReducer from './gestionarcompra'
import constantesReducer from './obtenerConstante'
import consultaCalculo from './calculatePrice'
import consultaempresa from './empresa'
import consultacliente from './cliente'
import listaempresa from './listaempresa' 
import gestionarEmpresaReducer from './consultarDatosEmpresa' 

const rootReducer = combineReducers({
	session		  : sessionImmutableReducer		 ,
	tercero		  : personaEquifaxReducer		 ,
	lead		  : registrarLeadReducer		 ,
	departamentos : consultarDepartamentoReducer ,
	provincias	  : consultarDistritoReducer	 ,
	distritos	  : consultarProvinciaReducer	 ,
	cotizacion    : gestionarCotizacionReducer	 ,
	envioCorreo   : enviarCorreoReducer			 ,
	culqitoken    : culqiTokenReducer			 ,
	grabarTercero : grabarTerceroReducer		 ,
	consultaprima : consultaprimaReducer		 ,
	culqicargodirecto : culqicargodirectoReducer ,
	gestionarcompra   : gestionarcompraReducer	 ,
	constantes        : constantesReducer      	 ,
	calculo			  : consultaCalculo			 ,
	empresa			  : consultaempresa			 ,
	principalcliente  : consultacliente			 ,
	listaempresa      : listaempresa, 
	ubigeoEmpresa 	  : gestionarEmpresaReducer
});

export default rootReducer