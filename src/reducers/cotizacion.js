import { RESET_STORE, UPDATE_COTIZACION } from '../actions/cotizacion';


export const initialState = {
	cotizacion: []
}

export default function gestionarCotizacionReducer(state = initialState, action) {
	const { type, cotizacion } = action;

	switch (type) {

		case RESET_STORE: {
			return initialState;
		}

		case UPDATE_COTIZACION: {
			return { ...state, cotizacion }
		}

		default: {
			return state;
		}
	}
} 