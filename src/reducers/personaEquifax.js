import { INVALIDATE_PERSONAEQUIFAX, RECEIVE_PERSONAEQUIFAX, REQUEST_PERSONAEQUIFAX } from '../actions/personaEquifax';

function personaEquifax(state, action) {
	switch (action.type) {
		case REQUEST_PERSONAEQUIFAX:
			return Object.assign({}, state, {
				isFetching: true,
				didInvalidate: false
			})
		case RECEIVE_PERSONAEQUIFAX:
			return Object.assign({}, state, {
				isFetching: false,
				didInvalidate: false,
				tercero: action.tercero
			})
		case INVALIDATE_PERSONAEQUIFAX:
			return Object.assign({}, state, {
				isFetching: false,
				didInvalidate: true
			})
		default:
			return state
	}
}

export default function personaEquifaxReducer(state = {
	isFetching: false,
	didInvalidate: false,
	tercero: []
}, action) {
	switch (action.type) {
		case INVALIDATE_PERSONAEQUIFAX:
		case REQUEST_PERSONAEQUIFAX:
		case RECEIVE_PERSONAEQUIFAX:
			return Object.assign({}, state,
				personaEquifax(state, action)
			);
		default:
			return state
	}
}