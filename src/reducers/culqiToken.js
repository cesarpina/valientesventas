import { INVALID_CULQI_TOKEN, RECEIVE_CULQI_TOKEN, REQUEST_CULQI_TOKEN} from '../actions/culqiToken';

function culqiToken(state, action) {
	switch (action.type) {
		case REQUEST_CULQI_TOKEN:
			return Object.assign({}, state, {
				isFetching: true,
				didInvalidate: false
			})
		case RECEIVE_CULQI_TOKEN:
			return Object.assign({}, state, {
				isFetching: false,
				didInvalidate: false,
				culqitoken: action.culqitoken
			})
		case INVALID_CULQI_TOKEN:
			return Object.assign({}, state, {
				isFetching: false,
				didInvalidate: true
			})
		default:
			return state
	}
}

export default function culqiTokenReducer(state = {
	isFetching: false,
	didInvalidate: false,
	culqitoken: [],
	lastUpdated: ''
}, action) {
	switch (action.type) {
		case INVALID_CULQI_TOKEN:
		case REQUEST_CULQI_TOKEN:
		case RECEIVE_CULQI_TOKEN:
			return Object.assign({}, state,
				culqiToken(state, action)
			);
		default:
			return state
	}
}