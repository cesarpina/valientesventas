import { INVALID_ENVIARCORREO, RECEIVE_ENVIARCORREO, REQUEST_ENVIARCORREO } from '../actions/envioCorreo';

function enviarCorreo(state, action) {
	switch (action.type) {
		case REQUEST_ENVIARCORREO:
			return Object.assign({}, state, {
				isFetching: true,
				didInvalidate: false
			})
		case RECEIVE_ENVIARCORREO:
			return Object.assign({}, state, {
				isFetching: false,
				didInvalidate: false,
				correo: action.correo
			})
		case INVALID_ENVIARCORREO:
			return Object.assign({}, state, {
				isFetching: false,
				didInvalidate: true
			})
		default:
			return state
	}
}

export default function enviarCorreoReducer(state = {
	isFetching: false,
	didInvalidate: false,
	correo: [],
	lastUpdated: ''
}, action) {
	switch (action.type) {
		case INVALID_ENVIARCORREO:
		case REQUEST_ENVIARCORREO:
		case RECEIVE_ENVIARCORREO:
			return Object.assign({}, state,
				enviarCorreo(state, action)
			);
		default:
			return state
	}
}