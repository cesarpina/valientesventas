import { INVALID_DEPARTAMENTO, INVALID_DISTRITO, INVALID_PROVINCIA, 
		 RECEIVE_DEPARTAMENTO, RECEIVE_DISTRITO, RECEIVE_PROVINCIA, 
		 REQUEST_DEPARTAMENTO, REQUEST_DISTRITO, REQUEST_PROVINCIA } from '../actions/ubigeo';

function consultarUbigeo(state, action) {
	switch (action.type) {
		case REQUEST_DEPARTAMENTO:
			return Object.assign({}, state, {
				isFetching: true,
				didInvalidate: false
			})
		case RECEIVE_DEPARTAMENTO:
			return Object.assign({}, state, {
				isFetching: false,
				didInvalidate: false,
				items: action.departamentos
			})
		case INVALID_DEPARTAMENTO:
			return Object.assign({}, state, {
				isFetching: false,
				didInvalidate: true
			})
		case REQUEST_PROVINCIA:
			return Object.assign({}, state, {
				isFetching: true,
				didInvalidate: false
			})
		case RECEIVE_PROVINCIA:
			return Object.assign({}, state, {
				isFetching: false,
				didInvalidate: false,
				items: action.provincias
			})
		case INVALID_PROVINCIA:
			return Object.assign({}, state, {
				isFetching: false,
				didInvalidate: true
			})
		case REQUEST_DISTRITO:
			return Object.assign({}, state, {
				isFetching: true,
				didInvalidate: false
			})
		case RECEIVE_DISTRITO:
			return Object.assign({}, state, {
				isFetching: false,
				didInvalidate: false,
				items: action.distritos
			})
		case INVALID_DISTRITO:
			return Object.assign({}, state, {
				isFetching: false,
				didInvalidate: true
			})
		default:
			return state
	}
}

export function consultarDepartamentoReducer(state = {
	isFetching: false,
	didInvalidate: false,
	items: [],
	lastUpdated: ''
}, action) {
	switch (action.type) {
		case REQUEST_DEPARTAMENTO:
		case RECEIVE_DEPARTAMENTO:
		case INVALID_DEPARTAMENTO:
			return Object.assign({}, state,
				consultarUbigeo(state, action)
			);
		default:
			return state
	}
}

export function consultarProvinciaReducer(state = {
	provincia: {
		isFetching: false,
		didInvalidate: false,
		data: [],
		lastUpdated: ''
	}
}, action) {
	switch (action.type) {
		case REQUEST_PROVINCIA:
		case RECEIVE_PROVINCIA:
		case INVALID_PROVINCIA:
			return Object.assign({}, state,
				consultarUbigeo(state, action)
			);
		default:
			return state
	}
}

export function consultarDistritoReducer(state = {
	distrito: {
		isFetching: false,
		didInvalidate: false,
		data: [],
		lastUpdated: ''
	}
}, action) {
	switch (action.type) {
		case REQUEST_DISTRITO:
		case RECEIVE_DISTRITO:
		case INVALID_DISTRITO:
			return Object.assign({}, state,
				consultarUbigeo(state, action)
			);
		default:
			return state
	}
}
