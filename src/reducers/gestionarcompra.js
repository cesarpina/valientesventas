import { INVALID_GESTIONAR_COMPRA, RECEIVE_GESTIONAR_COMPRA, REQUEST_GESTIONAR_COMPRA } from '../actions/gestionarcompra';

function gestionarcompra(state, action) {
	switch (action.type) {
		case REQUEST_GESTIONAR_COMPRA:
			return Object.assign({}, state, {
				isFetching: true,
				didInvalidate: false
			})
		case RECEIVE_GESTIONAR_COMPRA:
			return Object.assign({}, state, {
				isFetching: false,
				didInvalidate: false,
				compra: action.compra
			})
		case INVALID_GESTIONAR_COMPRA:
			return Object.assign({}, state, {
				isFetching: false,
				didInvalidate: true
			})
		default:
			return state
	}
}

export default function gestionarcompraReducer(state = {
	isFetching: false,
	didInvalidate: false,
	compra: [],
	lastUpdated: ''
}, action) {
	switch (action.type) {
		case INVALID_GESTIONAR_COMPRA:
		case REQUEST_GESTIONAR_COMPRA:
		case RECEIVE_GESTIONAR_COMPRA:
			return Object.assign({}, state,
				gestionarcompra(state, action)
			);
		default:
			return state
	}
}