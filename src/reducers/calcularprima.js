import { INVALID_CONSULTA_PRIMA, RECEIVE_CONSULTA_PRIMA, REQUEST_CONSULTA_PRIMA } from '../actions/calcularprima';

function consultaprima(state, action) {
	switch (action.type) {
		case REQUEST_CONSULTA_PRIMA:
			return Object.assign({}, state, {
				isFetching: true,
				didInvalidate: false
			})
		case RECEIVE_CONSULTA_PRIMA:
			return Object.assign({}, state, {
				isFetching: false,
				didInvalidate: false,
				datosprima: action.datosprima
			})
		case INVALID_CONSULTA_PRIMA:
			return Object.assign({}, state, {
				isFetching: false,
				didInvalidate: true
			})
		default:
			return state
	}
}

export default function consultaprimaReducer(state = {
	isFetching: false,
	didInvalidate: false,
	datosprima: [],
	lastUpdated: ''
}, action) {
	switch (action.type) {
		case INVALID_CONSULTA_PRIMA:
		case REQUEST_CONSULTA_PRIMA:
		case RECEIVE_CONSULTA_PRIMA:
			return Object.assign({}, state,
				consultaprima(state, action)
			);
		default:
			return state
	}
}